#ifndef __LIBVE_DECORDER2_H__
#define __LIBVE_DECORDER2_H__

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <pthread.h>
#include "vencoder.h"  //* video encode library in "LIBRARY/CODEC/VIDEO/ENCODER"
#include "memoryAdapter.h"
#include "vdecoder.h"

#define DBG_ENABLE 0

#ifdef __cplusplus
extern "C" {
#endif

int DecorderOpen(int outFormat,int codecFormat,int framerate,int frameWidth,int frameHeight);
int DecorderClose();
int DecorderEnter(const void *in,void *out,int size ,int64_t timestamp);

#ifdef __cplusplus
}
#endif


#endif  /* __LIBVE_DECORDER2_H__ */

