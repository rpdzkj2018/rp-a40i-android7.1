#ifndef _POWER_H_
#define _POWER_H_
#include <hardware/hardware.h>
#include <hardware/power.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cutils/properties.h>

#ifdef A83T
#include "SUN8IW6P1.h"
#elif defined A64
#include "SUN50IW1P1.h"
#elif defined A80
#include "SUN9IW1P1.h"
#elif defined A33
#include "SUN8IW5P1.h"
#elif defined T3
#include "SUN8IW11P1.h"
#elif defined A40
#include "SUN8IW11P1.h"
#endif

/* app type define */
#define APP_TYPE_CPU        (0x00000001)
#define APP_TYPE_GPU        (0x00000002)
#define APP_TYPE_IO         (0x00000003)
#define APP_TYPE_CPU_GPU    (0x00000004)


#define  BOOTCOMPLETE  0x0000000f
#define  BENCHMARK     0x000000f0
#define  NORMAL        0x00000f00
#define  MUSIC         0x0000f000
#define  CPU_GOVERNOR     "interactive"
#define  BENCHMARK_INDEX  4
#endif

