#ifndef _VND_BUILDCFG_H
#define _VND_BUILDCFG_H
#define BLUETOOTH_UART_DEVICE_PORT   "/dev/ttyS1"
#define FW_PATCHFILE_LOCATION   "/etc/firmware/"
#define LPM_IDLE_TIMEOUT_MULTIPLE   5
#define SCO_USE_I2S_INTERFACE   TRUE
#define BTVND_DBG   FALSE
#define BTHW_DBG   FALSE
#define VNDUSERIAL_DBG   FALSE
#define UPIO_DBG   FALSE
#endif
