CUR_PATH := vendor/aw/rpdzkj/superuser/

PRODUCT_COPY_FILES += vendor/aw/rpdzkj/superuser/superuser.sh:system/bin/superuser.sh
PRODUCT_COPY_FILES += vendor/aw/rpdzkj/superuser/nosuperuser.sh:system/bin/nosuperuser.sh
modeswitch_files := $(shell ls $(CUR_PATH)/root_enable)
PRODUCT_COPY_FILES += \
    $(foreach file, $(modeswitch_files), \
    $(CUR_PATH)/root_enable/$(file):system/usr/superuser/root_enable/$(file))
modeswitch_files := $(shell ls $(CUR_PATH)/root_disable)
PRODUCT_COPY_FILES += \
    $(foreach file, $(modeswitch_files), \
    $(CUR_PATH)/root_disable/$(file):system/usr/superuser/root_disable/$(file))
