package com.softwinner.bionsettings.common;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.provider.Settings;
import android.preference.SwitchPreference;
import android.util.Log;

import com.softwinner.bionsettings.R;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class SoundSettings extends PreferenceFragment{
    private static final String TAG = "SoundSettings";
    private SwitchPreference soundEnablePreference;

    public SoundSettings() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_sound);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        soundEnablePreference = (SwitchPreference) findPreference("sound_enable");

        int defSound = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.VOICE_CONTROLLER, 0);
        soundEnablePreference.setChecked(defSound == 0 ? false : true);
        soundEnablePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                boolean checked = (Boolean) value;
                if (checked) {
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.VOICE_CONTROLLER, 1);
                } else {
                    Log.w(TAG, "---------fuck you---------");
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.VOICE_CONTROLLER, 0);
                }

                return true;
            }
        });

    }

}
