package com.softwinner.bionsettings.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.Preference;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.widget.Toast;

import com.android.internal.logging.MetricsLogger;
import com.android.settingslib.WirelessUtils;
import com.softwinner.bionsettings.R;

import java.util.concurrent.atomic.AtomicBoolean;

public class WifiEnabler implements Preference.OnPreferenceChangeListener {
    private Context mContext;
//    private SwitchBar mSwitchBar;
    private SwitchPreference mWifiEnabler;
    private boolean mListeningToOnSwitchChange = false;
    private AtomicBoolean mConnected = new AtomicBoolean(false);

    private final WifiManager mWifiManager;
    private boolean mStateMachineEvent;
    private final IntentFilter mIntentFilter;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                handleWifiStateChanged(intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN));
            } else if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION.equals(action)) {
                if (!mConnected.get()) {
                    handleStateChanged(WifiInfo.getDetailedStateOf((SupplicantState)
                            intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE)));
                }
            } else if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
                NetworkInfo info = (NetworkInfo) intent.getParcelableExtra(
                        WifiManager.EXTRA_NETWORK_INFO);
                mConnected.set(info.isConnected());
                handleStateChanged(info.getDetailedState());
            }
        }
    };

    private static final String EVENT_DATA_IS_WIFI_ON = "is_wifi_on";
    private static final int EVENT_UPDATE_INDEX = 0;

//    private Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case EVENT_UPDATE_INDEX:
//                    final boolean isWiFiOn = msg.getData().getBoolean(EVENT_DATA_IS_WIFI_ON);
//                    Index.getInstance(mContext).updateFromClassNameResource(
//                            WifiSettings.class.getName(), true, isWiFiOn);
//                    break;
//            }
//        }
//    };

    public WifiEnabler(Context context, SwitchPreference wifiEnabler) {
        mContext = context;
        mWifiEnabler = wifiEnabler;


        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        mIntentFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        // The order matters! We really should not depend on this. :(
        mIntentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        setupSwitchBar();
    }

    public void setupSwitchBar() {
        final int state = mWifiManager.getWifiState();
        handleWifiStateChanged(state);
        if (!mListeningToOnSwitchChange) {
//            mSwitchBar.addOnSwitchChangeListener(this);
            mWifiEnabler.setOnPreferenceChangeListener(this);
            mListeningToOnSwitchChange = true;
        }
//        mSwitchBar.show();
    }

    public void teardownSwitchBar() {
        if (mListeningToOnSwitchChange) {
//            mSwitchBar.removeOnSwitchChangeListener(this);
            mWifiEnabler.setOnPreferenceChangeListener(null);
            mListeningToOnSwitchChange = false;
        }
//        mSwitchBar.hide();
    }

    public void resume(Context context) {
        mContext = context;
        // Wi-Fi state is sticky, so just let the receiver update UI
        mContext.registerReceiver(mReceiver, mIntentFilter);
        if (!mListeningToOnSwitchChange) {
//            mSwitchBar.addOnSwitchChangeListener(this);
            mWifiEnabler.setOnPreferenceChangeListener(this);
            mListeningToOnSwitchChange = true;
        }
    }

    public void pause() {
        mContext.unregisterReceiver(mReceiver);
        if (mListeningToOnSwitchChange) {
//            mSwitchBar.removeOnSwitchChangeListener(this);
            mWifiEnabler.setOnPreferenceChangeListener(null);
            mListeningToOnSwitchChange = false;
        }
    }

    private void handleWifiStateChanged(int state) {
        switch (state) {
            case WifiManager.WIFI_STATE_ENABLING:
                mWifiEnabler.setEnabled(false);
                break;
            case WifiManager.WIFI_STATE_ENABLED:
                setwifiEnablerChecked(true);
                mWifiEnabler.setEnabled(true);
//                updateSearchIndex(true);
                break;
            case WifiManager.WIFI_STATE_DISABLING:
                mWifiEnabler.setEnabled(false);
                break;
            case WifiManager.WIFI_STATE_DISABLED:
                setwifiEnablerChecked(false);
                mWifiEnabler.setEnabled(true);
//                updateSearchIndex(false);
                break;
            default:
                setwifiEnablerChecked(false);
                mWifiEnabler.setEnabled(true);
//                updateSearchIndex(false);
        }
    }

//    private void updateSearchIndex(boolean isWiFiOn) {
//        mHandler.removeMessages(EVENT_UPDATE_INDEX);
//
//        Message msg = new Message();
//        msg.what = EVENT_UPDATE_INDEX;
//        msg.getData().putBoolean(EVENT_DATA_IS_WIFI_ON, isWiFiOn);
//        mHandler.sendMessage(msg);
//    }

    private void setwifiEnablerChecked(boolean checked) {
        mStateMachineEvent = true;
        mWifiEnabler.setChecked(checked);
        if (checked) {
            mWifiEnabler.setTitle(R.string.switch_on_text);    
        } else {
            mWifiEnabler.setTitle(R.string.switch_off_text);
        }
        
//        mSwitchBar.setChecked(checked);
        mStateMachineEvent = false;
    }

    private void handleStateChanged(@SuppressWarnings("unused") NetworkInfo.DetailedState state) {
        // After the refactoring from a CheckBoxPreference to a Switch, this method is useless since
        // there is nowhere to display a summary.
        // This code is kept in case a future change re-introduces an associated text.
        /*
        // WifiInfo is valid if and only if Wi-Fi is enabled.
        // Here we use the state of the switch as an optimization.
        if (state != null && mSwitch.isChecked()) {
            WifiInfo info = mWifiManager.getConnectionInfo();
            if (info != null) {
                //setSummary(Summary.get(mContext, info.getSSID(), state));
            }
        }
        */
    }

//    @Override
//    public void onSwitchChanged(Switch switchView, boolean isChecked) {
//        //Do nothing if called as a result of a state machine event
//        if (mStateMachineEvent) {
//            return;
//        }
//        // Show toast message if Wi-Fi is not allowed in airplane mode
//        if (isChecked && !WirelessUtils.isRadioAllowed(mContext, Settings.Global.RADIO_WIFI)) {
//            int moId = mContext.getResources().getIdentifier("wifi_in_airplane_mode", "strings", mContext.getPackageName());
//            Toast.makeText(mContext, moId, Toast.LENGTH_SHORT).show();
//            // Reset switch to off. No infinite check/listenenr loop.
//            mSwitchBar.setChecked(false);
//            return;
//        }
//
//        // Disable tethering if enabling Wifi
//        int wifiApState = mWifiManager.getWifiApState();
//        if (isChecked && ((wifiApState == WifiManager.WIFI_AP_STATE_ENABLING) ||
//                (wifiApState == WifiManager.WIFI_AP_STATE_ENABLED))) {
//            mWifiManager.setWifiApEnabled(null, false);
//        }
//        MetricsLogger.action(mContext,
//                isChecked ? MetricsLogger.ACTION_WIFI_ON : MetricsLogger.ACTION_WIFI_OFF);
//        if (!mWifiManager.setWifiEnabled(isChecked)) {
//            // Error
//            mSwitchBar.setEnabled(true);
//            int errId = mContext.getResources().getIdentifier("wifi_error", "strings", mContext.getPackageName());
//            Toast.makeText(mContext, errId, Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean isChecked = Boolean.valueOf(newValue.toString());
        //Do nothing if called as a result of a state machine event
        if (mStateMachineEvent) {
            return true;
        }
        // Show toast message if Wi-Fi is not allowed in airplane mode
        if (isChecked && !WirelessUtils.isRadioAllowed(mContext, Settings.Global.RADIO_WIFI)) {
            int moId = mContext.getResources().getIdentifier("wifi_in_airplane_mode", "strings", mContext.getPackageName());
            Toast.makeText(mContext, moId, Toast.LENGTH_SHORT).show();
            // Reset switch to off. No infinite check/listenenr loop.
//            mSwitchBar.setChecked(false);
            mWifiEnabler.setChecked(false);
            mWifiEnabler.setTitle(R.string.switch_off_text);
            return true;
        }

        // Disable tethering if enabling Wifi
        int wifiApState = mWifiManager.getWifiApState();
        if (isChecked && ((wifiApState == WifiManager.WIFI_AP_STATE_ENABLING) ||
                (wifiApState == WifiManager.WIFI_AP_STATE_ENABLED))) {
            mWifiManager.setWifiApEnabled(null, false);
        }
        // MetricsLogger.action(mContext,
        //         isChecked ? MetricsLogger.ACTION_WIFI_ON : MetricsLogger.ACTION_WIFI_OFF);
        if (!mWifiManager.setWifiEnabled(isChecked)) {
            // Error
//            mSwitchBar.setEnabled(true);
            mWifiEnabler.setEnabled(true);
            int errId = mContext.getResources().getIdentifier("wifi_error", "strings", mContext.getPackageName());
            Toast.makeText(mContext, errId, Toast.LENGTH_SHORT).show();
        }

        return true;
    }
}
