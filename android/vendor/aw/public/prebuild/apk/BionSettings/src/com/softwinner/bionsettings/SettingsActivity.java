package com.softwinner.bionsettings;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.softwinner.bionsettings.R;
import com.softwinner.bionsettings.common.CommonSettings;
import com.softwinner.bionsettings.driving.DrivingSettings;
import com.softwinner.bionsettings.navi.NaviSettings;
import com.softwinner.bionsettings.system.SystemSettings;
import com.softwinner.bionsettings.version.VersionSettings;
import com.softwinner.bionsettings.reverb.ReverbSettings;
import com.softwinner.bionsettings.wifi.WifiSettings;

import java.util.List;

public class SettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_settings, target);
    }

    @Override
    public boolean onIsMultiPane() {
        return true;
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || CommonSettings.class.getName().equals(fragmentName)
                || DrivingSettings.class.getName().equals(fragmentName)
                || NaviSettings.class.getName().equals(fragmentName)
                || SystemSettings.class.getName().equals(fragmentName)
                || VersionSettings.class.getName().equals(fragmentName)
                || ReverbSettings.class.getName().equals(fragmentName)
                || WifiSettings.class.getName().equals(fragmentName);
    }
}
