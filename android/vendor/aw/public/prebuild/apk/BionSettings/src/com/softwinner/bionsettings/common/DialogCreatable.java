package com.softwinner.bionsettings.common;

import android.app.Dialog;

/**
 * @author Zhiwen, Zhong
 * @time 2017/4/28 14:30
 * @mail zhongzhiwen24@gmail.com
 */

public interface DialogCreatable {
    Dialog onCreateDialog(int dialogId);
}
