package com.softwinner.bionsettings.wifi;

import com.softwinner.bionsettings.R;
import com.android.settingslib.wifi.AccessPoint;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

class WifiDialog extends AlertDialog implements WifiConfigUiBase {
    static final int BUTTON_SUBMIT = DialogInterface.BUTTON_POSITIVE;
    static final int BUTTON_FORGET = DialogInterface.BUTTON_NEUTRAL;

    private final boolean mEdit;
    private final boolean mModify;
    private final DialogInterface.OnClickListener mListener;
    private final AccessPoint mAccessPoint;

    private View mView;
    private WifiConfigController mController;
    private boolean mHideSubmitButton;
    private boolean mHideForgetButton;

    public WifiDialog(Context context, DialogInterface.OnClickListener listener,
                      AccessPoint accessPoint, boolean edit, boolean modify,
                      boolean hideSubmitButton, boolean hideForgetButton) {
        this(context, listener, accessPoint, edit, modify);
        mHideSubmitButton = hideSubmitButton;
        mHideForgetButton = hideForgetButton;
    }

    public WifiDialog(Context context, DialogInterface.OnClickListener listener,
                      AccessPoint accessPoint, boolean edit, boolean modify) {
        super(context);
        mEdit = edit;
        mModify = modify;
        mListener = listener;
        mAccessPoint = accessPoint;
        mHideSubmitButton = false;
        mHideForgetButton = false;
    }

    @Override
    public WifiConfigController getController() {
        return mController;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mView = getLayoutInflater().inflate(R.layout.wifi_dialog, null);
        setView(mView);
        setInverseBackgroundForced(true);
        mController = new WifiConfigController(this, mView, mAccessPoint, mEdit, mModify);
        super.onCreate(savedInstanceState);

        if (mHideSubmitButton) {
            mController.hideSubmitButton();
        } else {
            /* During creation, the submit button can be unavailable to determine
             * visibility. Right after creation, update button visibility */
            // mController.enableSubmitIfAppropriate();
        }

        if (mHideForgetButton) {
            mController.hideForgetButton();
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mController.updatePassword();
    }

    @Override
    public boolean isEdit() {
        return mEdit;
    }

    @Override
    public Button getSubmitButton() {
        return getButton(BUTTON_SUBMIT);
    }

    @Override
    public Button getForgetButton() {
        return getButton(BUTTON_FORGET);
    }

    @Override
    public Button getCancelButton() {
        return getButton(BUTTON_NEGATIVE);
    }

    @Override
    public void setSubmitButton(CharSequence text) {
        setButton(BUTTON_SUBMIT, text, mListener);
    }

    @Override
    public void setForgetButton(CharSequence text) {
        setButton(BUTTON_FORGET, text, mListener);
    }

    @Override
    public void setCancelButton(CharSequence text) {
        setButton(BUTTON_NEGATIVE, text, mListener);
    }
}
