package com.softwinner.bionsettings.reverb;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.softwinner.bionsettings.R;

/**
 * @author zhongzhiwen
 * @date 2017/5/25
 * @mail zhongzhiwen24@gmail.com
 */

public class ReverbSettings extends PreferenceFragment{
    private ListPreference reverbPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_reverb);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        reverbPreference = (ListPreference) findPreference("reverb_list");
        MediaPlayer player = new MediaPlayer();
        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
        Equalizer mEqualizer = new Equalizer(0, player.getAudioSessionId());
        mEqualizer.setEnabled(true);

        int number = mEqualizer.getNumberOfPresets();
        CharSequence[] reverbNames = new CharSequence[number];
        CharSequence[] reverbValues = new CharSequence[number];
        for (short i = 0; i < number; i++) {
            Log.d("zhongzhiwen", "------------" + i);
            reverbNames[i] = mEqualizer.getPresetName(i);
            reverbValues[i] = String.valueOf(i);
        }
        
        reverbPreference.setEntries(reverbNames);
        reverbPreference.setEntryValues(reverbValues);

        reverbPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.d("zhongzhiwen", "--------------" + newValue.toString());
                return false;
            }
        });
    }
}
