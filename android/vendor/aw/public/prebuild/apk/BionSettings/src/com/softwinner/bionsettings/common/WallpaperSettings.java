package com.softwinner.bionsettings.common;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.softwinner.bionsettings.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 11:57
 * @mail zhongzhiwen24@gmail.com
 */

public class WallpaperSettings extends PreferenceFragment{
    private ArrayList<String> nameList;
    private HashMap<String, Intent> intentList;

    public WallpaperSettings() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_wallpaper);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        populateWallpaperTypes();

        for (String name : nameList) {
            Preference pref = new Preference(getActivity());
            pref.setTitle(name);
            pref.setIntent(intentList.get(name));
            getPreferenceScreen().addPreference(pref);
        }
    }

    private void populateWallpaperTypes() {
        // Search for activities that satisfy the ACTION_SET_WALLPAPER action
        final Intent intent = new Intent(Intent.ACTION_SET_WALLPAPER);
        final PackageManager pm = getActivity().getPackageManager();
        final List<ResolveInfo> rList = pm.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);

        nameList = new ArrayList<>();
        intentList = new HashMap<>();
        // Add Preference items for each of the matching activities
        for (ResolveInfo info : rList) {
            CharSequence label = info.loadLabel(pm);
            if (label == null) {
                label = info.activityInfo.packageName;
            }
            nameList.add(label.toString());

            Intent prefIntent = new Intent(intent);
            prefIntent.setComponent(new ComponentName(
                    info.activityInfo.packageName, info.activityInfo.name));
            intentList.put(label.toString(), prefIntent);
        }
    }
}
