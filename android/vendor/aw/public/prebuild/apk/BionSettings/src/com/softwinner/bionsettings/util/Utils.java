package com.softwinner.bionsettings.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.UserManager;
import android.preference.PreferenceFrameLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TabWidget;

import com.softwinner.bionsettings.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Zhiwen, Zhong
 * @time 2017/4/19 14:25
 * @mail zhongzhiwen24@gmail.com
 */

public final class Utils {
    private static final String LOG_TAG = "Utils";
    private static final String BAIDU_MAP_PACKAGE_NAME = "com.baidu.BaiduMap"; //百度
    private static final String AMAP_PACKAGE_NAME = "com.autonavi.minimap";  // 高德
    private static final String TECENT_MAP_PACKAGE_NAME = "com.tencent.map"; // 腾讯
    private static final String GOOGLE_MAP_PACKAGE_NAME = "com.google.android.apps.maps"; // 谷歌
    private static final HashMap<String, String> naviNamesMap = new HashMap<>();

    static {
        naviNamesMap.put("baidu", "百度地图");
        naviNamesMap.put("amap", "高德地图");
        naviNamesMap.put("tencent", "腾讯地图");
        naviNamesMap.put("google", "谷歌地图");
    }

    /**
     * 判断是否安装了某个软件
     */
    public static boolean isAppInstalled(Context context, String packageName) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Error: can not get package info for specified package");
            e.printStackTrace();
        }

        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    public static void forcePrepareCustomPreferencesList(
            ViewGroup parent, View child, ListView list, boolean ignoreSidePadding) {
        list.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        list.setClipToPadding(false);
        prepareCustomPreferencesList(parent, child, list, ignoreSidePadding);
    }

    /**
     * Prepare a custom preferences layout, moving padding to {@link ListView}
     * when outside scrollbars are requested. Usually used to display
     * {@link ListView} and {@link TabWidget} with correct padding.
     */
    public static void prepareCustomPreferencesList(
            ViewGroup parent, View child, View list, boolean ignoreSidePadding) {
        final boolean movePadding = list.getScrollBarStyle() == View.SCROLLBARS_OUTSIDE_OVERLAY;
        if (movePadding) {
            final Resources res = list.getResources();
            final int paddingSide = res.getDimensionPixelSize(R.dimen.settings_side_margin);
            final int paddingBottom = res.getDimensionPixelSize(
                    com.android.internal.R.dimen.preference_fragment_padding_bottom);

            if (parent instanceof PreferenceFrameLayout) {
                ((PreferenceFrameLayout.LayoutParams) child.getLayoutParams()).removeBorders = true;

                final int effectivePaddingSide = ignoreSidePadding ? 0 : paddingSide;
                list.setPaddingRelative(effectivePaddingSide, 0, effectivePaddingSide, paddingBottom);
            } else {
                list.setPaddingRelative(paddingSide, 0, paddingSide, paddingBottom);
            }
        }
    }

    /** Not global warming, it's global change warning. */
    public static Dialog buildGlobalChangeWarningDialog(final Context context, int titleResId,
                                                        final Runnable positiveAction) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleResId);
        int resId = context.getResources().getIdentifier("global_change_warning", "strings", 
            context.getPackageName());
        String info = context.getResources().getString(resId);
        builder.setMessage(info);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positiveAction.run();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);

        return builder.create();
    }

    public static boolean isWifiOnly(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        return (cm.isNetworkSupported(ConnectivityManager.TYPE_MOBILE) == false);
    }

    /**
     * 获取第三方导航软件信息
     * @return
     */
    public static List<String> getNaviData(Context context) {
        List<String> naviData = new ArrayList<>();

        // 判断是否安装了高德地图
        if (Utils.isAppInstalled(context, AMAP_PACKAGE_NAME)) {
            Log.d(LOG_TAG, "----------高德地图-----------");
            naviData.add("amap");
        } else {
            naviData.add(null);
        }

        // 判断是否安装了百度地图
        if (Utils.isAppInstalled(context, BAIDU_MAP_PACKAGE_NAME)) {
            Log.d(LOG_TAG, "----------百度地图-----------");
            naviData.add("baidu");
        } else {
            naviData.add(null);
        }

        // 判断是否安装了腾讯地图
        if (Utils.isAppInstalled(context, TECENT_MAP_PACKAGE_NAME)) {
            Log.d(LOG_TAG, "----------腾讯地图-----------");
            naviData.add("tencent");
        } else {
            naviData.add(null);
        }

        // 判断是否安装了谷歌地图
        if (Utils.isAppInstalled(context, GOOGLE_MAP_PACKAGE_NAME)) {
            Log.d(LOG_TAG, "----------谷歌地图-----------");
            naviData.add("google");
        } else {
            naviData.add(null);
        }

        if (naviData == null) {
            Log.d(LOG_TAG, "--------navidata is null-----");
        } else {
            Log.d(LOG_TAG, "--------navidata is not null fucking shit-----");
        }

        return naviData;
    }

    public static boolean hasMultipleUsers(Context context) {
        return ((UserManager) context.getSystemService(Context.USER_SERVICE))
                .getUsers().size() > 1;
    }

    public static String getNaviName(String navi) {
        return naviNamesMap.get(navi);
    }

    public static String getNaviCode(String naviName) {
        for (Map.Entry navi : naviNamesMap.entrySet()) {
            if (naviName.equals(navi.getValue())) {
                return (String) navi.getKey();
            }
        }

        return null;
    }

    /**
     * 获取系统版本
     * @return
     */
    public static String getSDKVersion() {
        return Build.VERSION.RELEASE;
    }

    public static void copyFile(String oldPath, String newPath) {
        if (oldPath == null || newPath == null) {
            return;
        }

        FileInputStream input = null;
        FileOutputStream output = null;
        try {
            input = new FileInputStream(oldPath);
            output = new FileOutputStream(newPath);
            byte[] buffer = new byte[1024];
            int length;
            if ((length = input.read(buffer)) != -1) {
                output.write(buffer, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void copyFile(File oldFile, File newFile) {
        if (oldFile == null || newFile == null) {
            return;
        }

        FileInputStream input = null;
        FileOutputStream output = null;
        try {
            input = new FileInputStream(oldFile);
            output = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            int length;
            if ((length = input.read(buffer)) != -1) {
                output.write(buffer, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void deleteFile(String path) {
        if (path == null) {
            return;
        }

        File file = new File(path);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }

    public static void deleteFile(File file) {
        if (file == null) {
            return;
        }

        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }
 }
