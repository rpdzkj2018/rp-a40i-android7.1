package com.flyound.launcher.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.flyound.launcher.R;
/**
 * @author zhongzhiwen
 * @date 2017/8/30
 * @mail zhongzhiwen24@gmail.com
 */

public class HomePagerAdapter extends PagerAdapter{
    private List<View> mViewList;

    public HomePagerAdapter(List<View> viewList) {
        if (viewList == null) {
            mViewList = new ArrayList<>();
        } else {
            Log.d("zhongzhiwen", "-------HomePagerAdapter--------");
            View view = viewList.get(0);
            View temp = view.findViewById(R.id.home_left_bottom);
            if (temp != null) {
                Log.d("zhongzhiwen", "-------temp is not null--------");
            } else {
                Log.d("zhongzhiwen", "-------temp is null--------");
            }
            mViewList = viewList;
        }
    }

    public void updateViews(List<View> viewList) {
        if (viewList != null) {
            mViewList = viewList;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        Log.d("zhongzhiwen", "-------HomePagerAdapter--------" + mViewList.size());
        return mViewList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mViewList.get(position));
        return mViewList.get(position);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mViewList.get(position));
    }
}
