package com.flyound.launcher.view;

import com.flyound.launcher.StateEventOfView;

import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class EventViewHelper {
	private static EventViewHelper sThis = new EventViewHelper();
	public static EventViewHelper getInstance(){
		return sThis;
	}
	class OnClickStateEventListener implements OnClickListener {
		StateEventOfView stateEvent;
		public OnClickStateEventListener(StateEventOfView stateEvent) {
			// TODO Auto-generated constructor stub
			this.stateEvent = stateEvent;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			handleEventType(v, stateEvent);
		}
	};
	
	class OnCheckStateChangeEventListener implements OnCheckedChangeListener{
		StateEventOfView stateEventOfChecked;
		StateEventOfView stateEventOfUnChecked;
		public OnCheckStateChangeEventListener(StateEventOfView stateEventOfChecked,
		StateEventOfView stateEventOfUnChecked) {
			// TODO Auto-generated constructor stub
			this.stateEventOfChecked = stateEventOfChecked;
			this.stateEventOfUnChecked = stateEventOfUnChecked;
		}
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			Log.i("OnCheckStateChangeEventListener","OnCheckStateChangeEventListener:isChecked:" + isChecked);
			if(isChecked && stateEventOfChecked != null){
				handleEventType(buttonView, stateEventOfChecked);
			}
			
			if(!isChecked && stateEventOfUnChecked != null){
				handleEventType(buttonView, stateEventOfUnChecked);
			}
		}
		public void setCheckedStateEvent(StateEventOfView stateEvent){
			this.stateEventOfChecked = stateEvent;
		}
		
		public void setUnCheckedStateEvent(StateEventOfView stateEvent){
			this.stateEventOfUnChecked = stateEvent;
		}
	}
	
	class OnCreateViewEventListener{
		StateEventOfView stateEvent;
		public OnCreateViewEventListener(StateEventOfView stateEvent){
			this.stateEvent = stateEvent;
		}
		
		public void onCreate(View v){
			handleEventType(v, stateEvent);
		}
	}
	
	public Intent createIntentFromEventData(Intent intent,String eventData){
		String[] datas = eventData.split(",");
		if(datas.length > 0){
			if(intent == null){
				intent = new Intent();
			}	       
			
			for(int i=0; i<datas.length; i++){
				String[] data = datas[i].split(":");
				String key = data[0];
				String valueType = data[1];
				String value = data[2];
				if(key.equalsIgnoreCase("EDComponentName")){
					ComponentName component = new ComponentName(valueType, value);
					intent.setComponent(component);
				}else if(key.equalsIgnoreCase("EDAction")){
					intent.setAction(value);
				}else if(key.equalsIgnoreCase("EDFlag")){
					intent.setFlags(Integer.parseInt(value,16));
				}else if(key.contains("EDCategory")){
					intent.addCategory(value);
				}
				else{
					if(valueType.equalsIgnoreCase("string")){
						intent.putExtra(key, value);
					}else if(valueType.equalsIgnoreCase("int")){
						intent.putExtra(key, Integer.parseInt(value));
					}else if(valueType.equalsIgnoreCase("float")){
						intent.putExtra(key, Float.parseFloat(value));
					}else if(valueType.equalsIgnoreCase("double")){
						intent.putExtra(key, Double.parseDouble(value));
					}
				}
			}
		}
		return intent;
	}
	
	public void handleEventType(View view,StateEventOfView stateEvent){
		if(stateEvent != null){
			if(stateEvent.eventType.equals("startActivity")){
				Log.d("zhongzhiwen", "-----handleEventType------" + stateEvent.stateName);
				Log.d("zhongzhiwen", "-----handleEventType------" + stateEvent.eventData);
				Intent intent = new Intent();				 
				intent = createIntentFromEventData(intent,stateEvent.eventData);
				if(intent.getAction() == null || intent.getAction().length() <= 0){
					intent.setAction(Intent.ACTION_MAIN);
				}
				if(Intent.ACTION_MAIN.equals(intent.getAction())){
					intent.addCategory(Intent.CATEGORY_LAUNCHER);
			        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
				}
				view.getContext().startActivity(intent);
			}else if(stateEvent.eventType.equals("startService")){
				Intent intent = createIntentFromEventData(null,stateEvent.eventData);
				view.getContext().startService(intent);
			}else if(stateEvent.eventType.equals("sendBroadcast")){
				Intent intent = createIntentFromEventData(null,stateEvent.eventData);
				view.getContext().sendBroadcast(intent);				
			}
		}
	}
}
