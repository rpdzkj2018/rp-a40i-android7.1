package com.flyound.launcher.bean;

/**
 * @author zhongzhiwen
 * @date 2017/9/1
 * @mail zhongzhiwen24@gmail.com
 */

public class AppItem {
    public String pkg;
    public String cls;
    public String title;

    public AppItem(String pkg, String cls, String title) {
    	this.pkg = pkg;
    	this.cls = cls;
    	this.title = title;
    }
}
