package com.flyound.launcher.view;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flyound.launcher.CounterTextView;
import com.flyound.launcher.R;
import com.flyound.launcher.StateEventOfView;
import com.flyound.launcher.view.EventViewHelper.OnClickStateEventListener;
import com.mesada.util.JSONUtil;

import android.widget.ImageView;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class EventImageView extends ImageView {
	private static final String TAG = EventTextView.class.getSimpleName();
	String eventDefineStr;
	OnClickStateEventListener onClickStateEventListener = null;
	
	
	public EventImageView(Context context){
		super(context,null,0);
		paserAttribute(context, null,0);
		init();
	}
	public EventImageView(Context context, AttributeSet attrs){
		super(context,attrs,0);
		paserAttribute(context, attrs, 0);
		init();
	}
	public EventImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		paserAttribute(context, attrs, defStyle);
		init();
	}
	private void paserAttribute(Context context,AttributeSet attrs, int defStyle){
		TypedArray a=context.obtainStyledAttributes(attrs,R.styleable.EventTextView,defStyle,0);
		eventDefineStr = a.getString(R.styleable.EventTextView_eventDefine);
		Log.i(TAG,"eventDefineStr:" + eventDefineStr);
		a.recycle();
		
	}
	
	private void init(){
		
		if(eventDefineStr != null){
			JSONObject eventJsonObj = JSONUtil.createJSONObject(eventDefineStr);
			if(eventJsonObj != null){
				JSONArray stateEventsJsonArray = JSONUtil.getJSONArray(eventJsonObj, "stateEvents");
				if(stateEventsJsonArray != null){
					for(int i=0; i< stateEventsJsonArray.length(); i++){
						StateEventOfView stateEvent = StateEventOfView.createFrom(JSONUtil.getJSONObject(stateEventsJsonArray,i));
						if(stateEvent.stateName.equals("onClick")){
							onClickStateEventListener = EventViewHelper.getInstance().new OnClickStateEventListener(stateEvent);
							setOnClickListener(onClickStateEventListener);
							Log.i(TAG,"setOnClickListener: eventDefine" + eventDefineStr);
						}
					}
				}
			}
		}
		
	}

	
	public OnClickStateEventListener getOnClickStateEventListener(){
		return onClickStateEventListener;
	}
	@Override
    protected void drawableStateChanged() {
    	super.drawableStateChanged();
    		
//		if(this.isPressed()) {
//			setAlpha(0.6f);
//		} else {
//			setAlpha(1.0f);
//		}	
		
		invalidate();
    }
}
