package com.flyound.launcher;

import org.json.JSONObject;

import com.mesada.util.JSONUtil;

public class StateEventOfView {
	public String stateName;
	public String eventType;
	public String eventData;

	public static StateEventOfView createFrom(JSONObject obj) {
		// TODO Auto-generated constructor stub
		if(obj != null){
			StateEventOfView stateEvent = new StateEventOfView();
			stateEvent.stateName = JSONUtil.getString(obj,"stateName","");
			stateEvent.eventType = JSONUtil.getString(obj, "eventType", "");
			stateEvent.eventData = JSONUtil.getString(obj, "eventData", "");
			return stateEvent;
		}
		return null;
	}
	
}
