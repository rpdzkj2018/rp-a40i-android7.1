LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    com_softwinner_AWDisplay.cpp

LOCAL_SHARED_LIBRARIES := \
    libandroid_runtime \
    libnativehelper \
    libcutils \
    libutils \
    libbinder \
    liblog \
    libdisplayservice

LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE) \

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libdisplay_jni
LOCAL_PRELINK_MODULE:= false

include $(BUILD_SHARED_LIBRARY)

