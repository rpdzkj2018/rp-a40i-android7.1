LOCAL_PATH:= $(call my-dir)

# merge all aw code into one jar
# ============================================================
include $(CLEAR_VARS)

LOCAL_MODULE := bms_src

LOCAL_SRC_FILES := $(call all-java-files-under,java)
LOCAL_SRC_FILES += \
        java/android/aw/IBackgroundManager.aidl

# Uncomment to enable output of certain warnings (deprecated, unchecked)
# LOCAL_JAVACFLAGS := -Xlint

LOCAL_PROGUARD_ENABLED := disabled
LOCAL_JACK_ENABLED := disabled

include $(BUILD_STATIC_JAVA_LIBRARY)
