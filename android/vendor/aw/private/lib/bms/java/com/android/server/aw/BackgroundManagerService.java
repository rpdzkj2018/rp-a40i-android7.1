package com.android.server.aw;

import android.app.ActivityManager;
import android.app.INotificationManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.aw.IBackgroundManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Slog;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;

import com.android.internal.R;
import com.android.internal.util.MemInfoReader;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @hide
 */
public final class BackgroundManagerService extends IBackgroundManager.Stub {
    private static final String TAG = "BackgroundManagerService";
    private static final boolean DEBUG = true;

    private Context mContext;
    private final MainHandler mHandler;
    private ActivityManager mActivityManager;

    private static final int NOTIFY_CHECK_BACKGROUND_TASK_MSG = 9527;
    private static final int NOTIFY_EXECUTE_KILL_BACKGROUND_TASK_MSG = 9528;
    private static final int NOTIFY_EXECUTE_KILL_BACKGROUND_COUNT_MSG = 9529;

    private static final long BACKGROUND_TASK_IDLE_TIME = 1 * 60 * 1000; // 1min

    private ArrayList<String> mBootCompletedBroadcastWhitelist = null;
    private boolean mKillBackgroundServices = false;
    private int mBackgroundServicesCount = 0;
    private ArrayList<String> mBackgroundServicesWhitelist = null;
    private ArrayList<String> mBackgroundServicesBlacklist = null;
    private ArrayList<String> mInputMethodList = null;
    private ArrayList<String> mBgmusicList = null;
    private Map<String, Integer> mBgmusicMap = null;
    private Map<String, Long> mRecentPackasges = new HashMap<String, Long>(20);

    /** for observer the settings of Settings.System.KILL_BACKGROUND_SERVICES_LIST */
    private SettingsObserver mSettingsObserver;

    /**
     * main handler
     */
    final class MainHandler extends Handler {
        public MainHandler(Looper looper) {
            super(looper, null, true);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case NOTIFY_CHECK_BACKGROUND_TASK_MSG: {
                if (!mKillBackgroundServices) return;
                long now = System.currentTimeMillis();
                MemInfoReader mMemInfoReader = new MemInfoReader();
                mMemInfoReader.readMemInfo();
                final long freeMem = mMemInfoReader.getFreeSize() + mMemInfoReader.getCachedSize();
                ActivityManager.MemoryInfo info = new ActivityManager.MemoryInfo();
                mActivityManager.getMemoryInfo(info);
                final long threshold = info.threshold;
                if (DEBUG) Slog.d(TAG, "readMemInfo: freeMem=" + freeMem + ",threshold=" + threshold);
                notifyKillBackgroundServicesListIfNecessary(freeMem, threshold);
                long idleTimeLimit = getIdleTimeLimit(freeMem, threshold);
                executeKillBackgroundTask(idleTimeLimit);
                // if (DEBUG) Slog.d(TAG, "NOTIFY_CHECK_BACKGROUND_TASK_MSG spends " + (System.currentTimeMillis() - now) + "ms");
                checkBackgroundTaskDelay(BACKGROUND_TASK_IDLE_TIME);
            } break;
            case NOTIFY_EXECUTE_KILL_BACKGROUND_TASK_MSG: {
                executeKillBackgroundTask(-1);
            } break;
            case NOTIFY_EXECUTE_KILL_BACKGROUND_COUNT_MSG: {
                executeKillBackgroundCount(mBackgroundServicesCount);
            } break;
            }
        }
    };

    public BackgroundManagerService(Context context) {
        mContext = context;
        mActivityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);

        HandlerThread hthread = new HandlerThread(TAG,
                android.os.Process.THREAD_PRIORITY_FOREGROUND);
        hthread.start();
        mHandler = new MainHandler(hthread.getLooper());
    }

    /**
     * system ready
     */
    public void systemReady() {
        final ContentResolver resolver = mContext.getContentResolver();
        final Resources resources = mContext.getResources();
        String[] broadcastWhitelist = resources.getStringArray(
                R.array.boot_completed_broadcast_whitelist);
        boolean killBackgroundServices = Settings.System.getInt(
                resolver, Settings.System.KILL_BACKGROUND_SERVICES, 0) != 0;
        String[] whitelist = resources.getStringArray(
                R.array.background_services_whitelist);

        synchronized (this) {
            mBootCompletedBroadcastWhitelist = new ArrayList<String>();
            if (broadcastWhitelist != null) {
                for (String item : broadcastWhitelist) {
                    mBootCompletedBroadcastWhitelist.add(item);
                }
            }
            mKillBackgroundServices = killBackgroundServices;
            mBackgroundServicesWhitelist = new ArrayList<String>();
            if (whitelist != null) {
                for (String item : whitelist) {
                    mBackgroundServicesWhitelist.add(item);
                }
            }
            mBackgroundServicesBlacklist = new ArrayList<String>();
            mInputMethodList = new ArrayList<String>();
            mBgmusicList = new ArrayList<String>();
            mBgmusicMap = new HashMap<String, Integer>();
        }

        // register package receiver
        IntentFilter appsFilter = new IntentFilter();
        appsFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        appsFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        appsFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        appsFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        appsFilter.addDataScheme("package");
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                reloadInputMethodList();
            }
        }, appsFilter);
        // register music receiver
        IntentFilter musicFilter = new IntentFilter();
        musicFilter.addAction("com.android.bgmusic");
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String key = intent.getStringExtra("key");
                int pid = intent.getIntExtra("pid", -1);
                boolean playing = intent.getBooleanExtra("playing", false);
                updateBgmusicList(key, pid, playing);
            }
        }, musicFilter);
        // add settings observer
        mSettingsObserver = new SettingsObserver(mHandler);
        resolver.registerContentObserver(Settings.System.getUriFor(
                Settings.System.KILL_BACKGROUND_SERVICES_LIST),
                false, mSettingsObserver, UserHandle.USER_ALL);
        // reload list
        reloadInputMethodList();
        reloadKillBackgroundServiceslist();
        resolver.registerContentObserver(Settings.System.getUriFor(
                Settings.System.BACKGROUND_SERVICES_LIMIT_COUNT),
                false, mSettingsObserver, UserHandle.USER_ALL);
        reloadBackgroundServicesLimitCount();
    }

    /**
     * settings observer for KILL_BACKGROUND_SERVICES_LIST
     */
    private final class SettingsObserver extends ContentObserver {
        public SettingsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            if(Settings.System.getUriFor(
                Settings.System.KILL_BACKGROUND_SERVICES_LIST).equals(uri)) {
                reloadKillBackgroundServiceslist();
            } else if (Settings.System.getUriFor(
                Settings.System.BACKGROUND_SERVICES_LIMIT_COUNT).equals(uri)) {
                reloadBackgroundServicesLimitCount();
            }
        }
    }

    private void reloadBackgroundServicesLimitCount() {
        final ContentResolver resolver = mContext.getContentResolver();
        mBackgroundServicesCount = Settings.System.getInt(
                resolver, Settings.System.BACKGROUND_SERVICES_LIMIT_COUNT, 0);
    }

    /**
     * reload kill background services list
     */
    private void reloadKillBackgroundServiceslist() {
        final ContentResolver resolver = mContext.getContentResolver();
        String killString = Settings.System.getString(
                resolver, Settings.System.KILL_BACKGROUND_SERVICES_LIST);
        synchronized (mBackgroundServicesBlacklist) {
            mBackgroundServicesBlacklist.clear();
            if (killString != null && killString.length() > 0) {
                String[] list = killString.split(",");
                for (String item : list) {
                    mBackgroundServicesBlacklist.add(item);
                }
            }
        }
    }

    /**
     * reload inputmethod list
     */
    private void reloadInputMethodList() {
        if (mInputMethodList == null) return;
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        List<InputMethodInfo> imis = imm.getInputMethodList();
        synchronized (mInputMethodList) {
            mInputMethodList.clear();
            for (InputMethodInfo info : imis) {
                mInputMethodList.add(info.getPackageName());
            }
        }
    }

    /**
     * update background music list
     */
    private void updateBgmusicList(String key, int pid, boolean playing) {
        if (mBgmusicList == null || mBgmusicMap == null) return;
        if (pid <= 0) return;
        List<ActivityManager.RunningAppProcessInfo> runningApps = mActivityManager.getRunningAppProcesses();
        Set<Integer> alivePids = new HashSet<Integer>();
        Set<String> pkgList = new HashSet<String>();
        synchronized (mBgmusicMap) {
            if (playing) mBgmusicMap.put(key, pid);
            else mBgmusicMap.remove(key);
            if (runningApps != null && runningApps.size() > 0) {
                for (ActivityManager.RunningAppProcessInfo app : runningApps) {
                    if (mBgmusicMap.containsValue(app.pid)) {
                        alivePids.add(app.pid);
                        if (app.pkgList != null) {
                            for (String pkg : app.pkgList) {
                                pkgList.add(pkg);
                            }
                        }
                    }
                }
            }
        }
        synchronized (mBgmusicList) {
            mBgmusicList.clear();
            mBgmusicList.addAll(pkgList);
        }
    }

    /**
     * notify list settings
     */
    private void notifyKillBackgroundServicesList() {
        INotificationManager inm = NotificationManager.getService();
        if (inm == null) {
            return;
        }
        final Intent intent = new Intent("android.settings.BACKGROUND_CLEANUP_LIST_SETTINGS");
        final PendingIntent pendingIntent = PendingIntent.getActivityAsUser(mContext, 0,
                intent, PendingIntent.FLAG_ONE_SHOT, null, UserHandle.CURRENT);
        Resources r = mContext.getResources();
        Notification notification = new Notification.Builder(mContext)
                .setSmallIcon(R.drawable.stat_background_cleanup)
                .setContentTitle(r.getString(R.string.background_cleanup_notification_title))
                .setContentText(r.getString(R.string.background_cleanup_notification_message))
                .setColor(mContext.getColor(R.color.system_notification_accent_color))
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        try {
            int[] outId = new int[1];
            inm.enqueueNotificationWithTag("android", "android", null,
                    R.string.background_cleanup_notification_title,
                    notification, outId, UserHandle.USER_ALL);
        } catch (RuntimeException e) {
            Slog.w(TAG, "Error showing notification for dump heap", e);
        } catch (RemoteException e) {
        }
    }

    /**
     * notify list settings if necessary
     */
    private void notifyKillBackgroundServicesListIfNecessary(final long freeMem, final long threshold) {
        if (!mKillBackgroundServices) return;
        if (freeMem < threshold / 2) {
            boolean notify = false;
            synchronized (mRecentPackasges) {
                Iterator<Map.Entry<String, Long>> it = mRecentPackasges.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Long> entry = it.next();
                    String packageName = entry.getKey();
                    if (allowBackgroundTask(packageName)) {
                        notify = true;
                        break;
                    }
                }
            }
            if (notify)
                notifyKillBackgroundServicesList();
        }
    }

    /**
     * get idle time limit
     */
    private long getIdleTimeLimit(final long freeMem, final long threshold) {
        if (freeMem > threshold / 2) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * check package is in whitelist
     * including system whitelist, inputmethod, background music.
     */
    private boolean isWhitelistBackgroundTask(String packageName) {
        if (!mKillBackgroundServices) return true;
        if (mBackgroundServicesWhitelist == null) return false;
        synchronized (mBackgroundServicesWhitelist) {
            for (String pkg : mBackgroundServicesWhitelist) {
                if (packageName.startsWith(pkg))
                    return true;
            }
        }
        if (mInputMethodList != null)
            synchronized (mInputMethodList) {
                if (mInputMethodList.contains(packageName))
                    return true;
            }
        if (mBgmusicList != null)
            synchronized (mBgmusicList) {
                if (mBgmusicList.contains(packageName))
                    return true;
            }
        return false;
    }

    /**
     * check if allow background task
     * including whitelist and user custom settings
     * @param packageName package name
     */
    public boolean allowBackgroundTask(String packageName) {
        if (!mKillBackgroundServices) return true;
        if (isWhitelistBackgroundTask(packageName)) {
            return true;
        } else {
            if (mBackgroundServicesBlacklist == null) return false;
            synchronized (mBackgroundServicesBlacklist) {
                return mBackgroundServicesBlacklist.contains(packageName);
            }
        }
    }

    /**
     * execute kill background task
     * idleTimeLimit=-1 to kill all immediately
     */
    private void executeKillBackgroundTask(long idleTimeLimit) {
        if (!mKillBackgroundServices) return;
        if (idleTimeLimit == 0) return;
        long now = System.currentTimeMillis();
        Set<String> pkgList = new HashSet<String>();
        synchronized (mRecentPackasges) {
            Iterator<Map.Entry<String, Long>> it = mRecentPackasges.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Long> entry = it.next();
                long time = entry.getValue();
                if (time < 0) {
                    continue;
                } else { // running background services
                    String packageName = entry.getKey();
                    if (DEBUG) Slog.i(TAG, packageName + " idle " + (now - time) + "ms");
                    if (allowBackgroundTask(packageName)) {
                        entry.setValue(now);
                    } else if (now - time > idleTimeLimit) {
                        if (DEBUG) Slog.i(TAG, "forceStopPackage: " + packageName + " idle " + (now - time) + "ms");
                        pkgList.add(packageName);
                        it.remove();
                    }
                }
            }
        }
        for (String packageName : pkgList) {
            mActivityManager.forceStopPackage(packageName);
        }
    }

    private void executeKillBackgroundCount(int count) {
        List<ActivityManager.RunningServiceInfo> services = mActivityManager.getRunningServices(100);
        Set<String> set = new HashSet<String>();
        for (ActivityManager.RunningServiceInfo info : services) {
            String packageName = info.service.getPackageName();
            if (count == 0) {
                if (isWhitelistBackgroundTask(packageName)) continue;
            } else {
                if (allowBackgroundTask(packageName)) continue;
            }
            if (isTopActivity(count, packageName)) continue;
            set.add(packageName);
        }
        for (String packageName : set) {
            if (DEBUG) Slog.i(TAG, "force stop service: " + packageName);
            mActivityManager.forceStopPackage(packageName);
        }
    }

    private boolean isTopActivity(int count, String packageName) {
        List<ActivityManager.RunningTaskInfo> list = mActivityManager.getRunningTasks(count + 1);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (info.baseActivity.getPackageName().equals(packageName))
                return true;
        }
        return false;
    }

    /**
     * kill background task immediately
     */
    public void killBackgroundTaskImmediately() {
        mHandler.removeMessages(NOTIFY_EXECUTE_KILL_BACKGROUND_TASK_MSG);
        Message msg = mHandler.obtainMessage(NOTIFY_EXECUTE_KILL_BACKGROUND_TASK_MSG);
        mHandler.sendMessage(msg);
    }

    /**
     * check background task delay
     */
    private void checkBackgroundTaskDelay(long delayMillis) {
        mHandler.removeMessages(NOTIFY_CHECK_BACKGROUND_TASK_MSG);
        Message msg = mHandler.obtainMessage(NOTIFY_CHECK_BACKGROUND_TASK_MSG);
        mHandler.sendMessageDelayed(msg, delayMillis);
    }

    /**
     * manager package and auto kill when unuse in background
     * @param packageName Package to manager
     * @param timestamp Timestamp of switching into background, and foreground is -1
     */
    public void managerPackage(String packageName, long timestamp) {
        if (!mKillBackgroundServices) return;
        mHandler.removeMessages(NOTIFY_EXECUTE_KILL_BACKGROUND_COUNT_MSG);
        Message msg = mHandler.obtainMessage(NOTIFY_EXECUTE_KILL_BACKGROUND_COUNT_MSG);
        mHandler.sendMessageDelayed(msg, 500L);
        if (isWhitelistBackgroundTask(packageName)) return;
        synchronized(mRecentPackasges) {
            mRecentPackasges.put(packageName, timestamp);
            checkBackgroundTaskDelay(5000L);
        }
    }

    /**
     * resolver receivers and remove not in whitelist
     * @param Intent target intent
     * @param receivers resolver receivers
     */
    public void resolverReceiver(Intent intent, List receivers) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            if (mBootCompletedBroadcastWhitelist != null && mBootCompletedBroadcastWhitelist.size() > 0) {
                int NT = receivers.size();
                for (int it=0; it<NT; it++) {
                    ResolveInfo curt = (ResolveInfo)receivers.get(it);
                    if (!mBootCompletedBroadcastWhitelist.contains(curt.activityInfo.packageName)) {
                        Slog.i(TAG, "prevent from boot complete broadcast: " + curt.activityInfo.packageName);
                        receivers.remove(it);
                        it--;
                        NT--;
                    }
                }
            }
        }
    }

    /**
     * resolver receivers and remove not in whitelist
     * @param Intent target intent
     * @param receivers resolver receivers
     */
    public boolean skipService(Intent service) {
        // don't skip wallpaper service
        String action = service.getAction();
        if (action != null && "android.service.wallpaper.WallpaperService".equals(action)) {
            return false;
        }
        String packageName = service.getPackage();
        if (packageName == null && service.getComponent() != null) {
            packageName = service.getComponent().getPackageName();
        }
        if (packageName != null) {
            if (isTopActivity(mBackgroundServicesCount, packageName)) {
                return false;
            }
            if (mBackgroundServicesCount == 0) {
                if (isWhitelistBackgroundTask(packageName)) {
                    return false;
                }
            } else {
                if (allowBackgroundTask(packageName)) {
                    return false;
                }
            }
            if (DEBUG) Slog.i(TAG, "skipService " + service + " because of activity not started!");
            return true;
        }
        if (DEBUG) Slog.i(TAG, "Do not skip unhandled service " + service);
        return false;
    }

    boolean dumpWhitelistLocked(FileDescriptor fd, PrintWriter pw, String[] args) {
        pw.println("  whitelist:");
        pw.println("...." + mBackgroundServicesWhitelist);
        return true;
    }

    boolean dumpInputmethodLocked(FileDescriptor fd, PrintWriter pw, String[] args) {
        pw.println("  inputmethod:");
        pw.println("...." + mInputMethodList);
        return true;
    }

    boolean dumpBgmusicLocked(FileDescriptor fd, PrintWriter pw, String[] args) {
        pw.println("  bgmusic:");
        pw.println("...." + mBgmusicList);
        return true;
    }

    boolean dumpUserListLocked(FileDescriptor fd, PrintWriter pw, String[] args) {
        pw.println("  user:");
        pw.println("...." + mBackgroundServicesBlacklist);
        return true;
    }

    boolean dumpRecentPackagesLocked(FileDescriptor fd, PrintWriter pw, String[] args) {
        pw.println("  recent:");
        Iterator<Map.Entry<String, Long>> it = mRecentPackasges.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Long> entry = it.next();
            String packageName = entry.getKey();
            long time = entry.getValue();
            pw.println(String.format("|%-61s|%16d|", packageName, time));
        }
        return true;
    }

    boolean dumpRunningTaskLocked(FileDescriptor fd, PrintWriter pw, String[] args) {
        pw.println("  tasks:");
        List<ActivityManager.RunningTaskInfo> list = mActivityManager.getRunningTasks(10);
        for (ActivityManager.RunningTaskInfo info : list) {
            pw.println(info.baseActivity.getPackageName());
        }
        return true;
    }

    @Override
    protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (mContext.checkCallingOrSelfPermission(android.Manifest.permission.DUMP)
                != PackageManager.PERMISSION_GRANTED) {
            pw.println("Permission Denial: can't dump ActivityManager from from pid="
                    + Binder.getCallingPid()
                    + ", uid=" + Binder.getCallingUid()
                    + " without permission "
                    + android.Manifest.permission.DUMP);
            return;
        }

        if (args.length > 0) {
            String cmd = args[0];
            if ("whitelist".equals(cmd)) {
                dumpWhitelistLocked(fd, pw, args);
            } else if ("inputmethod".equals(cmd)) {
                dumpInputmethodLocked(fd, pw, args);
            } else if ("bgmusic".equals(cmd)) {
                dumpBgmusicLocked(fd, pw, args);
            } else if ("user".equals(cmd)) {
                dumpUserListLocked(fd, pw, args);
            } else if ("recent".equals(cmd)) {
                dumpRecentPackagesLocked(fd, pw, args);
            } else if ("tasks".equals(cmd)) {
                dumpRunningTaskLocked(fd, pw, args);
            }
            return;
        } else {
            pw.println("BackgroundManagerService:");
            pw.println("  mKillBackgroundServices=" + mKillBackgroundServices);
            pw.println("  mBackgroundServicesCount=" + mBackgroundServicesCount);
            pw.println("--------------------------------------------------------------------------------");
            dumpWhitelistLocked(fd, pw, args);
            pw.println("--------------------------------------------------------------------------------");
            dumpInputmethodLocked(fd, pw, args);
            pw.println("--------------------------------------------------------------------------------");
            dumpBgmusicLocked(fd, pw, args);
            pw.println("--------------------------------------------------------------------------------");
            dumpUserListLocked(fd, pw, args);
            //pw.println("--------------------------------------------------------------------------------");
            //dumpRecentPackagesLocked(fd, pw, args);
            pw.println("--------------------------------------------------------------------------------");
            dumpRunningTaskLocked(fd, pw, args);
            pw.println("--------------------------------------------------------------------------------");
        }
    }
}
