#ifndef __MEM_SRAM_SUN50IW3_H__
#define __MEM_SRAM_SUN50IW3_H__

#if defined CONFIG_ARCH_SUN50IW3P1

#define SRAM_REG_LENGTH		((0xec+0x4)>>2)

struct sram_state{
	unsigned int sram_reg_back[SRAM_REG_LENGTH];
};

#endif /* CONFIG_ARCH_SUN50IW3P1 */
#endif /* __MEM_SRAM_SUN50IW3_H__ */
