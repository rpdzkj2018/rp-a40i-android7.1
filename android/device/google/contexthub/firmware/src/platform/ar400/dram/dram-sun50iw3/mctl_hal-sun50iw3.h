#ifndef   _MCTL_HAL_H
#define   _MCTL_HAL_H
/**********************
global define
1.FPGA_VERIFY --- FOR FPGA VERIFY
2.IC_VERIFY --- FOR IC VERIFY
3.SYSTEM_VERIFY --- FOR SYSTEM VERIFY
**********************/
#define FPGA_VERIFY
//#define IC_VERIFY
//#define SYSTEM_VERIFY

#ifdef SYSTEM_VERIFY
/*boot set the .h*/
#include <common.h>
#include <asm/io.h>
#include <asm/arch/cpu.h>
#include <asm/arch/timer.h>
#include <asm/arch/ccmu.h>
#endif

/**********************
STANDBY DEFINE
**********************/
//#define CPUS_STANDBY_CODE
/**********************
MODULE FUNCTION
**********************/
#ifndef CPUS_STANDBY_CODE
//#define USE_PMU
//#define USE_CHIPID
//#define USE_SPECIAL_DRAM
//#define USE_CHEAP_DRAM
#endif

/**********************
AUTO SCAN DEFINE
**********************/
#ifndef CPUS_STANDBY_CODE
#define DRAM_AUTO_SCAN
#ifdef  DRAM_AUTO_SCAN
#define DRAM_TYPE_SCAN
#define DRAM_RANK_SCAN
#define DRAM_SIZE_SCAN


#endif
#endif
/**********************
PRINT DEFINE
**********************/
//#define DEBUG_LEVEL_1
//#define DEBUG_LEVEL_4
//#define DEBUG_LEVEL_8
//#define TIMING_DEBUG
//#define ERROR_DEBUG
//#define AUTO_DEBUG

#if defined DEBUG_LEVEL_8
#define dram_dbg_8(fmt,args...)	 printk(fmt ,##args)
#define dram_dbg_4(fmt,args...)	 printk(fmt ,##args)
#define dram_dbg_0(fmt,args...)  printk(fmt ,##args)
#elif defined DEBUG_LEVEL_4
#define dram_dbg_8(fmt,args...)
#define dram_dbg_4(fmt,args...)  printk(fmt ,##args)
#define dram_dbg_0(fmt,args...)  printk(fmt ,##args)
#elif defined DEBUG_LEVEL_1
#define dram_dbg_8(fmt,args...)
#define dram_dbg_4(fmt,args...)
#define dram_dbg_0(fmt,args...)  printk(fmt ,##args)
#else
#define dram_dbg_8(fmt,args...)
#define dram_dbg_4(fmt,args...)
#define dram_dbg_0(fmt,args...)
#endif

#if defined TIMING_DEBUG
#define dram_dbg_timing(fmt,args...)  printk(fmt ,##args)
#else
#define dram_dbg_timing(fmt,args...)
#endif

#if defined ERROR_DEBUG
#define dram_dbg_error(fmt,args...)  printk(fmt ,##args)
#else
#define dram_dbg_error(fmt,args...)
#endif


#if defined AUTO_DEBUG
#define dram_dbg_auto(fmt,args...)  printk(fmt ,##args)
#else
#define dram_dbg_auto(fmt,args...)
#endif

extern unsigned int mctl_core_init(dram_para_t *para);
extern unsigned int mctl_init(void);
extern signed int init_DRAM(int type, dram_para_t *para);
#endif  //_MCTL_HAL_H
