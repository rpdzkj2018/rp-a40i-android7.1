#ifndef __MEM_SMTA_H__
#define __MEM_SMTA_H__

#if defined CONFIG_ARCH_SUN8IW6P1
#define SMTA_REG_START       (0x4)
#define SMTA_REG_END         (0x24 + 0x04)
#define SMTA_SKIP            3
#define SMTA_REG_LENGTH      (((0x24 + 0x04)>>2)/SMTA_SKIP)
#elif (defined CONFIG_ARCH_SUN50IW1P1) || \
	(defined CONFIG_ARCH_SUN50IW2P1)
#define SMTA_REG_START       (0x4)
#define SMTA_REG_END         (0x48 + 0x04)
#define SMTA_SKIP            3
#define SMTA_REG_LENGTH      (((0x48 + 0x04)>>2)/SMTA_SKIP)
#elif (defined CONFIG_ARCH_SUN50IW6P1) || \
	(defined CONFIG_ARCH_SUN50IW3P1)
#define SMTA_REG_START       (0x0)
#define SMTA_REG_END         (0xd8 + 0x04)
#define SMTA_SKIP            3
#define SMTA_REG_LENGTH      (((0xd8 + 0x04)>>2)/SMTA_SKIP)
#endif

struct smta_regs{
	unsigned int smta_reg_back[SMTA_REG_LENGTH];
};

#endif /* __MEM_SMTA_H__ */
