include device/softwinner/common/BoardConfigCommon.mk
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a7

TARGET_BOARD_PLATFORM := a40
TARGET_BOARD_CHIP := sun8iw11p1
TARGET_BOOTLOADER_BOARD_NAME := exdroid
TARGET_BOOTLOADER_NAME := exdroid

BOARD_EGL_CFG := hardware/aw/egl/a40/egl.cfg
SW_CHIP_PLATFORM := A40
BOARD_KERNEL_BASE := 0x40000000
BOARD_MKBOOTIMG_ARGS := --kernel_offset 0x8000

#SurfaceFlinger's configs  Echo add from A64
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := true
TARGET_USES_HWC2 := false

BOARD_CHARGER_ENABLE_SUSPEND := true
SW_PLATFORM_KERNEL_VERSION := v3_10
TARGET_USES_ION := true

BOARD_SEPOLICY_DIRS := \
    device/softwinner/common/sepolicy \
    device/softwinner/a40-common/sepolicy

USE_OPENGL_RENDERER := true

BOARD_HAS_GPS ?= true

