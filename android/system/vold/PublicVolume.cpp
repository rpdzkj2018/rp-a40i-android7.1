/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fs/Vfat.h"
#include "fs/Exfat.h"
#include "fs/iso9660.h"
#include "fs/Ntfs.h"
#include "PublicVolume.h"
#include "Utils.h"
#include "VolumeManager.h"
#include "ResponseCode.h"

#include <android-base/stringprintf.h>
#include <android-base/logging.h>
#include <cutils/fs.h>
#include <private/android_filesystem_config.h>
#include "cutils/log.h"

#include <fcntl.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mount.h>

using android::base::StringPrintf;

namespace android {
namespace vold {

static const char* kFusePath = "/system/bin/sdcard";

static const char* kAsecPath = "/mnt/secure/asec";



PublicVolume::PublicVolume(dev_t device,const std::string& name) :
        VolumeBase(Type::kPublic), mDevice(device), mFusePid(0),mNickName(name) {
    setId(StringPrintf("public:%u:%u", major(device), minor(device)));
    mDevPath = StringPrintf("/dev/block/vold/%s", getId().c_str());
}

PublicVolume::PublicVolume(dev_t device) :
        VolumeBase(Type::kPublic), mDevice(device), mFusePid(0) {
    setId(StringPrintf("public:%u:%u", major(device), minor(device)));
    mDevPath = StringPrintf("/dev/block/vold/%s", getId().c_str());
}

PublicVolume::~PublicVolume() {
}

status_t PublicVolume::readMetadata() {
    status_t res = ReadMetadataUntrusted(mDevPath, mFsType, mFsUuid, mFsLabel);
	mTrueUuid = mFsUuid;
	if (!mNickName.empty()) 
		mFsUuid = mNickName;
    notifyEvent(ResponseCode::VolumeFsTypeChanged, mFsType);
    notifyEvent(ResponseCode::VolumeFsUuidChanged, mFsUuid);
    notifyEvent(ResponseCode::VolumeFsLabelChanged, mFsLabel);
    return res;
}

status_t PublicVolume::initAsecStage() {
    std::string legacyPath(mRawPath + "/android_secure");
    std::string securePath(mRawPath + "/.android_secure");

    // Recover legacy secure path
    if (!access(legacyPath.c_str(), R_OK | X_OK)
            && access(securePath.c_str(), R_OK | X_OK)) {
        if (rename(legacyPath.c_str(), securePath.c_str())) {
            PLOG(WARNING) << getId() << " failed to rename legacy ASEC dir";
        }
    }

    if (TEMP_FAILURE_RETRY(mkdir(securePath.c_str(), 0700))) {
        if (errno != EEXIST) {
            PLOG(WARNING) << getId() << " creating ASEC stage failed";
            return -errno;
        }
    }

    BindMount(securePath, kAsecPath);

    return OK;
}

status_t PublicVolume::doCreate() {
	mUsbLabelIndex = VolumeManager::MAX_VOLUMES - 1;   //default value!!!
    return CreateDeviceNode(mDevPath, mDevice);
}

status_t PublicVolume::doDestroy() {
    return DestroyDeviceNode(mDevPath);
}

status_t PublicVolume::doMount() {
    // TODO: expand to support mounting other filesystems
    readMetadata();    
	VolumeManager *p = VolumeManager::Instance();
	
/*
    if (mFsType != "vfat") {
        LOG(ERROR) << getId() << " unsupported filesystem " << mFsType;
        return -EIO;
    }

    if (vfat::Check(mDevPath)) {
        LOG(ERROR) << getId() << " failed filesystem check";
        return -EIO;
    }
*/
    // Use UUID as stable name, if available
    std::string stableName;
	
	if((mDevPath.find("public:8") != std::string::npos)||(mDevPath.find("public:179") != std::string::npos))
	{				
		int index = 0;
		if(!strcmp(mFsUuid.c_str(),"udisk"))		
			{
				while (p->volumeUsb0Bitmap[index])
			    {
			        index++;
			        if (index==	VolumeManager::MAX_VOLUMES)
			            break;
			    }

			    if (index == VolumeManager::MAX_VOLUMES)
			    {
			        
			        PLOG(ERROR) << getId() << "usb0 full volume! can't create new one";
			    }
			    else
			    {
					mUsbLabelIndex = index;
			    }
			}
		if(!strcmp(mFsUuid.c_str(),"udiskh"))		
			{
				while (p->volumeUsb1Bitmap[index])
			    {
			        index++;
			        if (index==	VolumeManager::MAX_VOLUMES)
			            break;
			    }

			    if (index == VolumeManager::MAX_VOLUMES)
			    {
			        
			        PLOG(ERROR) << getId() << "usb1 full volume! can't create new one";
			    }
			    else
			    {
					mUsbLabelIndex = index;
			    }
			}
		if(!strcmp(mFsUuid.c_str(),"udisk3"))		
			{
				while (p->volumeUsb2Bitmap[index])
			    {
			        index++;
			        if (index==	VolumeManager::MAX_VOLUMES)
			            break;
			    }

			    if (index == VolumeManager::MAX_VOLUMES)
			    {
			        
			        PLOG(ERROR) << getId() << "usb2 full volume! can't create new one";
			    }
			    else
			    {
					mUsbLabelIndex = index;
			    }
			}

		if(!strcmp(mFsUuid.c_str(),"card"))		
			{
				while (p->volumeMMC0Bitmap[index])
			    {
			        index++;
			        if (index==	VolumeManager::MAX_VOLUMES)
			            break;
			    }

			    if (index == VolumeManager::MAX_VOLUMES)
			    {
			        
			        PLOG(ERROR) << getId() << "mmc0 full volume! can't create new one";
			    }
			    else
			    {
					mUsbLabelIndex = index;
			    }
			}
		if(!strcmp(mFsUuid.c_str(),"extsd"))		
			{
				while (p->volumeMMC1Bitmap[index])
			    {
			        index++;
			        if (index==	VolumeManager::MAX_VOLUMES)
			            break;
			    }

			    if (index == VolumeManager::MAX_VOLUMES)
			    {
			        
			        PLOG(ERROR) << getId() << "mmc1 full volume! can't create new one";
			    }
			    else
			    {
					mUsbLabelIndex = index;
			    }
			}
		if(!strcmp(mFsUuid.c_str(),"mapcard"))		
			{
				while (p->volumeMMC3Bitmap[index])
			    {
			        index++;
			        if (index==	VolumeManager::MAX_VOLUMES)
			            break;
			    }

			    if (index == VolumeManager::MAX_VOLUMES)
			    {
			        
			        PLOG(ERROR) << getId() << "mmc3 full volume! can't create new one";
			    }
			    else
			    {
					mUsbLabelIndex = index;
			    }
			}
		//stableName = StringPrintf("%s(id=%s)", mFsUuid.c_str(),mTrueUuid.c_str());
        
		if(index >9)
		stableName = StringPrintf("%s%2.2d", mFsUuid.c_str(),index);
		else if(index > 0)
		stableName = StringPrintf("%s%1.1d", mFsUuid.c_str(),index);	
		else if(index == 0)
			stableName = mFsUuid.c_str();
	 }
	
	else
	{
		
		if (!mFsUuid.empty()) {
        	stableName = mFsUuid;
       	}
	}

    mRawPath = StringPrintf("/mnt/media_rw/%s", stableName.c_str());

    mFuseDefault = StringPrintf("/mnt/runtime/default/%s", stableName.c_str());
    mFuseRead = StringPrintf("/mnt/runtime/read/%s", stableName.c_str());
    mFuseWrite = StringPrintf("/mnt/runtime/write/%s", stableName.c_str());

    setInternalPath(mRawPath);
    if (getMountFlags() & MountFlags::kVisible) {
        setPath(StringPrintf("/storage/%s", stableName.c_str()));
    } else {
        setPath(mRawPath);
    }

    int retry = 0;
    bool bPrepare_dir = false;
    while(retry < 4  && !bPrepare_dir)
    {
        if (fs_prepare_dir(mRawPath.c_str(), 0777, AID_ROOT, AID_ROOT) ||
            fs_prepare_dir(mFuseDefault.c_str(), 0777, AID_ROOT, AID_ROOT) ||
            fs_prepare_dir(mFuseRead.c_str(), 0777, AID_ROOT, AID_ROOT) ||
            fs_prepare_dir(mFuseWrite.c_str(), 0777, AID_ROOT, AID_ROOT)) {
        
        PLOG(ERROR) << getId() << " failed to create mount points";

	    ForceUnmount(mFuseDefault);
	    ForceUnmount(mFuseRead);
	    ForceUnmount(mFuseWrite);
	    ForceUnmount(mRawPath);
	
	    rmdir(mFuseDefault.c_str());
	    rmdir(mFuseRead.c_str());
	    rmdir(mFuseWrite.c_str());
	    rmdir(mRawPath.c_str());
        
        retry++;
        
    	}
    	else
    	{
    		bPrepare_dir = true;
    	}
    }
    
    if( retry == 4)
    {
    	return -errno;
    }
    
    retry = 0;
    
    while(retry < 10)
    {    
	    if (mFsType == "vfat") {
			 if(!vfat::Check(mDevPath))
			 {		
		    	if (vfat::Mount(mDevPath, mRawPath, false, false, false,
		            AID_MEDIA_RW, AID_MEDIA_RW, 0007, true)) {
		        	PLOG(ERROR) << getId() << " failed to mount " << mDevPath;
					return -EIO;
				}
				else
				{
					retry = 10; //exit loop while!!!!
					PLOG(ERROR) << mFsType.c_str()<< " mount success!!!!!  "<< mDevPath;   
				}
			}
			else  //VFAT CHECK FAILED,RETRY AGAIN!!
			{
				retry++;
				PLOG(ERROR) << mFsType.c_str()<< " check failed!!!!!  "<< mDevPath;   
			}	         
	    }
	    else if(mFsType == "ntfs")
	    {
	    	if (!Ntfs::check(mDevPath.c_str())) 
	    	{        
		    	if (Ntfs::doMount(mDevPath.c_str(), mRawPath.c_str(), false, false, false,AID_MEDIA_RW, AID_MEDIA_RW, 0007,  true)) {  
					ALOGE("%s failed to mount via NTFS (%s)\n", mDevPath.c_str(), strerror(errno)); 
				 	return -EIO;
				}
				else
				{
					retry = 10; //exit loop while!!!!
					PLOG(ERROR) << mFsType.c_str()<< " mount success!!!!!  "<< mDevPath;   
				}				        
	   	 	}
	   	 	else   //NTFS CHECK FAILED,RETRY AGAIN!!
			{
				retry++;
				PLOG(ERROR) << mFsType.c_str()<< " check failed!!!!!  "<< mDevPath;   
			}
	    }
	    else if(mFsType == "exfat")
	    {
			if (!Exfat::check(mDevPath.c_str())) 
			{     
			   	if (Exfat::doMount(mDevPath.c_str(), mRawPath.c_str(), false, false, false,AID_MEDIA_RW, AID_MEDIA_RW, 0007, true)) 
				{               
				   	ALOGE("%s failed to mount via EXFAT (%s)\n", mDevPath.c_str(), strerror(errno));   
				   	return -EIO;
				}
				else
				{
					retry = 10; //exit loop while!!!!
					PLOG(ERROR) << mFsType.c_str()<< " mount success!!!!!  "<< mDevPath;   
				}				        
	     	}
	     	else    //EXFAT CHECK FAILED,RETRY AGAIN!!
			{
				retry++;
				PLOG(ERROR) << mFsType.c_str()<< " check failed!!!!!  "<< mDevPath;   
			}     	
	    }
	    else if(mFsType == "ext4")
	    {
	    	ALOGE("shouldn't go here!!! %s mount ext4 \n", mDevPath.c_str());
	    	return -errno;
	    }
	    else
	    {
	    	PLOG(ERROR) << getId() << " all system mount fail " << mDevPath;
		   	return -EIO;
	    }
	}
	
        
/***************************************************************/	
 		if((mDevPath.find("public:8") != std::string::npos)||(mDevPath.find("public:179") != std::string::npos))
		{
			if(!strcmp(mFsUuid.c_str(),"udisk3"))					
				p->volumeUsb2Bitmap[mUsbLabelIndex] = true;
			if(!strcmp(mFsUuid.c_str(),"udiskh"))					
				p->volumeUsb1Bitmap[mUsbLabelIndex] = true;
			if(!strcmp(mFsUuid.c_str(),"udisk"))				
				p->volumeUsb0Bitmap[mUsbLabelIndex] = true;
			if(!strcmp(mFsUuid.c_str(),"card"))				
				p->volumeMMC0Bitmap[mUsbLabelIndex] = true;
			if(!strcmp(mFsUuid.c_str(),"extsd"))				
				p->volumeMMC1Bitmap[mUsbLabelIndex] = true;
			if(!strcmp(mFsUuid.c_str(),"mapcard"))				
				p->volumeMMC3Bitmap[mUsbLabelIndex] = true;
			
			PLOG(ERROR) << mFsUuid.c_str()<< " mount success!!!!!  "<< mUsbLabelIndex;   
			
		}	
/***************************************************************/    
    

    if (getMountFlags() & MountFlags::kPrimary) {
        initAsecStage();
    }

    if (!(getMountFlags() & MountFlags::kVisible)) {
        // Not visible to apps, so no need to spin up FUSE
        return OK;
    }

    dev_t before = GetDevice(mFuseWrite);

    if (!(mFusePid = fork())) {
        if (getMountFlags() & MountFlags::kPrimary) {
            if (execl(kFusePath, kFusePath,
                    "-u", "1023", // AID_MEDIA_RW
                    "-g", "1023", // AID_MEDIA_RW
                    "-U", std::to_string(getMountUserId()).c_str(),
                    "-w",
                    mRawPath.c_str(),
                    stableName.c_str(),
                    NULL)) {
                PLOG(ERROR) << "Failed to exec";
            }
        } else {
            if (execl(kFusePath, kFusePath,
                    "-u", "1023", // AID_MEDIA_RW
                    "-g", "1023", // AID_MEDIA_RW
                    "-U", std::to_string(getMountUserId()).c_str(),
                    "-w",
                    mRawPath.c_str(),
                    stableName.c_str(),
                    NULL)) {
                PLOG(ERROR) << "Failed to exec";
            }
        }

        LOG(ERROR) << "FUSE exiting";
        _exit(1);
    }

    if (mFusePid == -1) {
        PLOG(ERROR) << getId() << " failed to fork";
        return -errno;
    }

    while (before == GetDevice(mFuseWrite)) {
        LOG(VERBOSE) << "Waiting for FUSE to spin up...";
        usleep(50000); // 50ms
    }

    return OK;
}

status_t PublicVolume::doUnmount() {
    // Unmount the storage before we kill the FUSE process. If we kill
    // the FUSE process first, most file system operations will return
    // ENOTCONN until the unmount completes. This is an exotic and unusual
    // error code and might cause broken behaviour in applications.
    KillProcessesUsingPath(getPath());
/***************************************************************/
	VolumeManager *p = VolumeManager::Instance();
	
 if((mDevPath.find("public:8") != std::string::npos)||(mDevPath.find("public:179") != std::string::npos))
		{
			if(!strcmp(mFsUuid.c_str(),"udisk3"))					
				p->volumeUsb2Bitmap[mUsbLabelIndex] = false;
			if(!strcmp(mFsUuid.c_str(),"udiskh"))					
				p->volumeUsb1Bitmap[mUsbLabelIndex] = false;
			if(!strcmp(mFsUuid.c_str(),"udisk"))				
				p->volumeUsb0Bitmap[mUsbLabelIndex] = false;
			if(!strcmp(mFsUuid.c_str(),"card"))				
				p->volumeMMC0Bitmap[mUsbLabelIndex] = false;
			if(!strcmp(mFsUuid.c_str(),"extsd"))				
				p->volumeMMC1Bitmap[mUsbLabelIndex] = false;
			if(!strcmp(mFsUuid.c_str(),"mapcard"))				
				p->volumeMMC3Bitmap[mUsbLabelIndex] = false;			
		}			
	
/***************************************************************/

    ForceUnmount(kAsecPath);

    ForceUnmount(mFuseDefault);
    ForceUnmount(mFuseRead);
    ForceUnmount(mFuseWrite);
    ForceUnmount(mRawPath);

    if (mFusePid > 0) {
        kill(mFusePid, SIGTERM);
        TEMP_FAILURE_RETRY(waitpid(mFusePid, nullptr, 0));
        mFusePid = 0;
    }

    rmdir(mFuseDefault.c_str());
    rmdir(mFuseRead.c_str());
    rmdir(mFuseWrite.c_str());
    rmdir(mRawPath.c_str());

    mFuseDefault.clear();
    mFuseRead.clear();
    mFuseWrite.clear();
    mRawPath.clear();

    return OK;
}

status_t PublicVolume::doFormat(const std::string& fsType) {
    if (fsType == "vfat" || fsType == "auto") {
        if (WipeBlockDevice(mDevPath) != OK) {
            LOG(WARNING) << getId() << " failed to wipe";
        }
        if (vfat::Format(mDevPath, 0)) {
            LOG(ERROR) << getId() << " failed to format";
            return -errno;
        }
    } else {
        LOG(ERROR) << "Unsupported filesystem " << fsType;
        return -EINVAL;
    }

    return OK;
}

}  // namespace vold
}  // namespace android
