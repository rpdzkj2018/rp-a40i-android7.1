#include "default_panel.h"

#define SDA(v) (sunxi_lcd_gpio_set_value(0, 0, v)) //sda
#define DCX(v) (sunxi_lcd_gpio_set_value(0, 1, v)) //data->1 command->0
#define SCL(v) (sunxi_lcd_gpio_set_value(0, 2, v)) //clk
#define CS0(v) (sunxi_lcd_gpio_set_value(0, 3, v)) //cs
#define LCD_RESET(v) (sunxi_lcd_gpio_set_value(0, 4, v)) //reset
#define delay(x) mdelay(x)
#define  Delay_Xms(x) mdelay(x) 
#define SPI_Delay() sunxi_lcd_delay_us(50)
#define CSB(x) CS0(x)
#define SCLB(x) SCL(x)
#define SDO(x) SDA(x)

static void lcd_panel_st7789v_init(void);
static void LCD_power_on(u32 sel);
static void LCD_power_off(u32 sel);
static void LCD_bl_open(u32 sel);
static void LCD_bl_close(u32 sel);

static void LCD_panel_init(u32 sel);
static void LCD_panel_exit(u32 sel);

static void LCD_cfg_panel_info(panel_extend_para * info)
{
    u32 i = 0, j=0;
    u32 items;
    u8 lcd_gamma_tbl[][2] =
    {
        //{input value, corrected value}
        {0, 0},
        {15, 15},
        {30, 30},
        {45, 45},
        {60, 60},
        {75, 75},
        {90, 90},
        {105, 105},
        {120, 120},
        {135, 135},
        {150, 150},
        {165, 165},
        {180, 180},
        {195, 195},
        {210, 210},
        {225, 225},
        {240, 240},
        {255, 255},
    };

    u32 lcd_cmap_tbl[2][3][4] = {
        {
            {LCD_CMAP_G0,LCD_CMAP_B1,LCD_CMAP_G2,LCD_CMAP_B3},
            {LCD_CMAP_B0,LCD_CMAP_R1,LCD_CMAP_B2,LCD_CMAP_R3},
            {LCD_CMAP_R0,LCD_CMAP_G1,LCD_CMAP_R2,LCD_CMAP_G3},
        },
        {
            {LCD_CMAP_B3,LCD_CMAP_G2,LCD_CMAP_B1,LCD_CMAP_G0},
            {LCD_CMAP_R3,LCD_CMAP_B2,LCD_CMAP_R1,LCD_CMAP_B0},
            {LCD_CMAP_G3,LCD_CMAP_R2,LCD_CMAP_G1,LCD_CMAP_R0},
        },
    };

    items = sizeof(lcd_gamma_tbl)/2;
    for (i=0; i<items-1; i++) {
        u32 num = lcd_gamma_tbl[i+1][0] - lcd_gamma_tbl[i][0];

        for (j=0; j<num; j++) {
            u32 value = 0;

            value = lcd_gamma_tbl[i][1] + ((lcd_gamma_tbl[i+1][1] - lcd_gamma_tbl[i][1]) * j)/num;
            info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] = (value<<16) + (value<<8) + value;
        }
    }
    info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items-1][1]<<16) + (lcd_gamma_tbl[items-1][1]<<8) + lcd_gamma_tbl[items-1][1];

    memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));

}

#if 0
void write_command (unsigned char y) // (uchar y,uchar x)
{
    unsigned char i;
    //csb=0;
    //sclb=0;
    //sdi=0;
    //sclb=1;
    CSB(0);
    SCLB(0);
    SDO(0);
    SCLB(1);
    for(i=0;i<8;i++)
    {
        //sclb=0;      
        SCLB(0);
        if (y&0x80)
        {
            //sdi=1;
            SDO(1);
        }
        else
        {
            //sdi=0;
            SDO(0);
        }
        //sclb=1;
        SCLB(1);
        y=y<<1;
    }
    //csb=1;
    CSB(1);
}
//***************************************************************
void write_data(unsigned char w) // (uchar w, uchar v)
{
    unsigned char i;
    //csb=0;
    //sclb=0;
    //sdi=1;
    //sclb=1;
    CSB(0);
    SCLB(0);
    SDO(1);
    SCLB(1);
    for(i=0;i<8;i++)
    {
        //sclb=0;
        SCLB(0); 
        if (w&0x80)
        {
            //sdi=1;
            SDO(1);
        }
        else
        {
            //sdi=0;
            SDO(0);
        }
        //sclb=1;
        SCLB(1);
        w=w<<1;
    }
    //csb=1;
    CSB(1);
}
#else
void write_command(unsigned int com)
{

    unsigned char i;
    //CS0(0);
    //SCL(0);
    DCX(0);
   // SCL(1);
    for(i=0;i<8;i++)
    {
        SCL(0);
        if(com&0x80)
            SDA(1);
        else
            SDA(0);
        SCL(1);
        com<<=1;
        sunxi_lcd_delay_us(5);
    }
    //SCL(0);
    //CS0(1);
    sunxi_lcd_delay_us(5);


}
void write_data(unsigned int dat)
{
    unsigned char i;
    //CS0(0);
   // SCL(0);
    //SDA(1);
    DCX(1);
   // SCL(1);
    for(i=0;i<8;i++)
    {
        SCL(0);
        if(dat&0x80)
            SDA(1);
        else
            SDA(0);
        SCL(1);
        dat<<=1;
        sunxi_lcd_delay_us(5);
    }
    //SCL(0);
    //CS0(1);
    sunxi_lcd_delay_us(5);
}
#endif
#define CMD(x) write_command(x)
#define Parameter(x) write_data(x)
#define COL 240
#define ROW 320
#define SPI_WriteComm write_command
#define SPI_WriteData write_data
void ST7789V_CTC024_MCU(void)
{
    printk("\nlcd0 %s\n", __func__);

    LCD_RESET(1);
    delay(1);
    LCD_RESET(0);
    delay(10);
    LCD_RESET(1);
    delay(120);

    write_command(0x11); 
    delay(120);                //Delay 120ms 

    //--------------------------------------Display Setting------------------------------------------// 

    write_command(0x36); 
    write_data(0x00); 
    write_command(0x3a); 
    write_data(0x55);  

    SPI_WriteComm(0xb0);
    SPI_WriteData(0x11);
    SPI_WriteData(0x00);

    SPI_WriteComm(0xb1);
    SPI_WriteData(0x40);//0x40
    SPI_WriteData(0x04);	
    SPI_WriteData(0x0a);

    write_command(0xb2); 
    write_data(0x0c); 
    write_data(0x0c); 
    write_data(0x00); 
    write_data(0x33); 
    write_data(0x33); 

    write_command(0xb7); 
    write_data(0x35); 

    write_command(0xbb); 
    write_data(0x2b); 

    write_command(0xc0); 
    write_data(0x2c); 

    write_command(0xc2); 
    write_data(0x01); 

    write_command(0xc3); 
    write_data(0x17); 

    write_command(0xc4); 
    write_data(0x20); 

    write_command(0xc6); 
    write_data(0x0f); 

    write_command(0xd0); 
    write_data(0xa4); 
    write_data(0xa2); 

    write_command(0xe0); 
    write_data(0xf0); 
    write_data(0x00); 
    write_data(0x0a); 
    write_data(0x10); 
    write_data(0x12); 
    write_data(0x1b); 
    write_data(0x39); 
    write_data(0x44); 
    write_data(0x47); 
    write_data(0x28); 
    write_data(0x12); 
    write_data(0x10); 
    write_data(0x16); 
    write_data(0x1b); 

    write_command(0xe1); 
    write_data(0xf0); 
    write_data(0x00); 
    write_data(0x0a); 
    write_data(0x10); 
    write_data(0x11); 
    write_data(0x1a); 
    write_data(0x3b); 
    write_data(0x34); 
    write_data(0x4e); 
    write_data(0x3a); 
    write_data(0x17); 
    write_data(0x16); 
    write_data(0x21); 
    write_data(0x22); 


    write_command(0x29);
    delay(20);		
}

static void lcd_panel_st7789v_init(void)
{
    CS0(0);
#if 0
    printf("\nlcd0 ST7789V-----Init---on\n");
    LCD_RESET(1);
    delay(1);
    LCD_RESET(0);
    delay(10);
    LCD_RESET(1);
    delay(120);

    write_command(0x11);
    delay(120);
    write_command(0x36);
    write_data(0x00);

    write_command(0xb2);//ST7789V Frame rate setting
    write_data(0x0c);
    write_data(0x0c);
    write_data(0x00);
    write_data(0x33);
    write_data(0x33);
    write_command(0xb7);
    write_data(0x35);

    //---------ST7789V Power setting----------//
    write_command(0xbb); 
    write_data(0x1a);
    write_command(0xc0);
    write_data(0x2c);
    write_command(0xc2);
    write_data(0x01);
    write_command(0xc3); //AVDD
    write_data(0x1B);
    write_command(0xc4);
    write_data(0x20);
    write_command(0xc6);
    write_data(0x0f);
    write_command(0xd0);
    write_data(0xaf);
    write_data(0xa1);


    //---------ST7789V gamma setting----------//
    write_command(0xe0);
    write_data(0xd0);
    write_data(0x00);
    write_data(0x14);
    write_data(0x15);
    write_data(0x13);
    write_data(0x2c);
    write_data(0x42);
    write_data(0x43);
    write_data(0x4e);
    write_data(0x09);
    write_data(0x16);
    write_data(0x14);
    write_data(0x18);
    write_data(0x21);

    write_command(0xe1);
    write_data(0xd0);
    write_data(0x00);
    write_data(0x14);
    write_data(0x15);
    write_data(0x13);
    write_data(0x0b);
    write_data(0x43);
    write_data(0x55);
    write_data(0x53);
    write_data(0x0c);
    write_data(0x17);
    write_data(0x14);
    write_data(0x23);
    write_data(0x20);

    //*********SET RGB Interfae***************
    write_command(0xB0); 
    write_data(0x11); //set RGB interface and DE mode.
    write_data(0x00); 
    write_data(0x00); 

    write_command(0xB1); 
    write_data(0x40); // set DE mode ; SET Hs,Vs,DE,DOTCLK signal polarity 
    write_data(0x00); 
    write_data(0x00); 

    write_command(0x3A);
    write_data(0x55);//16BIT

    write_command(0x2A); //Frame rate control
    write_data(0x00);
    write_data(0x00);
    write_data(0x00);
    write_data(0xEF);

    write_command(0x2B); //Display function control
    write_data(0x00);
    write_data(0x00);
    write_data(0x01);
    write_data(0x3F);

    write_command(0x11); //Exit Sleep
    delay(120);

    write_command(0x29); //display on

    write_command(0x2c);
#else
    ST7789V_CTC024_MCU();
#endif
}

static s32 LCD_open_flow(u32 sel)
{
    LCD_OPEN_FUNC(sel, LCD_power_on, 0);   //open lcd power, and delay 50ms
    LCD_OPEN_FUNC(sel, LCD_panel_init, 10);   //open lcd power, than delay 200ms
    LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 300);     //open lcd controller, and delay 100ms
    LCD_OPEN_FUNC(sel, LCD_bl_open, 0);     //open lcd backlight, and delay 0ms

    return 0;
}

static s32 LCD_close_flow(u32 sel)
{
    LCD_CLOSE_FUNC(sel, LCD_bl_close, 300);       //close lcd backlight, and delay 0ms
    LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 10);         //close lcd controller, and delay 0ms
    LCD_CLOSE_FUNC(sel, LCD_panel_exit,	10);   //open lcd power, than delay 200ms
    LCD_CLOSE_FUNC(sel, LCD_power_off, 500);   //close lcd power, and delay 500ms

    return 0;
}

static void LCD_power_on(u32 sel)
{
    sunxi_lcd_power_enable(sel, 0);//config lcd_power pin to open lcd power0
    sunxi_lcd_gpio_set_value(sel, 3, 0);//pwr_en, active low
    sunxi_lcd_pin_cfg(sel, 1);
}

static void LCD_power_off(u32 sel)
{
    sunxi_lcd_pin_cfg(sel, 0);
    sunxi_lcd_gpio_set_value(sel, 3, 1);//pwr_en, active low
    sunxi_lcd_power_disable(sel, 0);//config lcd_power pin to close lcd power0
}

static void LCD_bl_open(u32 sel)
{
    sunxi_lcd_pwm_enable(sel);
    sunxi_lcd_backlight_enable(sel);//config lcd_bl_en pin to open lcd backlight
}

static void LCD_bl_close(u32 sel)
{
    sunxi_lcd_backlight_disable(sel);//config lcd_bl_en pin to close lcd backlight
    sunxi_lcd_pwm_disable(sel);
}

static void LCD_panel_init(u32 sel)
{
    disp_panel_para *info = kmalloc(sizeof(disp_panel_para), GFP_KERNEL | __GFP_ZERO);

    bsp_disp_get_panel_info(sel, info);
    lcd_panel_st7789v_init();
    kfree(info);
    return;
}

static void LCD_panel_exit(u32 sel)
{
    disp_panel_para *info = kmalloc(sizeof(disp_panel_para), GFP_KERNEL | __GFP_ZERO);

    bsp_disp_get_panel_info(sel, info);
    kfree(info);
    return ;
}



//sel: 0:lcd0; 1:lcd1
static s32 LCD_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
    return 0;
}

__lcd_panel_t st7789v_panel = {
    /* panel driver name, must mach the name of lcd_drv_name in sys_config.fex */
    .name = "ST7789V",
    .func = {
        .cfg_panel_info = LCD_cfg_panel_info,
        .cfg_open_flow = LCD_open_flow,
        .cfg_close_flow = LCD_close_flow,
        .lcd_user_defined_func = LCD_user_defined_func,
    },
};
