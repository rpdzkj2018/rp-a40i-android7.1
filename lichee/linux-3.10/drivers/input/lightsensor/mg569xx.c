/* drivers/i2c/chips/mg569xx.c - light and proxmity sensors driver
 * Copyright (C) 2011 ELAN Corporation.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/hrtimer.h>
#include <linux/timer.h>
#include <linux/delay.h>
//#include <linux/earlysuspend.h>

#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>
#include <linux/irq.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/miscdevice.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <asm/mach-types.h>
#include <asm/setup.h>
#include <linux/wakelock.h>
#include <linux/jiffies.h>
#include "mg569xx.h"
#include "mg569xx_gesture.h"
#include <linux/regulator/consumer.h>
#include <linux/platform_device.h>


/******************************************************************************
 * driver info
*******************************************************************************/
#define DRIVER_VERSION  "3.0.2"
#define EPL_DEV_NAME    "mg569xx"
/******************************************************************************
* paltform select
*******************************************************************************/
//platform select
#define S5PV210     0
#define SPREAD      0
#define QCOM        1
#define LEADCORE    0
#define MARVELL     0
#define SENSOR_CLASS    0

#if QCOM
#include <linux/of_gpio.h>
#endif
#if SENSOR_CLASS
#include <linux/sensors.h>
#endif

/******************************************************************************
 *  ALS / PS function define
 ******************************************************************************/
#define PS_DYN_K        1
#define ALS_DYN_INTT    0
#define HS_ENABLE       0

#define ALS_POLLING_MODE         	1					// 1 is polling mode, 0 is interrupt mode
#define PS_POLLING_MODE         	1					// 1 is polling mode, 0 is interrupt mode
#define GES_POLLING_MODE         	1					// 1 is polling mode, 0 is interrupt mode

#define ALS_LOW_THRESHOLD		1000
#define ALS_HIGH_THRESHOLD		2000

#define PS_LOW_THRESHOLD		4000
#define PS_HIGH_THRESHOLD		5000

#define LUX_PER_COUNT			700
#define ALS_LEVEL    16

static int als_level[] = {20, 45, 70, 90, 150, 300, 500, 700, 1150, 2250, 4500, 8000, 15000, 30000, 50000};
static int als_value[] = {10, 30, 60, 80, 100, 200, 400, 600, 800, 1500, 3000, 6000, 10000, 20000, 40000, 60000};
/*ALS interrupt table*/
unsigned long epl_als_lux_intr_level[] = {15, 39, 63, 316, 639, 4008, 5748, 10772, 14517, 65535};
unsigned long epl_als_adc_intr_level[10] = {0};

static const unsigned short normal_i2c[2] = {0x41, I2C_CLIENT_END};

static int polling_time = 200;
#define ELAN_INT_PIN 129    /*Interrupt pin setting*/

int epl_als_frame_time = 0;
int epl_ps_frame_time = 0;

/******************************************************************************
 *  factory setting
 ******************************************************************************/
 //ps calibration file location
static const char ps_cal_file[]="/data/data/com.eminent.ps.calibration/ps.dat";
//als calibration file location
static const char als_cal_file[]="/data/data/com.eminent.ps.calibration/als.dat";

static int PS_h_offset = 3000;
static int PS_l_offset = 2000;
static int PS_MAX_XTALK = 30000;

/******************************************************************************
 *I2C function define
*******************************************************************************/
#define TXBYTES                             2
#define RXBYTES                             2
#define PACKAGE_SIZE 			48
#define I2C_RETRY_COUNT 		2
int epl_i2c_max_count=8;

/******************************************************************************
 *  configuration
 ******************************************************************************/
 #if S5PV210
static const char ElanPsensorName[]="proximity";
static const char ElanALsensorName[]="lightsensor-level";
#elif SPREAD
static const char ElanPsensorName[] = "light sensor";
#elif QCOM || LEADCORE
static const char ElanPsensorName[] = "proximity";
static const char ElanALsensorName[] = "al3010"; //"light";
#elif MARVELL
static const char ElanPsensorName[] = "proximity_sensor";
static const char ElanALsensorName[] = "light_sensor";
#endif

#define LOG_TAG                      "[mg569xx] "
#define LOG_FUN(f)               	 printk(KERN_INFO LOG_TAG"%s\n", __FUNCTION__)
#define LOG_INFO(fmt, args...)    	 printk(KERN_INFO LOG_TAG fmt, ##args)
#define LOG_ERR(fmt, args...)   	 printk(KERN_ERR  LOG_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)

typedef enum
{
    CMC_BIT_RAW   			= 0x0,
    CMC_BIT_PRE_COUNT     	= 0x1,
    CMC_BIT_DYN_INT			= 0x2,
    CMC_BIT_TABLE			= 0x3,
    CMC_BIT_INTR_LEVEL      = 0x4,
} CMC_ALS_REPORT_TYPE;

typedef struct _epl_raw_data
{
    u8 raw_bytes[PACKAGE_SIZE];
    u16 renvo;
    u16 ges_data[4];

} epl_raw_data;

struct epl_priv
{
    struct i2c_client *client;
    struct input_dev *als_input_dev;
    struct input_dev *ps_input_dev;
    struct input_dev *ges_input_dev;
    struct delayed_work  eint_work;
    struct delayed_work  polling_work;
#if PS_DYN_K
    struct delayed_work  dynk_thd_polling_work;
#endif
    //struct early_suspend early_suspend;

    int intr_pin;
    int (*power)(int on);
    int ps_opened;
    int als_opened;
    int als_suspend;
    int ps_suspend;
    int ges_suspend;
    int lux_per_count;
    int enable_pflag;
    int enable_lflag;
    int enable_gflag;
    int read_flag;
    int irq;
    spinlock_t lock;
#if HS_ENABLE
    int enable_hflag;
	int hs_suspend;
#endif
#if SENSOR_CLASS
    struct sensors_classdev	als_cdev;
	struct sensors_classdev	ps_cdev;
	int			flush_count;
#endif
    /*data*/
    u16         	als_level_num;
    u16         	als_value_num;
    u32         	als_level[ALS_LEVEL-1];
    u32         	als_value[ALS_LEVEL];
    /*als interrupt*/
    int als_intr_level;
    int als_intr_lux;
} ;

static struct platform_device *sensor_dev;
struct epl_priv *epl_obj;
static epl_optical_sensor epl_sensor;
static epl_raw_data	gRawData;
static struct wake_lock ps_lock;
static struct mutex sensor_mutex;

static int epl_setup_interrupt(struct epl_priv *epld);
int epl_read_als(struct i2c_client *client);
void epl_fast_update(struct i2c_client *client);
void epl_update_mode(struct i2c_client *client);
static void epl_dumpReg(struct i2c_client *client);
static int epl_get_als_value(struct epl_priv *obj, u16 als);
static void epl_restart_polling(void);
static void epl_eint_work(struct work_struct *work);
static void epl_polling_work(struct work_struct *work);
static int epl_als_intr_update_table(struct epl_priv *epld);
#if PS_DYN_K
void epl_dynk_thd_polling_work(struct work_struct *work);
void epl_restart_dynk_polling(void);
#endif
#if HS_ENABLE
static void write_global_variable(struct i2c_client *client);
#endif

/******************************************************************************
 *  PS DYN K
 ******************************************************************************/
#if PS_DYN_K
static int dynk_polling_delay = 200;
int epl_dynk_min_ps_raw_data = 0xffff;
int epl_dynk_max_ir_data;
u32 epl_dynk_thd_low = 0;
u32 epl_dynk_thd_high = 0;
int epl_dynk_low_offset;
int epl_dynk_high_offset;
#endif

/******************************************************************************
 *  ALS DYN INTT
 ******************************************************************************/
#if ALS_DYN_INTT
//Dynamic INTT
int epl_dynamic_intt_idx;
int epl_dynamic_intt_init_idx = 1;	//initial epl_dynamic_intt_idx
int epl_c_gain;
int epl_dynamic_intt_lux = 0;

uint16_t epl_dynamic_intt_high_thr;
uint16_t epl_dynamic_intt_low_thr;
uint32_t epl_dynamic_intt_max_lux = 17000;
uint32_t epl_dynamic_intt_min_lux = 0;
uint32_t epl_dynamic_intt_min_unit = 1000;

static int als_dynamic_intt_intt[] = {EPL_ALS_INTT_150, EPL_ALS_INTT_150};
static int als_dynamic_intt_value[] = {150, 150};
static int als_dynamic_intt_gain[] = {EPL_GAIN_MID, EPL_GAIN_LOW};
static int als_dynamic_intt_high_thr[] = {60000, 60000};
static int als_dynamic_intt_low_thr[] = {200, 200};
static int als_dynamic_intt_intt_num =  sizeof(als_dynamic_intt_value)/sizeof(int);
#endif

/******************************************************************************
 *  HS_ENABLE
 ******************************************************************************/
#if HS_ENABLE
static struct mutex hs_sensor_mutex;
static bool hs_enable_flag = false;
#endif

/******************************************************************************
 *  Gesture
 ******************************************************************************/
int ges_frame_count;
//if als and ges enabled, you can setting this to update ALS
#define GES_ALS_COUNT           200
#define GES_DEBUG   0
//ges setting
static bool  ges_changed_flag = false;
static u8 ges_change_mode = 0;
static u8 ges_als_report_type;

#if SENSOR_CLASS
#define PS_MIN_POLLING_RATE    200
#define ALS_MIN_POLLING_RATE    200
static struct sensor_config_info sensor_config =
 {
	.input_type = LS_TYPE,
	.name = NULL,
	.int_number = 0,
};


static struct sensors_classdev als_cdev = {
	.name = "mg569xx-light",
	.vendor = "Eminent Technology Corp",
	.version = 1,
	.handle = SENSORS_LIGHT_HANDLE,
	.type = SENSOR_TYPE_LIGHT,
	.max_range = "65536",
	.resolution = "1.0",
	.sensor_power = "0.25",
	.min_delay = 50000,
	.max_delay = 2000,
	.fifo_reserved_event_count = 0,
	.fifo_max_event_count = 0,
	.flags = 2,
	.enabled = 0,
	.delay_msec = 50,
	.sensors_enable = NULL,
	.sensors_poll_delay = NULL,
};

static struct sensors_classdev ps_cdev = {
	.name = "mg569xx-proximity",
	.vendor = "Eminent Technology Corp",
	.version = 1,
	.handle = SENSORS_PROXIMITY_HANDLE,
	.type = SENSOR_TYPE_PROXIMITY,
	.max_range = "5",
	.resolution = "5.0",
	.sensor_power = "0.25",
	.min_delay = 10000,
	.max_delay = 2000,
	.fifo_reserved_event_count = 0,
	.fifo_max_event_count = 0,
	.flags = 3,
	.enabled = 0,
	.delay_msec = 50,
	.sensors_enable = NULL,
	.sensors_poll_delay = NULL,
};
#endif

/*
//====================I2C write operation===============//
*/
static int epl_I2C_Write_Block(struct i2c_client *client, u8 addr, u8 *data, u8 len)
{   /*because address also occupies one byte, the maximum length for write is 7 bytes*/
    int err, idx, num;
    char buf[8];
    err =0;

    if (!client)
    {
        return -EINVAL;
    }
    else if (len >= 8)
    {
        LOG_ERR(" length %d exceeds %d\n", len, 8);
        return -EINVAL;
    }

    num = 0;
    buf[num++] = addr;
    for (idx = 0; idx < len; idx++)
    {
        buf[num++] = data[idx];
    }

    err = i2c_master_send(client, buf, num);
    if (err < 0)
    {
        LOG_ERR("send command error!!\n");

        return -EFAULT;
    }

    return err;
}
/*----------------------------------------------------------------------------*/
static int epl_I2C_Write_Cmd(struct i2c_client *client, uint8_t regaddr, uint8_t data, uint8_t txbyte)
{
    uint8_t buffer[2];
    int ret = 0;
    int retry;

    buffer[0] = regaddr;
    buffer[1] = data;

    for(retry = 0; retry < I2C_RETRY_COUNT; retry++)
    {
        ret = i2c_master_send(client, buffer, txbyte);

        if (ret == txbyte)
        {
            break;
        }

        LOG_ERR("i2c write error,TXBYTES(0x%x) %d\n",regaddr, ret);
        mdelay(10);
    }


    if(retry>=I2C_RETRY_COUNT)
    {
        LOG_ERR("i2c write retry over %d\n", I2C_RETRY_COUNT);
        return -EINVAL;
    }

    return ret;
}
/*----------------------------------------------------------------------------*/
static int epl_I2C_Write(struct i2c_client *client, uint8_t regaddr, uint8_t data)
{
    int ret = 0;
    ret = epl_I2C_Write_Cmd(client, regaddr, data, 0x02);
    return ret;
}
/*----------------------------------------------------------------------------*/
static int epl_I2C_Read(struct i2c_client *client, uint8_t regaddr, uint8_t bytecount)
{

    int ret = 0;


    int retry;
    int read_count=0, rx_count=0;

    while(bytecount>0)
    {
        epl_I2C_Write_Cmd(client, regaddr+read_count, 0x00, 0x01);

        for(retry = 0; retry < I2C_RETRY_COUNT; retry++)
        {
            rx_count = bytecount>epl_i2c_max_count?epl_i2c_max_count:bytecount;
            ret = i2c_master_recv(client, &gRawData.raw_bytes[read_count], rx_count);

            if (ret == rx_count)
                break;

            LOG_ERR("i2c read error,RXBYTES %d\r\n",ret);
            mdelay(10);
        }

        if(retry>=I2C_RETRY_COUNT)
        {
            LOG_ERR("i2c read retry over %d\n", I2C_RETRY_COUNT);
            return -EINVAL;
        }
        bytecount-=rx_count;
        read_count+=rx_count;
    }

    return ret;
}


static int mg569_detect(struct i2c_client *client, struct i2c_board_info *info)
{
	struct i2c_adapter *adapter = client->adapter;
    int  ret = -1;
      
    if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA)){
        	printk("======return=====\n");
                return -ENODEV;
    }
        
    if(2 == adapter->nr){
            //dprintk(DEBUG_INIT,"%s: addr = %x\n", __func__, client->addr);
			LOG_INFO("%s: @@@@@@@@@@addr = %x\n", __func__, client->addr);
            //ret = gtp_i2c_test(client);
			ret = i2c_smbus_read_byte_data(client, 0x21);
			//if((i2c_smbus_read_byte_data(client, 0x21)) != 0x61)
			
		
				printk("detect ret %d\n",ret);
			
			
            if(!ret){
        		printk("%s:I2C connection might be something wrong \n", __func__);
        		return -ENODEV;
        	}else{           	    
            	strlcpy(info->type, EPL_DEV_NAME, I2C_NAME_SIZE);
				printk("======detect ok !=====\n");
				return 0;	
	        }
	}else{
	        return -ENODEV;
	}
}




/*----------------------------------------------------------------------------*/
static void epl_restart_polling(void)
{
    struct epl_priv *epld = epl_obj;

    cancel_delayed_work(&epld->polling_work);
    schedule_delayed_work(&epld->polling_work, msecs_to_jiffies(50));
}
/*----------------------------------------------------------------------------*/
static void epl_report_ps_status(void)
{
    struct epl_priv *epld = epl_obj;

    LOG_INFO("------------------- epl_sensor.ps.data.data=%d, value=%d \n\n", epl_sensor.ps.data.data, epl_sensor.ps.compare_low >> 3);
    input_report_abs(epld->ps_input_dev, ABS_DISTANCE, epl_sensor.ps.compare_low >> 3);
    input_sync(epld->ps_input_dev);
}
/*----------------------------------------------------------------------------*/
static void epl_report_lux(int repott_lux)
{
    struct epl_priv *epld = epl_obj;

    LOG_INFO("-------------------  ALS raw = %d, lux = %d\n\n",epl_sensor.als.data.channels[1],  repott_lux);
#if SPREAD
    input_report_abs(epld->ps_input_dev, ABS_MISC, repott_lux);
    input_sync(epld->ps_input_dev);
#else
    input_report_abs(epld->als_input_dev, ABS_MISC, repott_lux);
    input_sync(epld->als_input_dev);
#endif
}
/*----------------------------------------------------------------------------*/
static int set_psensor_intr_threshold(uint16_t low_thd, uint16_t high_thd)
{
    struct epl_priv *epld = epl_obj;
    struct i2c_client *client = epld->client;
    uint8_t high_msb ,high_lsb, low_msb, low_lsb;
    u8 buf[4];

    buf[3] = high_msb = (uint8_t) (high_thd >> 8);
    buf[2] = high_lsb = (uint8_t) (high_thd & 0x00ff);
    buf[1] = low_msb  = (uint8_t) (low_thd >> 8);
    buf[0] = low_lsb  = (uint8_t) (low_thd & 0x00ff);

    mutex_lock(&sensor_mutex);
    epl_I2C_Write_Block(client, 0x0c, buf, 4);
    mutex_unlock(&sensor_mutex);
    LOG_INFO("%s: low_thd = %d, high_thd = %d \n",__FUNCTION__, low_thd, high_thd);

    return 0;
}
/*----------------------------------------------------------------------------*/
static int set_lsensor_intr_threshold(uint16_t low_thd, uint16_t high_thd)
{
    struct epl_priv *epld = epl_obj;
    struct i2c_client *client = epld->client;
    uint8_t high_msb ,high_lsb, low_msb, low_lsb;
    u8 buf[4];

    buf[3] = high_msb = (uint8_t) (high_thd >> 8);
    buf[2] = high_lsb = (uint8_t) (high_thd & 0x00ff);
    buf[1] = low_msb  = (uint8_t) (low_thd >> 8);
    buf[0] = low_lsb  = (uint8_t) (low_thd & 0x00ff);

    mutex_lock(&sensor_mutex);
    epl_I2C_Write_Block(client, 0x08, buf, 4);
    mutex_unlock(&sensor_mutex);
    LOG_INFO("%s: low_thd = %d, high_thd = %d \n",__FUNCTION__, low_thd, high_thd);

    return 0;
}
/*----------------------------------------------------------------------------*/
#if ALS_DYN_INTT
long epl_raw_convert_to_lux(u16 raw_data)
{
    long lux = 0;
    long dyn_intt_raw = 0;
    int gain_value = 0;

    if(epl_sensor.als.gain == EPL_GAIN_MID)
    {
        gain_value = 8;
    }
    else if (epl_sensor.als.gain == EPL_GAIN_LOW)
    {
        gain_value = 1;
    }

    dyn_intt_raw = (raw_data * 10) / (10 * gain_value *als_dynamic_intt_value[epl_dynamic_intt_idx] / als_dynamic_intt_value[1]); //float calculate

    LOG_INFO("[%s]: dyn_intt_raw=%ld \r\n", __func__, dyn_intt_raw);

    if(dyn_intt_raw > 0xffff)
        epl_sensor.als.dyn_intt_raw = 0xffff;
    else
        epl_sensor.als.dyn_intt_raw = dyn_intt_raw;

    lux = epl_c_gain * epl_sensor.als.dyn_intt_raw;

    LOG_INFO("[%s]:raw_data=%d, epl_sensor.als.dyn_intt_raw=%d, lux=%ld\r\n", __func__, raw_data, epl_sensor.als.dyn_intt_raw, lux);

    if(lux >= (epl_dynamic_intt_max_lux*epl_dynamic_intt_min_unit)){
        LOG_INFO("[%s]:raw_convert_to_lux: change max lux\r\n", __func__);
        lux = epl_dynamic_intt_max_lux * epl_dynamic_intt_min_unit;
    }
    else if(lux <= (epl_dynamic_intt_min_lux*epl_dynamic_intt_min_unit)){
        LOG_INFO("[%s]:raw_convert_to_lux: change min lux\r\n", __func__);
        lux = epl_dynamic_intt_min_lux * epl_dynamic_intt_min_unit;
    }

    return lux;
}
#endif
/*----------------------------------------------------------------------------*/
static int epl_get_als_value(struct epl_priv *obj, u16 als)
{
	int idx;
	int invalid = 0;
    int level=0;
	long lux;
#if ALS_DYN_INTT
	long now_lux=0, lux_tmp=0;
    bool change_flag = false;
#endif
	switch(epl_sensor.als.report_type)
	{
		case CMC_BIT_RAW:
			return als;
		break;

		case CMC_BIT_PRE_COUNT:
			return (als * epl_sensor.als.factory.lux_per_count)/1000;
		break;

		case CMC_BIT_TABLE:
			for(idx = 0; idx < obj->als_level_num; idx++)
			{
			    if(als < als_level[idx])
			    {
			        break;
			    }
			}

			if(idx >= obj->als_value_num)
			{
			    LOG_ERR("exceed range\n");
			    idx = obj->als_value_num - 1;
			}

			if(!invalid)
			{
			    LOG_INFO("ALS: %05d => %05d\n", als, als_value[idx]);
			    return als_value[idx];
			}
			else
			{
			    LOG_ERR("ALS: %05d => %05d (-1)\n", als, als_value[idx]);
			    return als;
			}
		break;
#if ALS_DYN_INTT
		case CMC_BIT_DYN_INT:

            LOG_INFO("[%s]: epl_dynamic_intt_idx=%d, als_dynamic_intt_value=%d, dynamic_intt_gain=%d, als=%d \r\n",
                                    __func__, epl_dynamic_intt_idx, als_dynamic_intt_value[epl_dynamic_intt_idx], als_dynamic_intt_gain[epl_dynamic_intt_idx], als);

            if(als > epl_dynamic_intt_high_thr)
        	{
          		if(epl_dynamic_intt_idx == (als_dynamic_intt_intt_num - 1)){
                    als = epl_dynamic_intt_high_thr;
          		    lux_tmp = epl_raw_convert_to_lux(als);
        	      	LOG_INFO(">>>>>>>>>>>>>>>>>>>>>>>> INTT_MAX_LUX\r\n");
          		}
                else{
                    change_flag = true;
        			als  = epl_dynamic_intt_high_thr;
              		lux_tmp = epl_raw_convert_to_lux(als);
                    epl_dynamic_intt_idx++;
                    LOG_INFO(">>>>>>>>>>>>>>>>>>>>>>>>change INTT high: %d, raw: %d \r\n", epl_dynamic_intt_idx, als);
                }
            }
            else if(als < epl_dynamic_intt_low_thr)
            {
                if(epl_dynamic_intt_idx == 0){
                    //als = epl_dynamic_intt_low_thr;
                    lux_tmp = epl_raw_convert_to_lux(als);
                    LOG_INFO(">>>>>>>>>>>>>>>>>>>>>>>> INTT_MIN_LUX\r\n");
                }
                else{
                    change_flag = true;
        			als  = epl_dynamic_intt_low_thr;
                	lux_tmp = epl_raw_convert_to_lux(als);
                    epl_dynamic_intt_idx--;
                    LOG_INFO(">>>>>>>>>>>>>>>>>>>>>>>>change INTT low: %d, raw: %d \r\n", epl_dynamic_intt_idx, als);
                }
            }
            else
            {
            	lux_tmp = epl_raw_convert_to_lux(als);
            }
            now_lux = lux_tmp;

            if(change_flag == true)
            {
                epl_sensor.als.integration_time = als_dynamic_intt_intt[epl_dynamic_intt_idx];
                epl_sensor.als.gain = als_dynamic_intt_gain[epl_dynamic_intt_idx];
                epl_dynamic_intt_high_thr = als_dynamic_intt_high_thr[epl_dynamic_intt_idx];
                epl_dynamic_intt_low_thr = als_dynamic_intt_low_thr[epl_dynamic_intt_idx];
                epl_fast_update(obj->client);

                mutex_lock(&sensor_mutex);
                //epl_sensor_I2C_Write(obj->client, 0x11, EPL_POWER_OFF | EPL_RESETN_RESET);
                //epl_sensor_I2C_Write(obj->client, 0x01, epl_sensor.als.integration_time | epl_sensor.als.gain);
                epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);
                epl_I2C_Write(obj->client, 0x11, EPL_POWER_ON | EPL_RESETN_RUN);
                mutex_unlock(&sensor_mutex);

            }

            epl_dynamic_intt_lux = now_lux/epl_dynamic_intt_min_unit;

            if(epl_sensor.als.polling_mode == 0)
            {
                lux = epl_dynamic_intt_lux;
                if (lux > 0xFFFF)
    				lux = 0xFFFF;

    		    for (idx = 0; idx < 10; idx++)
    		    {
        			if (lux <= (*(epl_als_lux_intr_level + idx))) {
        				level = idx;
        				if (*(epl_als_lux_intr_level + idx))
        					break;
        			}
        			if (idx == 9) {
        				level = idx;
        				break;
        			}
        		}
                obj->als_intr_level = level;
                obj->als_intr_lux = lux;
                return level;
            }
            else
            {
                return epl_dynamic_intt_lux;
            }
		break;
#endif
        case CMC_BIT_INTR_LEVEL:
            lux = (als * epl_sensor.als.factory.lux_per_count)/1000;
            if (lux > 0xFFFF)
				lux = 0xFFFF;

		    for (idx = 0; idx < 10; idx++)
		    {
    			if (lux <= (*(epl_als_lux_intr_level + idx))) {
    				level = idx;
    				if (*(epl_als_lux_intr_level + idx))
    					break;
    			}
    			if (idx == 9) {
    				level = idx;
    				break;
    			}
    		}
            obj->als_intr_level = level;
            obj->als_intr_lux = lux;
            return level;
        break;
	}

	return 0;
}
/*----------------------------------------------------------------------------*/
static void epl_notify_event(int event)
{
    struct input_dev *idev = epl_obj->ges_input_dev;
    GES_LOG("notify event %d\n",event);
	LOG_INFO("*************epl_notify_event**************** \n");
    if(ges_mode==0)
		return;

    if(event<EVENT_UNKNOWN)
    {
        input_report_key(idev, KEYCODE_ARRAY[event], 1);
        input_report_key(idev, KEYCODE_ARRAY[event], 0);
        input_sync(idev);
    }
}
/*----------------------------------------------------------------------------*/
static void epl_gesture_rawdata(void)
{
    struct epl_priv *obj = epl_obj;
    bool enable_als = obj->enable_lflag==1 && obj->als_suspend==0;
    int index = 1<<(epl_sensor.ges.interrupt_index>>5);
    int event, i, j, k, f;
    int width = 6, shift=2;

    epl_I2C_Read(obj->client, 0x27, 6*index);

    if(index>4)
        f=4;
    else
        f=index;

    for(i = 0 ; i < index ; i++)
    {
        j=6*i;
        epl_sensor.ges.data.t[i].channels[0] = gRawData.raw_bytes[0+j] | ((gRawData.raw_bytes[1+j] & 0x0f) << 8);
        epl_sensor.ges.data.t[i].channels[1] = (gRawData.raw_bytes[2+j] << 4) | ((gRawData.raw_bytes[1+j] & 0xf0) >> 4);
        epl_sensor.ges.data.t[i].channels[2] = gRawData.raw_bytes[3+j] | ((gRawData.raw_bytes[4+j] & 0x0f) << 8);
        epl_sensor.ges.data.t[i].channels[3] = (gRawData.raw_bytes[5+j] << 4) | ((gRawData.raw_bytes[4+j] & 0xf0) >> 4);
#if GES_DEBUG
        LOG_INFO("[%s]:i=%d: %d, %d, %d, %d \r\n", __func__, i,
                                                epl_sensor.ges.data.t[i].channels[0],
                                                epl_sensor.ges.data.t[i].channels[1],
                                                epl_sensor.ges.data.t[i].channels[2],
                                                epl_sensor.ges.data.t[i].channels[3]);
#endif
    }

    for(i=0; i<index; i+=shift)
    {
        for(j = 0 ; j<4; j++)
            gRawData.ges_data[j] = 0;

        for(k = 0 ; k<width; k++)
        {
            if(i+k<index)
            {
                for(j = 0 ; j<4; j++)
                    gRawData.ges_data[j] += epl_sensor.ges.data.t[i+k].channels[j];
            }
            if(i+k>=index || k==width-1)
            {
                for(j = 0 ; j<4; j++)
                    gRawData.ges_data[j] = gRawData.ges_data[j]/f;

                add_gesture_data(gRawData.ges_data);
                event = detect_gesture_event();
                if(event!=EVENT_UNKNOWN)
                    epl_notify_event(event);
                break;
            }
        }

        if(i+width>=index)
            break;
    }

    if(enable_als)
    {
        epl_sensor.als.report_count+=index;
        if(epl_sensor.als.report_count >= GES_ALS_COUNT)
        {
            int report_lux;
            epl_read_als(obj->client);
            epl_sensor.als.report_count = 0;
            report_lux = epl_get_als_value(obj, epl_sensor.als.data.channels[1]);
            epl_report_lux(report_lux);
        }
    }

    if((sleep_mode==1 && is_work_triger) || sleep_reset==1)
    {
        epl_I2C_Write(obj->client, 0x00, epl_sensor.ges.wait);
		epl_I2C_Write(obj->client, 0x00, epl_sensor.ges.wait | epl_sensor.mode);

        sleep_mode=0;
        sleep_reset=0;

        if(GESTURE_DEBUG>=2)
            GES_LOG("sleep mode: %d\n", sleep_mode);
    }
    else if(sleep_mode==0 && is_work_triger==false && sleep_count>SLEEP_PERIOD)
    {
		epl_I2C_Write(obj->client, 0x00, epl_sensor.ges.wait_sleep);
        epl_I2C_Write(obj->client, 0x00, epl_sensor.ges.wait_sleep| epl_sensor.mode);
        sleep_mode=1;

        if(GESTURE_DEBUG>=2)
            GES_LOG("sleep mode: %d\n", sleep_mode);
    }
}
/*----------------------------------------------------------------------------*/
int epl_read_als(struct i2c_client *client)
{
    struct epl_priv *obj = i2c_get_clientdata(client);
    u8 buf[5];
    if(client == NULL)
    {
        LOG_ERR("CLIENT CANN'T EQUL NULL\n");
        return -1;
    }

    mutex_lock(&sensor_mutex);
    epl_I2C_Read(obj->client, 0x12, 5);
    buf[0] = gRawData.raw_bytes[0];
    buf[1] = gRawData.raw_bytes[1];
    buf[2] = gRawData.raw_bytes[2];
    buf[3] = gRawData.raw_bytes[3];
    buf[4] = gRawData.raw_bytes[4];
    mutex_unlock(&sensor_mutex);

    epl_sensor.als.saturation = (buf[0] & 0x20);
    epl_sensor.als.compare_high = (buf[0] & 0x10);
    epl_sensor.als.compare_low = (buf[0] & 0x08);
    epl_sensor.als.interrupt_flag = (buf[0] & 0x04);
    epl_sensor.als.compare_reset = (buf[0] & 0x02);
    epl_sensor.als.lock = (buf[0] & 0x01);
    epl_sensor.als.data.channels[0] = (buf[2]<<8) | buf[1];
    epl_sensor.als.data.channels[1] = (buf[4]<<8) | buf[3];

	LOG_INFO("als: ~~~~ ALS ~~~~~ \n");
	LOG_INFO("als: buf = 0x%x\n", buf[0]);
	LOG_INFO("als: sat = 0x%x\n", epl_sensor.als.saturation);
	LOG_INFO("als: cmp h = 0x%x, l = %d\n", epl_sensor.als.compare_high, epl_sensor.als.compare_low);
	LOG_INFO("als: int_flag = 0x%x\n",epl_sensor.als.interrupt_flag);
	LOG_INFO("als: cmp_rstn = 0x%x, lock = 0x%x\n", epl_sensor.als.compare_reset, epl_sensor.als.lock);
    LOG_INFO("read als channel 0 = %d\n", epl_sensor.als.data.channels[0]);
    LOG_INFO("read als channel 1 = %d\n", epl_sensor.als.data.channels[1]);
    return 0;
}
/*----------------------------------------------------------------------------*/
int epl_read_ps(struct i2c_client *client)
{
    u8 buf[5];
    if(client == NULL)
    {
        LOG_ERR("CLIENT CANN'T EQUL NULL\n");
        return -1;
    }

    mutex_lock(&sensor_mutex);
    epl_I2C_Read(client,0x1b, 5);
    buf[0] = gRawData.raw_bytes[0];
    buf[1] = gRawData.raw_bytes[1];
    buf[2] = gRawData.raw_bytes[2];
    buf[3] = gRawData.raw_bytes[3];
    buf[4] = gRawData.raw_bytes[4];
    mutex_unlock(&sensor_mutex);

    epl_sensor.ps.saturation = (buf[0] & 0x20);
    epl_sensor.ps.compare_high = (buf[0] & 0x10);
    epl_sensor.ps.compare_low = (buf[0] & 0x08);
    epl_sensor.ps.interrupt_flag = (buf[0] & 0x04);
    epl_sensor.ps.compare_reset = (buf[0] & 0x02);
    epl_sensor.ps.lock= (buf[0] & 0x01);
    epl_sensor.ps.data.ir_data = (buf[2]<<8) | buf[1];
    epl_sensor.ps.data.data = (buf[4]<<8) | buf[3];

	LOG_INFO("ps: ~~~~ PS ~~~~~ \n");
	LOG_INFO("ps: buf = 0x%x\n", buf[0]);
	LOG_INFO("ps: sat = 0x%x\n", epl_sensor.ps.saturation);
	LOG_INFO("ps: cmp h = 0x%x, l = 0x%x\n", epl_sensor.ps.compare_high, epl_sensor.ps.compare_low);
	LOG_INFO("ps: int_flag = 0x%x\n",epl_sensor.ps.interrupt_flag);
	LOG_INFO("ps: cmp_rstn = 0x%x, lock = 0x%x\n", epl_sensor.ps.compare_reset, epl_sensor.ps.lock);
	LOG_INFO("[%s]: data = %d\n", __func__, epl_sensor.ps.data.data);
	LOG_INFO("[%s]: ir data = %d\n", __func__, epl_sensor.ps.data.ir_data);
    return 0;
}
/*----------------------------------------------------------------------------*/
#if HS_ENABLE
int epl_read_hs(struct i2c_client *client)
{
    u8 buf;

    if(client == NULL)
    {
        LOG_ERR("CLIENT CANN'T EQUL NULL\n");
        return -1;
    }

    mutex_lock(&hs_sensor_mutex);
    epl_I2C_Read(client,0x1e, 2);
    epl_sensor.hs.raw = (gRawData.raw_bytes[1]<<8) | gRawData.raw_bytes[0];
    //LOG_INFO("epl_sensor.hs.raw=%d \r\n", epl_sensor.hs.raw);
    if(epl_sensor.hs.dynamic_intt == true && epl_sensor.hs.raw>epl_sensor.hs.high_threshold && epl_sensor.hs.integration_time > epl_sensor.hs.integration_time_min)
    {
		epl_sensor.hs.integration_time -= 4;
		buf = epl_sensor.hs.integration_time | epl_sensor.hs.gain;
		epl_I2C_Write(client, 0x03, buf);
    }
    else if(epl_sensor.hs.dynamic_intt == true && epl_sensor.hs.raw>epl_sensor.hs.low_threshold && epl_sensor.hs.raw <epl_sensor.hs.mid_threshold && epl_sensor.hs.integration_time <  epl_sensor.hs.integration_time_max)
	{
		epl_sensor.hs.integration_time += 4;
		buf = epl_sensor.hs.integration_time | epl_sensor.hs.gain;
		epl_I2C_Write(client, 0x03, buf);
	}
	mutex_unlock(&hs_sensor_mutex);

    if(epl_sensor.hs.raws_count<200)
    {
		epl_sensor.hs.raws[epl_sensor.hs.raws_count] = epl_sensor.hs.raw;
        epl_sensor.hs.raws_count++;
    }

    return 0;
}
#endif
/*----------------------------------------------------------------------------*/
int epl_factory_ps_data(void)
{
    struct epl_priv *epld = epl_obj;
    bool enable_ps = epld->enable_pflag==1 && epld->ps_suspend==0;

#if !PS_DYN_K
    if(enable_ps == 1 && epl_sensor.ps.polling_mode == 0)
    {
        epl_read_ps(epld->client);
    }
    else if(enable_ps == 0)
    {
        LOG_INFO("[%s]: ps is disabled \r\n", __func__);
    }
#else
    if(enable_ps == 0)
    {
        LOG_INFO("[%s]: ps is disabled \r\n", __func__);
    }
#endif
    LOG_INFO("[%s]: enable_ps=%d, ps_raw=%d \r\n", __func__, enable_ps, epl_sensor.ps.data.data);

    return epl_sensor.ps.data.data;
}
/*----------------------------------------------------------------------------*/
int epl_factory_als_data(void)
{
    struct epl_priv *epld = epl_obj;
    u16 als_raw = 0;
    bool enable_als = epld->enable_lflag==1 && epld->als_suspend==0;

    if(enable_als == 1 && epl_sensor.als.polling_mode == 0)
    {
        epl_read_als(epld->client);

        if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
        {
            int als_lux=0;
            als_lux = epl_get_als_value(epld, epl_sensor.als.data.channels[1]);
        }
    }
    else if (enable_als == 0)
    {
        LOG_INFO("[%s]: als is disabled \r\n", __func__);
    }

    if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
    {
        als_raw = epl_sensor.als.dyn_intt_raw;
        LOG_INFO("[%s]: ALS_DYN_INTT: als_raw=%d \r\n", __func__, als_raw);
    }
    else
    {
        als_raw = epl_sensor.als.data.channels[1];
        LOG_INFO("[%s]: als_raw=%d \r\n", __func__, als_raw);
    }
    return als_raw;
}
/*----------------------------------------------------------------------------*/
void epl_enable_ps(int enable)
{
    struct epl_priv *epld = epl_obj;
    bool enable_ps = epld->enable_pflag==1;
#if HS_ENABLE
    bool enable_hs = epld->enable_hflag==1 && epld->hs_suspend==0;
#endif
    LOG_INFO("[%s]: ps enable = %d\r\n", __func__, enable);

#if HS_ENABLE
    if(enable_hs == 1 && enable == 1)
    {
        epld->enable_hflag = 0;
        if(hs_enable_flag == true)
        {
            epld->enable_lflag = 1;
            hs_enable_flag = false;
        }
        write_global_variable(epld->client);
        LOG_INFO("[%s] Disable HS and recover ps setting \r\n", __func__);
    }
#endif
    if(enable_ps != enable)
    {
        if(enable)
        {
            //wake_lock(&ps_lock);
            epld->enable_pflag = 1;
#if PS_DYN_K
            set_psensor_intr_threshold(65534, 65535);   ////dont use first ps status
            epl_dynk_min_ps_raw_data = 0xffff;
            epl_sensor.ps.compare_low = 0x08; //reg0x1b=0x08, FAR
#endif
        }
        else
        {

           epld->enable_pflag = 0;
#if PS_DYN_K
            cancel_delayed_work(&epld->dynk_thd_polling_work);
#endif
            //wake_unlock(&ps_lock);
        }
        epl_fast_update(epld->client); //if fast to on/off ALS, it read als for DOC_OFF setting for als_operate
        epl_update_mode(epld->client);
    }
    else
    {
        LOG_INFO("[%s]: ps_enable_flag is the same \r\n", __func__);
    }
}

void epl_enable_als(int enable)
{
    struct epl_priv *epld = epl_obj;
    bool enable_als = epld->enable_lflag==1;

    LOG_INFO("[%s]: als enable = %d\r\n", __func__, enable);

    if(enable_als != enable)
    {
        if(enable)
        {
            epld->enable_lflag = 1;
#if ALS_DYN_INTT
            if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
            {
                epl_dynamic_intt_idx = epl_dynamic_intt_init_idx;
                epl_sensor.als.integration_time = als_dynamic_intt_intt[epl_dynamic_intt_idx];
                epl_sensor.als.gain = als_dynamic_intt_gain[epl_dynamic_intt_idx];
                epl_dynamic_intt_high_thr = als_dynamic_intt_high_thr[epl_dynamic_intt_idx];
                epl_dynamic_intt_low_thr = als_dynamic_intt_low_thr[epl_dynamic_intt_idx];
            }
#endif
        epl_fast_update(epld->client);
        }
        else
        {
            epld->enable_lflag = 0;
        }
        epl_update_mode(epld->client);
    }
    else
    {
        LOG_INFO("[%s]: als_enable_flag is the same \r\n", __func__);
    }
}
/*----------------------------------------------------------------------------*/
static int write_factory_calibration(struct epl_priv *epl_data, char* ps_data, int ps_cal_len)
{
    struct file *fp_cal;

	mm_segment_t fs;
	loff_t pos;

	LOG_INFO("[ELAN] %s\n", __func__);
    pos = 0;

	fp_cal = filp_open(ps_cal_file, O_CREAT|O_RDWR|O_TRUNC, 0755/*S_IRWXU*/);
	if (IS_ERR(fp_cal))
	{
		LOG_ERR("[ELAN]create file_h error\n");
		return -1;
	}

    fs = get_fs();
	set_fs(KERNEL_DS);

	vfs_write(fp_cal, ps_data, ps_cal_len, &pos);

    filp_close(fp_cal, NULL);

	set_fs(fs);

	return 0;
}
/*----------------------------------------------------------------------------*/
static bool read_factory_calibration(void)
{
	struct epl_priv *epld = epl_obj;
	struct file *fp;
	mm_segment_t fs;
	loff_t pos;
	char buffer[100]= {0};
	if(epl_sensor.ps.factory.calibration_enable && !epl_sensor.ps.factory.calibrated)
	{
		fp = filp_open(ps_cal_file, O_RDWR, S_IRUSR);

		if (IS_ERR(fp))
		{
			LOG_ERR("NO PS calibration file(%d)\n", (int)IS_ERR(fp));
			epl_sensor.ps.factory.calibration_enable =  false;
		}
		else
		{
		    int ps_cancelation = 0, ps_hthr = 0, ps_lthr = 0;
			pos = 0;
			fs = get_fs();
			set_fs(KERNEL_DS);
			vfs_read(fp, buffer, sizeof(buffer), &pos);
			filp_close(fp, NULL);

			sscanf(buffer, "%d,%d,%d", &ps_cancelation, &ps_hthr, &ps_lthr);
			epl_sensor.ps.factory.cancelation = ps_cancelation;
			epl_sensor.ps.factory.high_threshold = ps_hthr;
			epl_sensor.ps.factory.low_threshold = ps_lthr;
			set_fs(fs);

			epl_sensor.ps.high_threshold = epl_sensor.ps.factory.high_threshold;
			epl_sensor.ps.low_threshold = epl_sensor.ps.factory.low_threshold;
			epl_sensor.ps.cancelation = epl_sensor.ps.factory.cancelation;
		}

		epl_I2C_Write(epld->client,0x22, (u8)(epl_sensor.ps.cancelation& 0xff));
		epl_I2C_Write(epld->client,0x23, (u8)((epl_sensor.ps.cancelation & 0xff00) >> 8));
		set_psensor_intr_threshold(epl_sensor.ps.low_threshold, epl_sensor.ps.high_threshold);

		epl_sensor.ps.factory.calibrated = true;
	}

	if(epl_sensor.als.factory.calibration_enable && !epl_sensor.als.factory.calibrated)
	{
		fp = filp_open(als_cal_file, O_RDONLY, S_IRUSR);
		if (IS_ERR(fp))
		{
			LOG_ERR("NO ALS calibration file(%d)\n", (int)IS_ERR(fp));
			epl_sensor.als.factory.calibration_enable =  false;
		}
		else
		{
		    int als_lux_per_count = 0;
			pos = 0;
			fs = get_fs();
			set_fs(KERNEL_DS);
			vfs_read(fp, buffer, sizeof(buffer), &pos);
			filp_close(fp, NULL);

			sscanf(buffer, "%d", &als_lux_per_count);
			epl_sensor.als.factory.lux_per_count = als_lux_per_count;
			set_fs(fs);
		}
		epl_sensor.als.factory.calibrated = true;
	}
	return true;
}
/*----------------------------------------------------------------------------*/
static int epl_run_ps_calibration(struct epl_priv *epl_data)
{
    struct epl_priv *epld = epl_data;
    u16 ch1=0;
    int ps_hthr=0, ps_lthr=0, ps_cancelation=0, ps_cal_len = 0;
    char ps_calibration[20];

    if(PS_MAX_XTALK < 0)
    {
        LOG_ERR("[%s]:Failed: PS_MAX_XTALK < 0 \r\n", __func__);
        return -EINVAL;
    }

	switch(epl_sensor.mode)
	{
		case EPL_MODE_PS:
		case EPL_MODE_ALS_PS:
			ch1 = epl_factory_ps_data();
	    break;
	}

    if(ch1 > PS_MAX_XTALK)
    {
        LOG_ERR("[%s]:Failed: ch1 > max_xtalk(%d) \r\n", __func__, ch1);
        return -EINVAL;
    }
    else if(ch1 <= 0)
    {
        LOG_ERR("[%s]:Failed: ch1 = 0\r\n", __func__);
        return -EINVAL;
    }

    ps_hthr = ch1 + PS_h_offset;
    ps_lthr = ch1 + PS_l_offset;
    ps_cal_len = sprintf(ps_calibration, "%d,%d,%d", ps_cancelation, ps_hthr, ps_lthr);

    if(write_factory_calibration(epld, ps_calibration, ps_cal_len) < 0)
    {
        LOG_ERR("[%s] create file error \n", __func__);
        return -EINVAL;
    }

    epl_sensor.ps.low_threshold = ps_lthr;
	epl_sensor.ps.high_threshold = ps_hthr;
	set_psensor_intr_threshold(epl_sensor.ps.low_threshold, epl_sensor.ps.high_threshold);

	LOG_INFO("[%s]: ch1 = %d\n", __func__, ch1);

	return ch1;
}
/*----------------------------------------------------------------------------*/
static void set_als_ps_intr_type(struct i2c_client *client, bool ps_polling, bool als_polling)
{
    //set als / ps interrupt control mode and trigger type
	switch((ps_polling << 1) | als_polling)
	{
		case 0: // ps and als interrupt
			epl_sensor.interrupt_control = 	EPL_INT_CTRL_ALS_OR_PS;
			epl_sensor.als.interrupt_type = EPL_INTTY_ACTIVE;
			epl_sensor.ps.interrupt_type = EPL_INTTY_ACTIVE;
		break;

		case 1: //ps interrupt and als polling
			epl_sensor.interrupt_control = 	EPL_INT_CTRL_PS;
			epl_sensor.als.interrupt_type = EPL_INTTY_DISABLE;
			epl_sensor.ps.interrupt_type = EPL_INTTY_ACTIVE;
		break;

		case 2: // ps polling and als interrupt
			epl_sensor.interrupt_control = 	EPL_INT_CTRL_ALS;
			epl_sensor.als.interrupt_type = EPL_INTTY_ACTIVE;
			epl_sensor.ps.interrupt_type = EPL_INTTY_DISABLE;
		break;

		case 3: //ps and als polling
			epl_sensor.interrupt_control = 	EPL_INT_CTRL_ALS_OR_PS;
			epl_sensor.als.interrupt_type = EPL_INTTY_DISABLE;
			epl_sensor.ps.interrupt_type = EPL_INTTY_DISABLE;
		break;
	}
}
/*
//====================write global variable===============//
*/
static void write_global_variable(struct i2c_client *client)
{
	u8 buf;
#if HS_ENABLE
    struct epl_priv *obj = epl_obj;
    bool enable_hs = obj->enable_hflag==1 && obj->hs_suspend==0;
#endif
    epl_I2C_Write(client, 0x11, EPL_POWER_OFF | EPL_RESETN_RESET);
    //wake up chip
    //buf = epl_sensor.reset | epl_sensor.power;
    //epl_I2C_Write(client, 0x11, buf);
    /* read revno*/
    epl_I2C_Read(client, 0x20, 2);
    epl_sensor.revno = gRawData.raw_bytes[0] | gRawData.raw_bytes[1] << 8;
    /*osc setting*/
    epl_I2C_Write(client, 0xfc, epl_sensor.osc_sel| EPL_GFIN_ENABLE | EPL_VOS_ENABLE | EPL_DOC_ON);

#if HS_ENABLE
	if(enable_hs)
	{
	    epl_sensor.mode = EPL_MODE_PS;
	    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | EPL_MODE_IDLE);

        set_als_ps_intr_type(client, 1, epl_sensor.als.polling_mode);
	    buf = epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type;
        epl_I2C_Write(client, 0x06, buf);
        buf = epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type;
        epl_I2C_Write(client, 0x07, buf);


		/*hs setting*/
		buf = epl_sensor.hs.integration_time | epl_sensor.hs.gain;
		epl_I2C_Write(client, 0x03, buf);

		buf = epl_sensor.hs.adc | epl_sensor.hs.cycle;
		epl_I2C_Write(client, 0x04, buf);

		buf = epl_sensor.hs.ir_on_control | epl_sensor.hs.ir_mode | epl_sensor.hs.ir_drive;
		epl_I2C_Write(client, 0x05, buf);

		buf = epl_sensor.hs.compare_reset | epl_sensor.hs.lock;

		epl_I2C_Write(client, 0x1b, buf);
	}
	else
#endif
    {
        //set als / ps interrupt control mode and trigger type
        set_als_ps_intr_type(client, epl_sensor.ps.polling_mode, epl_sensor.als.polling_mode);

        /*ps setting*/
        buf = epl_sensor.ps.integration_time | epl_sensor.ps.gain;
        epl_I2C_Write(client, 0x03, buf);

        buf = epl_sensor.ps.adc | epl_sensor.ps.cycle;
        epl_I2C_Write(client, 0x04, buf);

        buf = epl_sensor.ps.ir_on_control | epl_sensor.ps.ir_mode | epl_sensor.ps.ir_boost | epl_sensor.ps.ir_drive;
        epl_I2C_Write(client, 0x05, buf);

        buf = epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type;
        epl_I2C_Write(client, 0x06, buf);

        buf = epl_sensor.ps.compare_reset | epl_sensor.ps.lock;
        epl_I2C_Write(client, 0x1b, buf);

        epl_I2C_Write(client, 0x22, (u8)(epl_sensor.ps.cancelation& 0xff));
        epl_I2C_Write(client, 0x23, (u8)((epl_sensor.ps.cancelation & 0xff00) >> 8));
        set_psensor_intr_threshold(epl_sensor.ps.low_threshold, epl_sensor.ps.high_threshold);

        /*als setting*/
        buf = epl_sensor.als.integration_time | epl_sensor.als.gain;
        epl_I2C_Write(client, 0x01, buf);

        buf = epl_sensor.als.adc | epl_sensor.als.cycle;
        epl_I2C_Write(client, 0x02, buf);

        buf = epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type;
        epl_I2C_Write(client, 0x07, buf);

        buf = epl_sensor.als.compare_reset | epl_sensor.als.lock;
        epl_I2C_Write(client, 0x12, buf);

        set_lsensor_intr_threshold(epl_sensor.als.low_threshold, epl_sensor.als.high_threshold);

        /*ges setting*/
        buf = epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain;
        epl_I2C_Write(client, 0x24, buf);

        buf = epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive;
        epl_I2C_Write(client, 0x25, buf);
    }
    //set mode and wait
    buf = epl_sensor.wait | epl_sensor.mode;
    epl_I2C_Write(client, 0x00, buf);
}
//====================initial global variable===============//
static void initial_global_variable(struct i2c_client *client, struct epl_priv *obj)
{
    //general setting
    epl_sensor.power = EPL_POWER_ON;
    epl_sensor.reset = EPL_RESETN_RUN;
    epl_sensor.mode = EPL_MODE_IDLE;
    epl_sensor.wait = EPL_WAIT_0_MS;
    epl_sensor.osc_sel = EPL_OSC_SEL_1MHZ;

    //als setting
    epl_sensor.als.polling_mode = ALS_POLLING_MODE;
    epl_sensor.als.integration_time = EPL_ALS_INTT_150;
    epl_sensor.als.gain = EPL_GAIN_LOW;
    epl_sensor.als.adc = EPL_PSALS_ADC_11;
    epl_sensor.als.cycle = EPL_CYCLE_16;
    epl_sensor.als.interrupt_channel_select = EPL_ALS_INT_CHSEL_1;
    epl_sensor.als.persist = EPL_PERIST_1;
    epl_sensor.als.ir_on_control = EPL_IR_ON_CTRL_OFF;
    epl_sensor.als.ir_mode = EPL_IR_MODE_CURRENT;
    epl_sensor.als.ir_boost = EPL_IR_BOOST_100;
    epl_sensor.als.ir_drive = EPL_IR_DRIVE_100;
    epl_sensor.als.compare_reset = EPL_CMP_RUN;
    epl_sensor.als.lock = EPL_UN_LOCK;
    epl_sensor.als.report_type = CMC_BIT_RAW;
    epl_sensor.als.high_threshold = ALS_HIGH_THRESHOLD;
    epl_sensor.als.low_threshold = ALS_LOW_THRESHOLD;

    //als factory
    epl_sensor.als.factory.calibration_enable =  false;
    epl_sensor.als.factory.calibrated = false;
    epl_sensor.als.factory.lux_per_count = LUX_PER_COUNT;
#if ALS_DYN_INTT
    if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
    {
        epl_dynamic_intt_idx = epl_dynamic_intt_init_idx;
        epl_sensor.als.integration_time = als_dynamic_intt_intt[epl_dynamic_intt_idx];
        epl_sensor.als.gain = als_dynamic_intt_gain[epl_dynamic_intt_idx];
        epl_dynamic_intt_high_thr = als_dynamic_intt_high_thr[epl_dynamic_intt_idx];
        epl_dynamic_intt_low_thr = als_dynamic_intt_low_thr[epl_dynamic_intt_idx];
    }
    epl_c_gain = 300; // 300/1000=0.3
#endif

    //update als intr table
    if(epl_sensor.als.polling_mode == 0)
        epl_als_intr_update_table(obj);

    //ps setting
    epl_sensor.ps.polling_mode = PS_POLLING_MODE;
    //epl_sensor.ps.integration_time = EPL_PS_INTT_70;
	epl_sensor.ps.integration_time = EPL_PS_INTT_200;
    epl_sensor.ps.gain = EPL_GAIN_LOW;
    epl_sensor.ps.adc = EPL_PSALS_ADC_11;
    epl_sensor.ps.cycle = EPL_CYCLE_16;
    epl_sensor.ps.persist = EPL_PERIST_1;
    epl_sensor.ps.ir_on_control = EPL_IR_ON_CTRL_ON;
    epl_sensor.ps.ir_mode = EPL_IR_MODE_CURRENT;
    //epl_sensor.ps.ir_boost = EPL_IR_BOOST_100;
	epl_sensor.ps.ir_boost = EPL_IR_BOOST_200;
    epl_sensor.ps.ir_drive = EPL_IR_DRIVE_100;
    epl_sensor.ps.compare_reset = EPL_CMP_RUN;
    epl_sensor.ps.lock = EPL_UN_LOCK;
    epl_sensor.ps.high_threshold = PS_HIGH_THRESHOLD;
    epl_sensor.ps.low_threshold = PS_LOW_THRESHOLD;
#if PS_DYN_K
    epl_dynk_max_ir_data = 50000; // ps max ch0
   // epl_dynk_low_offset = 1000; //500;
   epl_dynk_low_offset = 3500; //500;
 //   epl_dynk_high_offset = 1600; //800;
	epl_dynk_high_offset = 4000; //800;
#endif
    //ps factory
    epl_sensor.ps.factory.calibration_enable =  false;
    epl_sensor.ps.factory.calibrated = false;
    epl_sensor.ps.factory.cancelation= 0;

    //ges setting
    epl_sensor.ges.integration_time = EPL_GES_INTT_80;  //hight level
    epl_sensor.ges.wait = EPL_WAIT_0_MS;
	epl_sensor.ges.wait_sleep = EPL_WAIT_2_MS;
    epl_sensor.ges.adc = EPL_GES_ADC_11;
    epl_sensor.ges.gain = EPL_GAIN_LOW;
    epl_sensor.ges.polling_mode = GES_POLLING_MODE;
    epl_sensor.ges.interrupt_index = EPL_INDEX_8;
    epl_sensor.ges.ir_mode = EPL_IR_MODE_CURRENT;
    //epl_sensor.ges.ir_boost = EPL_IR_BOOST_100;
	epl_sensor.ges.ir_boost = EPL_IR_BOOST_200;  //don't modify 
    epl_sensor.ges.ir_drive = EPL_IR_DRIVE_100;
    //set gesture interrupt enable / disable
    if(epl_sensor.ges.polling_mode == false)
    {
        epl_sensor.ges.interrupt_enable = EPL_GES_INT_EN;
    }
    else
    {
        epl_sensor.ges.interrupt_enable = EPL_GES_INT_DIS;
    }
#if HS_ENABLE
	//hs setting
    epl_sensor.hs.integration_time = EPL_PS_INTT_110;
	epl_sensor.hs.integration_time_max = EPL_PS_INTT_450;
	epl_sensor.hs.integration_time_min = EPL_PS_INTT_56;
    epl_sensor.hs.gain = EPL_GAIN_LOW;
    epl_sensor.hs.adc = EPL_PSALS_ADC_11;
    epl_sensor.hs.cycle = EPL_CYCLE_4;
    epl_sensor.hs.ir_on_control = EPL_IR_ON_CTRL_ON;
    epl_sensor.hs.ir_mode = EPL_IR_MODE_CURRENT;
    epl_sensor.hs.ir_drive = EPL_IR_DRIVE_100;
    epl_sensor.hs.compare_reset = EPL_CMP_RESET;
    epl_sensor.hs.lock = EPL_UN_LOCK;
	epl_sensor.hs.low_threshold = 6400;
	epl_sensor.hs.mid_threshold = 25600;
	epl_sensor.hs.high_threshold = 60800;
#endif
    //write setting to sensor
    write_global_variable(client);
}
/*----------------------------------------------------------------------------*/
static int als_sensing_time(int intt, int adc, int cycle)
{
    long sensing_us_time;
    int sensing_ms_time;
    int als_intt, als_adc, als_cycle;

    als_intt = als_intt_value[intt>>1];
    als_adc = adc_value[adc>>3];
    als_cycle = cycle_value[cycle];

    LOG_INFO("ALS: INTT=%d, ADC=%d, Cycle=%d \r\n", als_intt, als_adc, als_cycle);

    sensing_us_time = (als_intt + als_adc*2*3) * 2 * als_cycle;
    sensing_ms_time = sensing_us_time / 1000;
#if 0
    if(epl_sensor.osc_sel == EPL_OSC_SEL_4MHZ)
    {
        sensing_ms_time = sensing_ms_time/4;
    }
#endif

    LOG_INFO("[%s]: sensing=%d ms \r\n", __func__, sensing_ms_time);

    return (sensing_ms_time + 5);
}
/*----------------------------------------------------------------------------*/
static int ps_sensing_time(int intt, int adc, int cycle)
{
    long sensing_us_time;
    int sensing_ms_time;
    int ps_intt, ps_adc, ps_cycle;

    ps_intt = ps_intt_value[intt>>1];
    ps_adc = adc_value[adc>>3];
    ps_cycle = cycle_value[cycle];

    LOG_INFO("PS: INTT=%d, ADC=%d, Cycle=%d \r\n", ps_intt, ps_adc, ps_cycle);

    sensing_us_time = (ps_intt*3 + ps_adc*2*2) * ps_cycle;
    sensing_ms_time = sensing_us_time / 1000;
    if(epl_sensor.osc_sel == EPL_OSC_SEL_4MHZ)
    {
        sensing_ms_time = sensing_ms_time/4;
    }

    LOG_INFO("[%s]: sensing=%d ms\r\n", __func__, sensing_ms_time);

    return (sensing_ms_time + 5);
}
/*----------------------------------------------------------------------------*/
void epl_fast_update(struct i2c_client *client)
{
    int als_fast_time = 0;

    LOG_FUN();
    mutex_lock(&sensor_mutex);
    als_fast_time = als_sensing_time(epl_sensor.als.integration_time, epl_sensor.als.adc, EPL_CYCLE_1);
    epl_I2C_Write(client, 0x11, EPL_POWER_OFF | EPL_RESETN_RESET);

    epl_I2C_Write(client, 0x02, epl_sensor.als.adc | EPL_CYCLE_1);
    epl_I2C_Write(client, 0x01, epl_sensor.als.integration_time | epl_sensor.als.gain);
    epl_I2C_Write(client, 0x00, epl_sensor.wait | EPL_MODE_ALS);
    epl_I2C_Write(client, 0x11, EPL_POWER_ON | EPL_RESETN_RUN);
    mutex_unlock(&sensor_mutex);
    msleep(als_fast_time);
    LOG_INFO("[%s]: msleep(%d)\r\n", __func__, als_fast_time);

    mutex_lock(&sensor_mutex);
    if(epl_sensor.als.polling_mode == 0)
    {
        //fast_mode is already ran one frame, so must to reset CMP bit for als intr mode
        //IDLE mode and CMMP reset
        epl_I2C_Write(client, 0x00, epl_sensor.wait | EPL_MODE_IDLE);
        epl_I2C_Write(client, 0x12, EPL_CMP_RESET | EPL_UN_LOCK);
        epl_I2C_Write(client, 0x12, EPL_CMP_RUN | EPL_UN_LOCK);
    }

    epl_I2C_Write(client, 0x11, EPL_POWER_OFF | EPL_RESETN_RESET);
    epl_I2C_Write(client, 0x02, epl_sensor.als.adc | epl_sensor.als.cycle);
    mutex_unlock(&sensor_mutex);
}
/*----------------------------------------------------------------------------*/
void epl_update_mode(struct i2c_client *client)
{
	struct epl_priv *epld = epl_obj;
	int als_time = 0, ps_time = 0;
	bool enable_ps = epld->enable_pflag==1 && epld->ps_suspend==0;
    bool enable_als = epld->enable_lflag==1 && epld->als_suspend==0;
    bool enable_ges = epld->enable_gflag==1 && epld->ges_suspend==0;
#if HS_ENABLE
	bool enable_hs = epld->enable_hflag==1 && epld->hs_suspend==0;
#endif

    als_time = als_sensing_time(epl_sensor.als.integration_time, epl_sensor.als.adc, epl_sensor.als.cycle);
    ps_time = ps_sensing_time(epl_sensor.ps.integration_time, epl_sensor.ps.adc, epl_sensor.ps.cycle);
    epl_als_frame_time = als_time;
    epl_ps_frame_time = ps_time;

    mutex_lock(&sensor_mutex);
    LOG_INFO("mode selection =0x%x\n", enable_ps | (enable_als << 1) | (enable_ges << 2));
#if HS_ENABLE
    if(enable_hs)
    {
        LOG_INFO("[%s]: HS mode \r\n", __func__);
        epl_I2C_Write(client, 0x11, EPL_POWER_ON | EPL_RESETN_RUN);
        epl_restart_polling();
    }
	else
#endif
    {
        //**** mode selection ****
        switch( (enable_ges << 2) | (enable_als << 1) | enable_ps)
        {
            case 0: //disable all
                epl_sensor.mode = EPL_MODE_IDLE;
            break;

            case 1: //ges = 0, als = 0, ps = 1
                epl_sensor.mode = EPL_MODE_PS;
            break;

            case 2: //ges = 0, als = 1, ps = 0
                epl_sensor.mode = EPL_MODE_ALS;
            break;

            case 3: //ges = 0, als = 1, ps = 1
                epl_sensor.mode = EPL_MODE_ALS_PS;
            break;

            case 4: //ges = 1, als = 0, ps = 0
                if (!ges_changed_flag)
                    epl_sensor.mode = EPL_MODE_GES_SS;
                else
                    epl_sensor.mode = ges_change_mode;
            break;

            case 5: //ges = 1, als = 0, ps = 1 //procesing
                epl_sensor.mode = EPL_MODE_PS;
                enable_ges = false;
            break;

            case 6: //ges = 1, als = 1, ps = 0
                if (!ges_changed_flag)
                    epl_sensor.mode = EPL_MODE_GES_SS_ALS;
                else
                {
                    switch(ges_change_mode)
                    {
                        case EPL_MODE_GES:
                            epl_sensor.mode = EPL_MODE_GES_ALS;
                            break;

                        case EPL_MODE_GES_GL:
                            epl_sensor.mode = EPL_MODE_GES_GL_ALS;
                            break;

                        case EPL_MODE_GES_SS:
                            epl_sensor.mode = EPL_MODE_GES_SS_ALS;
                            break;
                    }
                }
            break;

            case 7:  //ges = 1, als = 1, ps = 1
                epl_sensor.mode = EPL_MODE_ALS_PS;
                enable_ges = false;
            break;
        }

        epl_I2C_Write(client, 0x11, EPL_POWER_OFF | EPL_RESETN_RESET);
        // initial factory calibration variable
        read_factory_calibration();

        if(enable_ges)
        {
            epl_I2C_Write(epld->client, 0x00, epl_sensor.ges.wait | epl_sensor.mode);
        }
        else
        {
            epl_I2C_Write(epld->client, 0x00, epl_sensor.wait | epl_sensor.mode);
        }


        if(epl_sensor.mode != EPL_MODE_IDLE)    // if mode isnt IDLE, PWR_ON and RUN    //check this
            epl_I2C_Write(client, 0x11, EPL_POWER_ON | EPL_RESETN_RUN);

        //**** check setting ****
        if(enable_ps == 1)
        {
            LOG_INFO("[%s] PS:low_thd = %d, high_thd = %d \n",__func__, epl_sensor.ps.low_threshold, epl_sensor.ps.high_threshold);
        }

        if(enable_als == 1 && epl_sensor.als.polling_mode == 0)
        {
            LOG_INFO("[%s] ALS:low_thd = %d, high_thd = %d \n",__func__, epl_sensor.als.low_threshold, epl_sensor.als.high_threshold);
        }
        LOG_INFO("[%s] reg0x00= 0x%x\n", __func__,  epl_sensor.wait | epl_sensor.mode);
    	LOG_INFO("[%s] reg0x07= 0x%x\n", __func__, epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type);
    	LOG_INFO("[%s] reg0x06= 0x%x\n", __func__, epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type);
    	LOG_INFO("[%s] reg0x11= 0x%x\n", __func__, epl_sensor.power | epl_sensor.reset);
    	LOG_INFO("[%s] reg0x12= 0x%x\n", __func__, epl_sensor.als.compare_reset | epl_sensor.als.lock);
    	LOG_INFO("[%s] reg0x1b= 0x%x\n", __func__, epl_sensor.ps.compare_reset | epl_sensor.ps.lock);
    	LOG_INFO("[%s] reg0x25= 0x%x\n", __func__, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
    }   /*HS_ENABLE*/

#if PS_DYN_K
    if(enable_ps == 1)
    {
        epl_restart_dynk_polling();
    }
#endif
    //**** start gesture polling schedule *****
    if( (enable_als==1 && epl_sensor.als.polling_mode==1) || (enable_ps==1 && epl_sensor.ps.polling_mode==1) || (enable_ges==1 && epl_sensor.ges.polling_mode==1) )
        epl_restart_polling();

    mutex_unlock(&sensor_mutex);
}
/*----------------------------------------------------------------------------*/
static void epl_polling_work(struct work_struct *work)
{

    struct epl_priv *epld = epl_obj;
    struct i2c_client *client = epld->client;
    u16 report_lux = 0;
    bool enable_ps = epld->enable_pflag==1 && epld->ps_suspend==0;
    bool enable_als = epld->enable_lflag==1 && epld->als_suspend==0;
    bool enable_ges = epld->enable_gflag==1 && epld->ges_suspend==0;
#if HS_ENABLE
    bool enable_hs = epld->enable_hflag==1 && epld->hs_suspend==0;
    LOG_INFO("enable_pflag=%d, enable_lflag=%d, enable_hs=%d, enable_ges=%d \n", enable_ps, enable_als, enable_hs, enable_ges);
#else
    LOG_INFO("enable_pflag = %d, enable_lflag = %d, enable_gflag = %d \n", enable_ps, enable_als, enable_ges);
#endif

    cancel_delayed_work(&epld->polling_work);
#if HS_ENABLE
    if (enable_hs)
    {
        schedule_delayed_work(&epld->polling_work, msecs_to_jiffies(20));
        epl_read_hs(client);
    }
#endif /*HS_ENABLE*/

    if( ((enable_als &&  epl_sensor.als.polling_mode == 1) && !(enable_ges == 1 && epl_sensor.ges.polling_mode == 1)) || (enable_ps &&  epl_sensor.ps.polling_mode == 1))
    {//als open and ges close, ps open
        schedule_delayed_work(&epld->polling_work, msecs_to_jiffies(polling_time));
    }

    if(enable_als &&  epl_sensor.als.polling_mode == 1 && enable_ges == 0)
    {
        epl_read_als(client);
        report_lux = epl_get_als_value(epld, epl_sensor.als.data.channels[1]);
        epl_report_lux(report_lux);
    }

    if(enable_ps && epl_sensor.ps.polling_mode == 1)
    {
#if !PS_DYN_K
        epl_read_ps(client);
#endif
        epl_report_ps_status();
    }
    else if(enable_ges && epl_sensor.ges.polling_mode == 1)
    {
        epl_gesture_rawdata();
        if(epl_sensor.ges.wait == EPL_WAIT_SINGLE)
        {
            epl_I2C_Write(epld->client, 0x11,  EPL_POWER_ON | EPL_RESETN_RUN);
        }

        schedule_delayed_work(&epld->polling_work, msecs_to_jiffies(sleep_mode==1?20:10));
    }

#if HS_ENABLE
    if(enable_als==false && enable_ps==false && enable_hs==false && enable_ges==false)
#else
    if(enable_als==false && enable_ps==false && enable_ges==false)
#endif
    {
        cancel_delayed_work(&epld->polling_work);
        LOG_INFO("disable sensor\n");
    }

}

#if PS_DYN_K
/*----------------------------------------------------------------------------*/
void epl_restart_dynk_polling(void)
{
    struct epl_priv *epld = epl_obj;

    cancel_delayed_work(&epld->dynk_thd_polling_work);
    schedule_delayed_work(&epld->dynk_thd_polling_work, msecs_to_jiffies(2*dynk_polling_delay));
}
/*----------------------------------------------------------------------------*/
void epl_dynk_thd_polling_work(struct work_struct *work)
{
    struct epl_priv *obj = epl_obj;
    bool enable_ps = obj->enable_pflag==1 && obj->ps_suspend==0;
    bool enable_als = obj->enable_lflag==1 && obj->als_suspend==0;

    LOG_INFO("[%s]:als / ps enable: %d / %d\n", __func__,enable_als, enable_ps);

    if(enable_ps == true)
    {
        epl_read_ps(obj->client);
        if( (epl_dynk_min_ps_raw_data > epl_sensor.ps.data.data)
            && (epl_sensor.ps.saturation == 0)
            && (epl_sensor.ps.data.ir_data < epl_dynk_max_ir_data) )
        {
            epl_dynk_min_ps_raw_data = epl_sensor.ps.data.data;
            epl_dynk_thd_low = epl_dynk_min_ps_raw_data + epl_dynk_low_offset;
		    epl_dynk_thd_high = epl_dynk_min_ps_raw_data + epl_dynk_high_offset;

            if(epl_dynk_thd_low>65534)
                epl_dynk_thd_low = 65534;
            if(epl_dynk_thd_high>65535)
                epl_dynk_thd_high = 65535;

		    LOG_INFO("[%s]:dyn ps raw = %d, min = %d, ir_data = %d\n", __func__, epl_sensor.ps.data.data, epl_dynk_min_ps_raw_data, epl_sensor.ps.data.ir_data);
		    set_psensor_intr_threshold((u16)epl_dynk_thd_low, (u16)epl_dynk_thd_high);
		    LOG_INFO("[%s]:dyn k thre_l = %ld, thre_h = %ld\n", __func__, (long)epl_dynk_thd_low, (long)epl_dynk_thd_high);
        }
        schedule_delayed_work(&obj->dynk_thd_polling_work, msecs_to_jiffies(dynk_polling_delay));
    }
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
static void epl_intr_als_report_lux(void)
{
    struct epl_priv *obj = epl_obj;
    u16 als;
//#if ALS_DYN_INTT
//    int last_dynamic_intt_idx = dynamic_intt_idx;
//#endif
    LOG_INFO("[%s]: IDEL MODE \r\n", __func__);
    mutex_lock(&sensor_mutex);
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | EPL_MODE_IDLE);
    mutex_unlock(&sensor_mutex);

    als = epl_get_als_value(obj, epl_sensor.als.data.channels[1]);

    mutex_lock(&sensor_mutex);
    epl_I2C_Write(obj->client, 0x12, EPL_CMP_RESET | EPL_UN_LOCK);
	epl_I2C_Write(obj->client, 0x11, EPL_POWER_OFF | EPL_RESETN_RESET);  //After runing CMP_RESET, dont clean interrupt_flag
    mutex_unlock(&sensor_mutex);

//#if ALS_DYN_INTT
//    if(dynamic_intt_idx == last_dynamic_intt_idx)
//#endif
    {
        LOG_INFO("[%s]: report als = %d \r\n", __func__, als);
        epl_report_lux(als);
    }

    if(epl_sensor.als.report_type == CMC_BIT_INTR_LEVEL || epl_sensor.als.report_type == CMC_BIT_DYN_INT)
    {
#if ALS_DYN_INTT
        if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
        {
            if(epl_dynamic_intt_idx == 0)
            {
                int gain_value = 0;
                int normal_value = 0;
                long low_thd = 0, high_thd = 0;

                if(epl_sensor.als.gain == EPL_GAIN_MID)
                {
                    gain_value = 8;
                }
                else if (epl_sensor.als.gain == EPL_GAIN_LOW)
                {
                    gain_value = 1;
                }
                normal_value = gain_value * als_dynamic_intt_value[epl_dynamic_intt_idx] / als_dynamic_intt_value[1];

                if((als == 0)) //Note: als =0 is nothing
                    low_thd = 0;
                else
                    low_thd = (*(epl_als_adc_intr_level + (als-1)) + 1) * normal_value;
                high_thd = (*(epl_als_adc_intr_level + als)) * normal_value;

                if(low_thd > 0xfffe)
                    low_thd = 65533;
                if(high_thd >= 0xffff)
                    high_thd = 65534;

                epl_sensor.als.low_threshold = low_thd;
                epl_sensor.als.high_threshold = high_thd;

            }
            else
            {
                epl_sensor.als.low_threshold = *(epl_als_adc_intr_level + (als-1)) + 1;
                epl_sensor.als.high_threshold = *(epl_als_adc_intr_level + als);

                if(epl_sensor.als.low_threshold == 0)
                    epl_sensor.als.low_threshold = 1;
            }
            LOG_INFO("[%s]:epl_dynamic_intt_idx=%d, thd(%d/%d) \r\n", __func__, epl_dynamic_intt_idx, epl_sensor.als.low_threshold, epl_sensor.als.high_threshold);
        }
        else
#endif
        {
            epl_sensor.als.low_threshold = *(epl_als_adc_intr_level + (als-1)) + 1;
            epl_sensor.als.high_threshold = *(epl_als_adc_intr_level + als);
            if( (als == 0) || (epl_sensor.als.data.channels[1] == 0) )
            {
                epl_sensor.als.low_threshold = 0;
            }
        }
    }

	//write new threshold
	set_lsensor_intr_threshold(epl_sensor.als.low_threshold, epl_sensor.als.high_threshold);
    mutex_lock(&sensor_mutex);
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);
    epl_I2C_Write(obj->client, 0x11, EPL_POWER_ON | EPL_RESETN_RUN);
    mutex_unlock(&sensor_mutex);
    LOG_INFO("[%s]: MODE=0x%x \r\n", __func__, epl_sensor.mode);
}
/*----------------------------------------------------------------------------*/
static irqreturn_t epl_eint_func(int irqNo, void *handle)
{
    struct epl_priv *epld = (struct epl_priv*)handle;

    disable_irq_nosync(epld->irq);
    schedule_delayed_work(&epld->eint_work, 0);
    return IRQ_HANDLED;
}
/*----------------------------------------------------------------------------*/
static void epl_eint_work(struct work_struct *work)
{
    struct epl_priv *epld = epl_obj;

	bool enable_ps = epld->enable_pflag==1 && epld->ps_suspend==0;
    bool enable_ges = epld->enable_gflag==1 && epld->ges_suspend==0;
    bool isWork;

    if(enable_ges && enable_ps == 0)
    {
        epl_gesture_rawdata();
        isWork=(channel_data[0]-crosstalk)>=WORK_TH;

        if(epl_sensor.ges.wait == EPL_WAIT_SINGLE)
        {
            if((sleep_mode==1 && isWork) || sleep_reset==1)
            {
                epl_I2C_Write(epld->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
                sleep_mode=0;
                sleep_reset=0;

                if(GESTURE_DEBUG>=2)
                    GES_LOG("sleep mode: %d\n", sleep_mode);
            }
            else if(sleep_mode==0 && isWork==false && sleep_count>SLEEP_PERIOD)
            {
                epl_I2C_Write(epld->client, 0x24, EPL_GES_ADC_14 | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
                sleep_mode=1;

               if(GESTURE_DEBUG>=2)
                    GES_LOG("sleep mode: %d\n", sleep_mode);
            }
            epl_I2C_Write(epld->client,0x11,  EPL_POWER_ON | EPL_RESETN_RUN);
        }
    }
    else
    {
        LOG_INFO("xxxxxxxxxxx\n\n");

        epl_read_ps(epld->client);
        epl_read_als(epld->client);
        if(epl_sensor.ps.interrupt_flag == EPL_INT_TRIGGER)
        {
            if(enable_ps)
            {
                wake_lock_timeout(&ps_lock, 2*HZ);
                epl_report_ps_status();
            }
            //PS unlock interrupt pin and restart chip
            mutex_lock(&sensor_mutex);
    		epl_I2C_Write(epld->client,0x1b, EPL_CMP_RUN | EPL_UN_LOCK);
    		mutex_unlock(&sensor_mutex);
        }

        if(epl_sensor.als.interrupt_flag == EPL_INT_TRIGGER)
        {
            epl_intr_als_report_lux();
            //ALS unlock interrupt pin and restart chip
        	mutex_lock(&sensor_mutex);
        	epl_I2C_Write(epld->client,0x12, EPL_CMP_RUN | EPL_UN_LOCK);
        	mutex_unlock(&sensor_mutex);
        }
    }

	enable_irq(epld->irq);
}
/*----------------------------------------------------------------------------*/
static int epl_setup_interrupt(struct epl_priv *epld)
{
    struct i2c_client *client = epld->client;

    int err = 0;
#if QCOM || MARVELL
    unsigned int irq_gpio;
    unsigned int irq_gpio_flags;
    struct device_node *np = client->dev.of_node;
#endif
    msleep(5);
#if S5PV210
    epld->intr_pin = S5PV210_GPH0(1);
    err = gpio_request(S5PV210_GPH0(1), "Elan EPL IRQ");
    if (err)
    {
        LOG_ERR("gpio pin request fail (%d)\n", err);
        goto initial_fail;
    }
    else
    {
        LOG_INFO("----- Samsung gpio config success -----\n");
        s3c_gpio_cfgpin(S5PV210_GPH0(1),S3C_GPIO_SFN(0x0F)/*(S5PV210_GPH0_1_EXT_INT30_1) */);
        s3c_gpio_setpull(S5PV210_GPH0(1),S3C_GPIO_PULL_UP);

    }
    epld->irq = gpio_to_irq(epld->intr_pin);
#elif SPREAD
    epld->intr_pin = ELAN_INT_PIN; /*need setting*/
    err = gpio_request(epld->intr_pin, "Elan EPL IRQ");
    if (err)
    {
        LOG_ERR("gpio pin request fail (%d)\n", err);
        goto initial_fail;
    }
    else
    {
		gpio_direction_input(epld->intr_pin);

		/*get irq*/
		client->irq = gpio_to_irq(epld->intr_pin);
		epld->irq = client->irq;

		LOG_INFO("IRQ number is %d\n", client->irq);

    }
#elif QCOM
    irq_gpio = of_get_named_gpio_flags(np, "epl,irq-gpio", 0, &irq_gpio_flags);
    //irq_gpio = ELAN_INT_PIN;
    epld->intr_pin = irq_gpio;
    if (epld->intr_pin < 0) {
        goto initial_fail;
    }

    if (gpio_is_valid(epld->intr_pin)) {
            err = gpio_request(epld->intr_pin, "epl_irq_gpio");
            if (err) {
                LOG_ERR( "irq gpio request failed");
                goto initial_fail;
            }

            err = gpio_direction_input(epld->intr_pin);
            if (err) {
                    LOG_ERR("set_direction for irq gpio failed\n");
                    goto initial_fail;
            }
    }
    epld->irq = gpio_to_irq(epld->intr_pin);
#elif LEADCORE
    //epld->intr_pin = ELAN_INT_PIN; /*need setting*/
	epld->intr_pin = irq_to_gpio(client->irq); /*need confirm*/


	err = gpio_request(epld->intr_pin, "epl irq");
	//err = gpio_request(epld->intr_pin, "epl irq");
	if (err < 0){
		LOG_ERR("%s:Gpio request failed! \r\n", __func__);
		goto initial_fail;
	}
	gpio_direction_input(epld->intr_pin);


	epld->irq = gpio_to_irq(epld->intr_pin);
#elif MARVELL

    //epld->intr_pin = ELAN_INT_PIN; /*need setting*/
    epld->intr_pin = of_get_named_gpio_flags(np, "epl,irq-gpio", 0, &irq_gpio_flags);
    if(client->irq <= 0)
    {
        LOG_ERR("client->irq(%d) Failed \r\n", client->irq);
        goto initial_fail;
    }
    gpio_request(epld->intr_pin, "epl irq");
    gpio_direction_input(epld->intr_pin);
    epld->irq = gpio_to_irq(epld->intr_pin);
#endif
    err = request_irq(epld->irq,epl_eint_func, IRQF_TRIGGER_LOW, //IRQF_TRIGGER_FALLING, //IRQF_TRIGGER_LOW
                      client->dev.driver->name, epld);
    if(err <0)
    {
        LOG_ERR("request irq pin %d fail for gpio\n",err);
        goto fail_free_intr_pin;
    }

    return err;

initial_fail:
fail_free_intr_pin:
    gpio_free(epld->intr_pin);
//    free_irq(epld->irq, epld);
    return err;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_reg(struct device *dev, struct device_attribute *attr, char *buf)
{
    ssize_t len = 0;
    struct i2c_client *client = epl_obj->client;

    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x00 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x00));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x01 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x01));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x02 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x02));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x03 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x03));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x04 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x04));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x05 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x05));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x06 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x06));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x07 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x07));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x08 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x08));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x09 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x09));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x0A value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x0A));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x0B value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x0B));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x0C value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x0C));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x0D value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x0D));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x0E value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x0E));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x0F value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x0F));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x11 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x11));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x12 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x12));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x13 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x13));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x14 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x14));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x15 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x15));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x16 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x16));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x1B value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x1B));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x1C value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x1C));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x1D value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x1D));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x1E value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x1E));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x1F value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x1F));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x22 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x22));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x23 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x23));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x24 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x24));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0x25 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x25));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0xFB value = 0x%x\n", i2c_smbus_read_byte_data(client, 0xFB));
    len += snprintf(buf+len, PAGE_SIZE-len, "chip id REG 0xFC value = 0x%x\n", i2c_smbus_read_byte_data(client, 0xFC));

    return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_status(struct device *dev, struct device_attribute *attr, char *buf)
{
    ssize_t len = 0;
    struct epl_priv *epld = epl_obj;
    bool enable_ps = epld->enable_pflag==1 && epld->ps_suspend==0;
    bool enable_als = epld->enable_lflag==1 && epld->als_suspend==0;
    bool enable_ges = epld->enable_gflag==1 && epld->ges_suspend==0;
#if HS_ENABLE
	bool enable_hs = epld->enable_hflag==1 && epld->hs_suspend==0;
#endif
    if(!epl_obj)
    {
        LOG_ERR("epl_obj is null!!\n");
        return 0;
    }
    len += snprintf(buf+len, PAGE_SIZE-len, "chip is %s, ver is %s \n", EPL_DEV_NAME, DRIVER_VERSION);
    len += snprintf(buf+len, PAGE_SIZE-len, "als/ps polling is %d-%d\n", epl_sensor.als.polling_mode, epl_sensor.ps.polling_mode);
    len += snprintf(buf+len, PAGE_SIZE-len, "wait = %d, mode = %d, osc=%d \n",epl_sensor.wait >> 4, epl_sensor.mode, epl_sensor.osc_sel);
    len += snprintf(buf+len, PAGE_SIZE-len, "interrupt control = %d\n", epl_sensor.interrupt_control >> 4);
    len += snprintf(buf+len, PAGE_SIZE-len, "frame time ps=%dms, als=%dms\n", epl_ps_frame_time, epl_als_frame_time);
#if HS_ENABLE
    if(enable_hs)
    {
        len += snprintf(buf+len, PAGE_SIZE-len, "hs adc= %d\n", epl_sensor.hs.adc>>3);
        len += snprintf(buf+len, PAGE_SIZE-len, "hs int_time= %d\n", epl_sensor.hs.integration_time>>1);
        len += snprintf(buf+len, PAGE_SIZE-len, "hs cycle= %d\n", epl_sensor.hs.cycle);
        len += snprintf(buf+len, PAGE_SIZE-len, "hs gain= %d\n", epl_sensor.hs.gain);
        len += snprintf(buf+len, PAGE_SIZE-len, "hs ch1 raw= %d\n", epl_sensor.hs.raw);
    }
#endif
    if(enable_ps)
    {
    len += snprintf(buf+len, PAGE_SIZE-len, "PS: \n");
    len += snprintf(buf+len, PAGE_SIZE-len, "INTEG = %d, gain = %d\n", epl_sensor.ps.integration_time >> 1, epl_sensor.ps.gain);
    len += snprintf(buf+len, PAGE_SIZE-len, "adc = %d, cycle = %d, ir drive = %d\n", epl_sensor.ps.adc >> 3, epl_sensor.ps.cycle, epl_sensor.ps.ir_drive);
    len += snprintf(buf+len, PAGE_SIZE-len, "saturation = %d, int flag = %d\n", epl_sensor.ps.saturation >> 5, epl_sensor.ps.interrupt_flag >> 2);
    len += snprintf(buf+len, PAGE_SIZE-len, "Thr(L/H) = (%d/%d)\n", epl_sensor.ps.low_threshold, epl_sensor.ps.high_threshold);
#if PS_DYN_K
        len += snprintf(buf+len, PAGE_SIZE-len, "Dyn thr(L/H) = (%ld/%ld)\n", (long)epl_dynk_thd_low, (long)epl_dynk_thd_high);
#endif
    len += snprintf(buf+len, PAGE_SIZE-len, "pals data = %d, data = %d\n", epl_sensor.ps.data.ir_data, epl_sensor.ps.data.data);
    }
    if(enable_als)
    {
    len += snprintf(buf+len, PAGE_SIZE-len, "ALS: \n");
    len += snprintf(buf+len, PAGE_SIZE-len, "INTEG = %d, gain = %d\n", epl_sensor.als.integration_time >> 1, epl_sensor.als.gain);
    len += snprintf(buf+len, PAGE_SIZE-len, "adc = %d, cycle = %d\n", epl_sensor.als.adc >> 3, epl_sensor.als.cycle);
#if ALS_DYN_INTT
    len += snprintf(buf+len, PAGE_SIZE-len, "epl_c_gain = %d\n", epl_c_gain);
    len += snprintf(buf+len, PAGE_SIZE-len, "epl_dynamic_intt_lux = %d\n", epl_dynamic_intt_lux);
#endif
    if(epl_sensor.als.polling_mode == 0)
        len += snprintf(buf+len, PAGE_SIZE-len, "Thr(L/H) = (%d/%d)\n", epl_sensor.als.low_threshold, epl_sensor.als.high_threshold);
    }
    len += snprintf(buf+len, PAGE_SIZE-len, "ch0 = %d, ch1 = %d\n", epl_sensor.als.data.channels[0], epl_sensor.als.data.channels[1]);
    if(enable_ges)
    {
        len += snprintf(buf+len, PAGE_SIZE-len, "crosstalk:%d\n", crosstalk);
    }

    return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_als_enable(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    uint16_t mode=0;

    sscanf(buf, "%hu", &mode);
    LOG_INFO("[%s]: als enable = %d \r\n", __func__, mode);
    epl_enable_als(mode);

    return count;
}

/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ps_enable(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    uint16_t mode=0;

    sscanf(buf, "%hu", &mode);

    LOG_INFO("[%s]: ps enable=%d \r\n", __func__, mode);
    epl_enable_ps(mode);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_cal_raw(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct epl_priv *obj = epl_obj;
    u16 ch1=0;
    ssize_t len = 0;

    if(!obj)
    {
        LOG_ERR("obj is null!!\n");
        return 0;
    }

    switch(epl_sensor.mode)
    {
		case EPL_MODE_PS:
            ch1 = epl_factory_ps_data();
        break;

        case EPL_MODE_ALS:
            ch1 = epl_factory_als_data();
        break;
    }

    LOG_INFO("cal_raw = %d \r\n" , ch1);
    len += snprintf(buf + len, PAGE_SIZE - len, "%d \r\n", ch1);

    return  len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_threshold(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *obj = epl_obj;
    int low, high;

    if(!obj)
    {
        LOG_ERR("epl_obj is null!!\n");
        return 0;
    }

    sscanf(buf, "%d,%d", &low, &high);
    LOG_INFO("[%s]: mode=0x%x, thd=%d/%d \r\n", __func__, epl_sensor.mode, low, high);
    switch(epl_sensor.mode)
    {
		case EPL_MODE_PS:
            epl_sensor.ps.low_threshold = low;
            epl_sensor.ps.high_threshold = high;
            set_psensor_intr_threshold(epl_sensor.ps.low_threshold, epl_sensor.ps.high_threshold);
        break;

        case EPL_MODE_ALS:
            epl_sensor.als.low_threshold = low;
            epl_sensor.als.high_threshold = high;
            set_lsensor_intr_threshold(epl_sensor.als.low_threshold, epl_sensor.als.high_threshold);
        break;

    }

    return  count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_threshold(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct epl_priv *obj = epl_obj;
    ssize_t len = 0;

    if(!obj)
    {
        LOG_ERR("epl_obj is null!!\n");
        return 0;
    }

    switch(epl_sensor.mode)
    {
		case EPL_MODE_PS:
            len += snprintf(buf + len, PAGE_SIZE - len, "epl_sensor.ps.low_threshold=%d \r\n", epl_sensor.ps.low_threshold);
            len += snprintf(buf + len, PAGE_SIZE - len, "epl_sensor.ps.high_threshold=%d \r\n", epl_sensor.ps.high_threshold);
        break;

        case EPL_MODE_ALS:
            len += snprintf(buf + len, PAGE_SIZE - len, "epl_sensor.als.low_threshold=%d \r\n", epl_sensor.als.low_threshold);
            len += snprintf(buf + len, PAGE_SIZE - len, "epl_sensor.als.high_threshold=%d \r\n", epl_sensor.als.high_threshold);
        break;

    }
    return  len;

}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_enable(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int mode=0;
    struct epl_priv *obj = epl_obj;
    struct i2c_client *client = obj->client;

    sscanf(buf, "%d",&mode);

    if(mode)
    {
        if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
        {
            ges_als_report_type = epl_sensor.als.report_type;
            epl_sensor.als.report_type = CMC_BIT_PRE_COUNT;
        }
        obj->enable_gflag = 1;
    }
    else
    {
        if(ges_als_report_type == CMC_BIT_DYN_INT)
        {
            epl_sensor.als.report_type = CMC_BIT_DYN_INT;
        }
        obj->enable_gflag = 0;
    }
    LOG_INFO("[%s]: enable %d\n", __func__, mode);

    set_gesture_mode(mode);
    epl_update_mode(client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_ges_raws(struct device *dev, struct device_attribute *attr, char *buf)
{
    u16 *tmp = (u16*)buf;
    u16 length = ges_count+1; // 0 is count, last is ir
    int byte_count = length*2;
    int i=0;

    tmp[0]= length-1;
    for(i=0; i<length-1; i++)
    {
        tmp[i+1]= ges_raws[i];
        ges_raws[i]=0;
    }
    ges_count=0;
    return byte_count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_int_time(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *obj = epl_obj;
    int val;

    sscanf(buf, "%d",&val);
    epl_sensor.ges.integration_time = (val & 0x1f) << 1;

    LOG_INFO("%s: 0x24 = 0x%x\n", __FUNCTION__, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);

    epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_wait_time(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int val;

    sscanf(buf, "%d",&val);

    switch(epl_sensor.mode)
    {
        case EPL_MODE_PS:
        case EPL_MODE_ALS:
        case EPL_MODE_ALS_PS:
            epl_sensor.wait = (val & 0xf) << 4;
        break;

        case EPL_MODE_GES:
            epl_sensor.ges.wait = (val & 0xf) << 4;
        break;

        case EPL_MODE_GES_GL:
            epl_sensor.ges.wait = (val & 0xf) << 4;
        break;

        case EPL_MODE_GES_SS:
            epl_sensor.ges.wait = (val & 0xf) << 4;
        break;
    }

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_opt(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d",&fir_filter_order);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_int_filter_order(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d",&int_filter_order);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_debug(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d",&GESTURE_DEBUG);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_work_threshold(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d",&WORK_TH);
    crosstalk_offset=WORK_TH/3;
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_polling_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *obj = epl_obj;
    int value;
    sscanf(buf, "%d", &value);
    epl_sensor.ges.polling_mode = value>0?true:false;

    //set gesture interrupt enable / disable
    if(epl_sensor.ges.polling_mode == false)
    {
        epl_sensor.ges.interrupt_enable = EPL_GES_INT_EN;
    }
    else
    {
        epl_sensor.ges.interrupt_enable = EPL_GES_INT_DIS;
    }

    epl_I2C_Write(obj->client, 0x25, (epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive) );
    epl_update_mode(obj->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_gain(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct epl_priv *epld = epl_obj;
	int value = 0;
    LOG_FUN();

    sscanf(buf, "%d", &value);

    value = value & 0x03;
	switch (epl_sensor.mode)
	{
		case EPL_MODE_PS:
            epl_sensor.ps.gain = value;
	        epl_I2C_Write(epld->client, 0x03, epl_sensor.ps.integration_time | epl_sensor.ps.gain);
		break;

        case EPL_MODE_ALS: //als
            epl_sensor.als.gain = value;
	        epl_I2C_Write(epld->client, 0x01, epl_sensor.als.integration_time | epl_sensor.als.gain);
		break;

		case EPL_MODE_GES: //ges
            epl_sensor.ges.gain = value;
	        epl_I2C_Write(epld->client, 0x24, epl_sensor.ges.integration_time | epl_sensor.ges.gain);
		break;
    }
	epl_update_mode(epld->client);

	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d", &ges_mode);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_zoom_delta(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d", &zoom_delta);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *obj = epl_obj;
    int value=0;
    LOG_FUN();
    obj->enable_pflag = 0;
    obj->enable_lflag = 0;
    obj->enable_gflag = 0;

    sscanf(buf, "%d",&value);

    switch (value)
    {
        case 0:
            epl_sensor.mode = EPL_MODE_IDLE;
        break;

        case 1:
            obj->enable_lflag = 1;
            epl_sensor.mode = EPL_MODE_ALS;
        break;

        case 2:
            obj->enable_pflag = 1;
            epl_sensor.mode = EPL_MODE_PS;
        break;

        case 3:
            obj->enable_lflag = 1;
            obj->enable_pflag = 1;
            epl_sensor.mode = EPL_MODE_ALS_PS;
        break;

        case 4:
            obj->enable_gflag = 1;
            ges_changed_flag = true;
            ges_change_mode = EPL_MODE_GES;
        break;

        case 5:
            obj->enable_gflag = 1;
            ges_changed_flag = true;
            ges_change_mode = EPL_MODE_GES_GL;
        break;

        case 6:
            obj->enable_gflag = 1;
            ges_changed_flag = true;
            ges_change_mode = EPL_MODE_GES_SS;
        break;

        case 7:
            obj->enable_gflag = 1;
            obj->enable_lflag = 1;
            ges_changed_flag = true;
            ges_change_mode = EPL_MODE_GES_ALS;
        break;

        case 8:
            obj->enable_gflag = 1;
            obj->enable_lflag = 1;
            ges_changed_flag = true;
            ges_change_mode = EPL_MODE_GES_GL_ALS;
        break;

        case 9:
            obj->enable_gflag = 1;
            obj->enable_lflag = 1;
            ges_changed_flag = true;
            ges_change_mode = EPL_MODE_GES_SS_ALS;
        break;
    }

    epl_update_mode(obj->client);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ir_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();
    sscanf(buf, "%d", &value);

    switch (epl_sensor.mode)
    {
		case EPL_MODE_PS:
            switch(value)
            {
                case 0:
                    epl_sensor.ps.ir_mode = EPL_IR_MODE_CURRENT;
                break;

                case 1:
                    epl_sensor.ps.ir_mode = EPL_IR_MODE_VOLTAGE;
                break;
            }
            epl_I2C_Write(obj->client, 0x05, epl_sensor.ps.ir_on_control | epl_sensor.ps.ir_mode | epl_sensor.ps.ir_boost | epl_sensor.ps.ir_drive);
        break;


        case EPL_MODE_GES_SS: //ps
            switch(value)
            {
                case 0:
                    epl_sensor.ges.ir_mode = EPL_IR_MODE_CURRENT;
                break;

                case 1:
                    epl_sensor.ges.ir_mode = EPL_IR_MODE_VOLTAGE;
                break;
            }
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;
    }
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ir_contrl(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    uint8_t  data;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();
    sscanf(buf, "%d",&value);

    switch (epl_sensor.mode)
    {
		case EPL_MODE_PS:
            switch(value)
            {
                case 0:
                    epl_sensor.ps.ir_on_control = EPL_IR_ON_CTRL_OFF;
                break;

                case 1:
                    epl_sensor.ps.ir_on_control = EPL_IR_ON_CTRL_ON;
                break;
            }
            data = epl_sensor.ps.ir_on_control | epl_sensor.ps.ir_mode | epl_sensor.ps.ir_boost | epl_sensor.ps.ir_drive;
            LOG_INFO("%s: 0x05 = 0x%x\n", __FUNCTION__, data);

            epl_I2C_Write(obj->client, 0x05, epl_sensor.ps.ir_on_control | epl_sensor.ps.ir_mode | epl_sensor.ps.ir_boost | epl_sensor.ps.ir_drive);
        break;
    }
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ir_boost(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int data;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();

    sscanf(buf, "%d", &data);

    switch(epl_sensor.mode)
    {
		case EPL_MODE_PS:
            epl_sensor.ps.ir_boost = (data & 0x03) << 2;
            epl_I2C_Write(obj->client, 0x05, epl_sensor.ps.ir_on_control | epl_sensor.ps.ir_mode | epl_sensor.ps.ir_boost | epl_sensor.ps.ir_drive);
        break;

        case EPL_MODE_GES:
            epl_sensor.ges.ir_boost = (data & 0x03) << 2;
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;

        case EPL_MODE_GES_GL:
            epl_sensor.ges.ir_boost = (data & 0x03) << 2;
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;

        case EPL_MODE_GES_SS:
            epl_sensor.ges.ir_boost = (data & 0x03) << 2;
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;
    }
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ir_drive(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();

    sscanf(buf, "%d", &value);

    switch(epl_sensor.mode)
    {
		case EPL_MODE_PS:
            epl_sensor.ps.ir_drive = (value & 0x03);
            epl_I2C_Write(obj->client, 0x05, epl_sensor.ps.ir_on_control | epl_sensor.ps.ir_mode | epl_sensor.ps.ir_boost | epl_sensor.ps.ir_drive);
        break;

        case EPL_MODE_GES:
            epl_sensor.ges.ir_drive = (value & 0x03);
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;

        case EPL_MODE_GES_GL:
            epl_sensor.ges.ir_drive = (value & 0x03);
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;

        case EPL_MODE_GES_SS:
            epl_sensor.ges.ir_drive = (value & 0x03);
            epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
        break;
    }
    epl_I2C_Write(obj->client, 0x00, epl_sensor.wait | epl_sensor.mode);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_interrupt_type(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();

    sscanf(buf, "%d",&value);

    switch (epl_sensor.mode)
    {
		case EPL_MODE_PS:
            if(!epl_sensor.ps.polling_mode)
            {
                epl_sensor.ps.interrupt_type = value & 0x03;
                epl_I2C_Write(obj->client, 0x06, epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type);
                LOG_INFO("%s: 0x06 = 0x%x\n", __FUNCTION__, epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type);
            }
        break;

        case EPL_MODE_ALS: //als
            if(!epl_sensor.als.polling_mode)
            {
                epl_sensor.als.interrupt_type = value & 0x03;
                epl_I2C_Write(obj->client, 0x07, epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type);
                LOG_INFO("%s: 0x07 = 0x%x\n", __FUNCTION__, epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type);
            }
        break;

        case EPL_MODE_GES:
            if(!epl_sensor.ges.polling_mode)
            {
                epl_sensor.ges.interrupt_enable = value & 0x80;
                LOG_INFO("%s: 0x25 = 0x%x\n", __FUNCTION__, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
                epl_I2C_Write(obj->client,0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
            }
        break;

        case EPL_MODE_GES_GL:
            if(!epl_sensor.ges.polling_mode)
            {
                epl_sensor.ges.interrupt_enable = value & 0x80;
                LOG_INFO("%s: 0x25 = 0x%x\n", __FUNCTION__, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
                epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
            }
        break;

        case EPL_MODE_GES_SS:
            if(!epl_sensor.ges.polling_mode)
            {
                epl_sensor.ges.interrupt_enable = value & 0x80;
                LOG_INFO("%s: 0x25 = 0x%x\n", __FUNCTION__, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
                epl_I2C_Write(obj->client, 0x25, epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive);
            }
        break;
    }
    epl_update_mode(obj->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ges_index(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    uint8_t  data;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();

    sscanf(buf, "%d", &value);
    switch(value)
    {
        case 0:
            epl_sensor.ges.interrupt_index = EPL_INDEX_1;
        break;

        case 1:
            epl_sensor.ges.interrupt_index = EPL_INDEX_2;
        break;

        case 2:
            epl_sensor.ges.interrupt_index = EPL_INDEX_4;
        break;

        case 3:
            epl_sensor.ges.interrupt_index = EPL_INDEX_8;
        break;
    }

    data = epl_sensor.ges.interrupt_enable | epl_sensor.ges.interrupt_index | epl_sensor.ges.ir_mode | epl_sensor.ges.ir_boost | epl_sensor.ges.ir_drive;
    LOG_INFO("%s: 0x25 = 0x%x\n", __FUNCTION__, data);
    epl_I2C_Write(obj->client, 0x25, data);
    epl_update_mode(obj->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ps_offset(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int data = 0;
    struct epl_priv *obj = epl_obj;
    LOG_FUN();

    sscanf(buf, "%d",&data);

    epl_I2C_Write(obj->client, 0x22, (u8)(data & 0xff));
    epl_I2C_Write(obj->client, 0x23, (u8)((data & 0xff00) >> 8));

    epl_update_mode(obj->client);

    return count;
}

/*----------------------------------------------------------------------------*/
static ssize_t epl_store_osc(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    uint8_t  data;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();
    sscanf(buf, "%d", &value);

    switch(value)
    {
        case 0:
            epl_sensor.osc_sel = EPL_OSC_SEL_1MHZ;
        break;

        case 1:
            epl_sensor.osc_sel = EPL_OSC_SEL_4MHZ;
        break;

    }
    data = epl_sensor.osc_sel | EPL_GFIN_ENABLE | EPL_VOS_ENABLE | EPL_DOC_ON;
    LOG_INFO("%s: 0xfc = 0x%x \n", __FUNCTION__, data);
    epl_I2C_Write(obj->client, 0xfc, data);
    epl_update_mode(obj->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_integration(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();
    sscanf(buf, "%d",&value);

    switch (epl_sensor.mode)
    {
		case EPL_MODE_PS:
            epl_sensor.ps.integration_time = (value & 0x1f) << 1;
            epl_I2C_Write(obj->client, 0x03, epl_sensor.ps.integration_time | epl_sensor.ps.gain);
            epl_I2C_Read(obj->client, 0x03, 1);
            LOG_INFO("%s: 0x03 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.ps.integration_time | epl_sensor.ps.gain, gRawData.raw_bytes[0]);
        break;

        case EPL_MODE_ALS: //als
            epl_sensor.als.integration_time = (value & 0x1f) << 1;
            epl_I2C_Write(obj->client, 0x01, epl_sensor.als.integration_time | epl_sensor.als.gain);
            epl_I2C_Read(obj->client, 0x01, 1);
            LOG_INFO("%s: 0x01 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.als.integration_time | epl_sensor.als.gain, gRawData.raw_bytes[0]);
        break;

        case EPL_MODE_GES:
            epl_sensor.ges.integration_time = (value & 0x1f) << 1;
            epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
            epl_I2C_Read(obj->client, 0x24, 1);
            LOG_INFO("%s: 0x24 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain, gRawData.raw_bytes[0]);
        break;

        case EPL_MODE_GES_GL:
            epl_sensor.ges.integration_time = (value & 0x1f) << 1;
            epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
            epl_I2C_Read(obj->client, 0x24, 1);
            LOG_INFO("%s: 0x24 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain, gRawData.raw_bytes[0]);
        break;

        case EPL_MODE_GES_SS:
            epl_sensor.ges.integration_time = (value & 0x1f) << 1;
            epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
            epl_I2C_Read(obj->client, 0x24, 1);
            LOG_INFO("%s: 0x24 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain, gRawData.raw_bytes[0]);
        break;
    }
    epl_update_mode(obj->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_adc(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;
    struct epl_priv *obj = epl_obj;

    LOG_FUN();
    sscanf(buf, "%d",&value);

    switch (epl_sensor.mode)
    {
		case EPL_MODE_PS:
            epl_sensor.ps.adc = (value & 0x3) << 3;
            epl_I2C_Write(obj->client, 0x04, epl_sensor.ps.adc | epl_sensor.ps.cycle);
        break;

        case EPL_MODE_ALS: //als
            epl_sensor.als.adc = (value & 0x3) << 3;
            epl_I2C_Write(obj->client, 0x02, epl_sensor.als.adc | epl_sensor.als.cycle);
        break;

        case EPL_MODE_GES:
            epl_sensor.ges.adc = (value & 0x3) << 6;
            epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
        break;

        case EPL_MODE_GES_GL:
            epl_sensor.ges.adc = (value & 0x3) << 6;
            epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
        break;

        case EPL_MODE_GES_SS:
            epl_sensor.ges.adc = (value & 0x3) << 6;
            epl_I2C_Write(obj->client, 0x24, epl_sensor.ges.adc | epl_sensor.ges.integration_time |epl_sensor.ges.gain);
        break;
    }
    epl_update_mode(obj->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_cycle(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int value=0;

	struct epl_priv *epld = epl_obj;

	LOG_FUN();
	sscanf(buf, "%d",&value);

	switch (epl_sensor.mode)
	{
		case EPL_MODE_PS:
			epl_sensor.ps.cycle = (value & 0x7);
			epl_I2C_Write(epld->client,0x04, epl_sensor.ps.adc | epl_sensor.ps.cycle);
			LOG_INFO("[%s]:0x04 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.ps.adc | epl_sensor.ps.cycle, gRawData.raw_bytes[0]);
	    break;

		case EPL_MODE_ALS: //als
			epl_sensor.als.cycle = (value & 0x7);
			epl_I2C_Write(epld->client,0x02, epl_sensor.als.adc | epl_sensor.als.cycle);
			LOG_INFO("[%s]:0x02 = 0x%x (0x%x)\n", __FUNCTION__, epl_sensor.als.adc | epl_sensor.als.cycle, gRawData.raw_bytes[0]);
		break;
	}
	epl_update_mode(epld->client);

	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_als_report_type(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int value=0;

    LOG_FUN();
    sscanf(buf, "%d", &value);
    epl_sensor.als.report_type = value & 0xf;

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ps_polling_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *epld = epl_obj;
    int polling;

    sscanf(buf, "%d",&polling);
    LOG_INFO("ps polling=%d \r\n", polling);

    epl_sensor.ps.polling_mode = polling;

    set_als_ps_intr_type(epld->client, epl_sensor.ps.polling_mode, epl_sensor.als.polling_mode);
    epl_I2C_Write(epld->client, 0x06, (epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type));
    epl_I2C_Write(epld->client, 0x07, (epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type));

    epl_update_mode(epld->client);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_als_polling_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *epld = epl_obj;
    int polling;

    sscanf(buf, "%d",&polling);
    LOG_INFO("als polling=%d \r\n", polling);
    epl_sensor.als.polling_mode = polling;

    //update als intr table
    if(epl_sensor.als.polling_mode == 0)
        epl_als_intr_update_table(epld);

    set_als_ps_intr_type(epld->client, epl_sensor.ps.polling_mode, epl_sensor.als.polling_mode);
    epl_I2C_Write(epld->client, 0x06, (epl_sensor.interrupt_control | epl_sensor.ps.persist |epl_sensor.ps.interrupt_type));
    epl_I2C_Write(epld->client, 0x07, (epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type));

    epl_fast_update(epld->client);
    epl_update_mode(epld->client);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ps_w_calfile(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct epl_priv *epld = epl_obj;
    int ps_hthr=0, ps_lthr=0, ps_cancelation=0;
    int ps_cal_len = 0;
    char ps_calibration[20];

	LOG_FUN();
	if(!epl_obj)
    {
        LOG_ERR("epl_obj is null!!\n");
        return 0;
    }
    sscanf(buf, "%d,%d,%d",&ps_cancelation, &ps_hthr, &ps_lthr);

    ps_cal_len = sprintf(ps_calibration, "%d,%d,%d",  ps_cancelation, ps_hthr, ps_lthr);

    write_factory_calibration(epld, ps_calibration, ps_cal_len);

	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_unlock(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct epl_priv *epld = epl_obj;
    int mode;

    sscanf(buf, "%d",&mode);
    LOG_INFO("mode = %d \r\n", mode);

	switch (mode)
	{
		case 0: //PS unlock and run
        	epl_sensor.ps.compare_reset = EPL_CMP_RUN;
        	epl_sensor.ps.lock = EPL_UN_LOCK;
        	epl_I2C_Write(epld->client,0x1b, epl_sensor.ps.compare_reset |epl_sensor.ps.lock);
		break;

		case 1: //PS unlock and reset
        	epl_sensor.ps.compare_reset = EPL_CMP_RESET;
        	epl_sensor.ps.lock = EPL_UN_LOCK;
        	epl_I2C_Write(epld->client,0x1b, epl_sensor.ps.compare_reset |epl_sensor.ps.lock);
		break;

		case 2: //ALS unlock and run
        	epl_sensor.als.compare_reset = EPL_CMP_RUN;
        	epl_sensor.als.lock = EPL_UN_LOCK;
        	epl_I2C_Write(epld->client,0x12, epl_sensor.als.compare_reset | epl_sensor.als.lock);
		break;

        case 3: //ALS unlock and reset
    		epl_sensor.als.compare_reset = EPL_CMP_RESET;
        	epl_sensor.als.lock = EPL_UN_LOCK;
        	epl_I2C_Write(epld->client,0x12, epl_sensor.als.compare_reset | epl_sensor.als.lock);
        break;

		case 4: //ps+als
		    //PS unlock and run
        	epl_sensor.ps.compare_reset = EPL_CMP_RUN;
        	epl_sensor.ps.lock = EPL_UN_LOCK;
        	epl_I2C_Write(epld->client,0x1b, epl_sensor.ps.compare_reset |epl_sensor.ps.lock);

			//ALS unlock and run
        	epl_sensor.als.compare_reset = EPL_CMP_RUN;
        	epl_sensor.als.lock = EPL_UN_LOCK;
        	epl_I2C_Write(epld->client,0x12, epl_sensor.als.compare_reset | epl_sensor.als.lock);
		break;
	}
    /*double check PS or ALS lock*/
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_als_ch_sel(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct epl_priv *epld = epl_obj;
    int ch_sel;

    sscanf(buf, "%d",&ch_sel);
    LOG_INFO("channel selection = %d \r\n", ch_sel);

	switch (ch_sel)
	{
		case 0: //ch0
		    epl_sensor.als.interrupt_channel_select = EPL_ALS_INT_CHSEL_0;
		break;

		case 1: //ch1
        	epl_sensor.als.interrupt_channel_select = EPL_ALS_INT_CHSEL_1;
		break;

		case 2: //ch2
		    epl_sensor.als.interrupt_channel_select = EPL_ALS_INT_CHSEL_2;
		break;

		case 3: //ch3
        	epl_sensor.als.interrupt_channel_select = EPL_ALS_INT_CHSEL_3;
		break;
	}

    epl_I2C_Write(epld->client,0x07, epl_sensor.als.interrupt_channel_select | epl_sensor.als.persist | epl_sensor.als.interrupt_type);
    epl_update_mode(epld->client);

	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_als_lux_per_count(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d",&epl_sensor.als.factory.lux_per_count);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_ps_cal_enable(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int ps_cal_enable = 0;
    LOG_FUN();
    sscanf(buf, "%d",&ps_cal_enable);
    epl_sensor.ps.factory.calibration_enable = ps_cal_enable;
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_i2c_max_count(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d",&epl_i2c_max_count);
    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_ps_polling(struct device *dev, struct device_attribute *attr, char *buf)
{
    u16 *tmp = (u16*)buf;
    tmp[0]= epl_sensor.ps.polling_mode;
    return 2;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_als_polling(struct device *dev, struct device_attribute *attr, char *buf)
{
    u16 *tmp = (u16*)buf;
    tmp[0]= epl_sensor.als.polling_mode;
    return 2;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_ps_run_cali(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct epl_priv *epld = epl_obj;
	ssize_t len = 0;
    int ret = 0;

    LOG_FUN();
    ret = epl_run_ps_calibration(epld);

    len += snprintf(buf+len, PAGE_SIZE-len, "ret = %d\r\n", ret);
	return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_pdata(struct device *dev, struct device_attribute *attr, char *buf)
{
    ssize_t len = 0;
    int ps_raw = 0;

    ps_raw = epl_factory_ps_data();
    LOG_INFO("[%s]: ps_raw=%d \r\n", __func__, ps_raw);

    len += snprintf(buf + len, PAGE_SIZE - len, "%d", ps_raw);
    return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_als_data(struct device *dev, struct device_attribute *attr, char *buf)
{
    ssize_t len = 0;
    u16 als_raw = 0;

    als_raw = epl_factory_als_data();
    LOG_INFO("[%s]: als_raw=%d \r\n", __func__, als_raw);

    len += snprintf(buf + len, PAGE_SIZE - len, "%d", als_raw);
    return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_reg_write(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *epld = epl_obj;
    int reg;
    int data;

    sscanf(buf, "%x,%x", &reg, &data);

    LOG_INFO("[%s]: reg=0x%x, data=0x%x", __func__, reg, data);
    epl_I2C_Write(epld->client, reg, data);
    return count;
}
/*----------------------------------------------------------------------------*/
#if PS_DYN_K
static ssize_t epl_store_dyn_offset(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int dyn_h,dyn_l;

    sscanf(buf, "%d,%d",&dyn_l, &dyn_h);
    LOG_INFO("[%s]: dynk_offset(%d/%d) \r\n", __func__, dyn_l, dyn_h);
    epl_dynk_low_offset = dyn_l;
    epl_dynk_high_offset = dyn_h;

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_dyn_max_ir_data(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int max_ir_data;

    sscanf(buf, "%d",&max_ir_data);
    epl_dynk_max_ir_data = max_ir_data;

    return count;
}
#endif
/*----------------------------------------------------------------------------*/
#if ALS_DYN_INTT
static ssize_t epl_store_c_gain(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    int als_c_gain;

    sscanf(buf, "%d",&als_c_gain);
    epl_c_gain = als_c_gain;
    LOG_INFO("epl_c_gain = %d \r\n", epl_c_gain);

	return count;
}
#endif
/*----------------------------------------------------------------------------*/
#if HS_ENABLE
static ssize_t epl_show_renvo(struct device *dev, struct device_attribute *attr, char *buf)
{
    ssize_t len = 0;

    LOG_INFO("gRawData.renvo=0x%x \r\n", epl_sensor.revno);
    len += snprintf(buf+len, PAGE_SIZE-len, "%x", epl_sensor.revno);

    return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_hs_enable(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    uint16_t mode=0;
    struct epl_priv *obj = epl_obj;
    bool enable_ps = obj->enable_pflag==1 && obj->ps_suspend==0;
	bool enable_als = obj->enable_lflag==1 && obj->als_suspend==0;
    LOG_FUN();

    sscanf(buf, "%hu",&mode);

    if(enable_ps == 0)
    {
        if(mode > 0)
    	{
    	    if(enable_als == 1)
    	    {
                obj->enable_lflag = 0;
                hs_enable_flag = true;
    	    }
    		epl_sensor.hs.integration_time = epl_sensor.hs.integration_time_max;
    		epl_sensor.hs.raws_count=0;
            obj->enable_hflag = 1;

            if(mode == 2)
            {
                epl_sensor.hs.dynamic_intt = false;
            }
            else
            {
                epl_sensor.hs.dynamic_intt = true;
            }
    	}
        else
    	{
            obj->enable_hflag = 0;
            if(hs_enable_flag == true)
            {
                obj->enable_lflag = 1;
                hs_enable_flag = false;
            }
    	}
        write_global_variable(obj->client);
        epl_update_mode(obj->client);
    }

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_store_hs_int_time(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct epl_priv *obj = epl_obj;
	int value;
	u8 intt_buf;

    sscanf(buf, "%d",&value);

	mutex_lock(&hs_sensor_mutex);
	epl_sensor.hs.integration_time = value<<2;
	intt_buf = epl_sensor.hs.integration_time | epl_sensor.hs.gain;
	epl_I2C_Write(obj->client, 0x03, intt_buf);
	mutex_unlock(&hs_sensor_mutex);

    return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_show_hs_raws(struct device *dev, struct device_attribute *attr, char *buf)
{
    u16 *tmp = (u16*)buf;
    int byte_count=2+epl_sensor.hs.raws_count*2;
    int i=0;
    LOG_FUN();
    mutex_lock(&hs_sensor_mutex);
    tmp[0]= epl_sensor.hs.raws_count;

    for(i=0; i<epl_sensor.hs.raws_count; i++){
        tmp[i+1] = epl_sensor.hs.raws[i];
    }
    epl_sensor.hs.raws_count=0;
    mutex_unlock(&hs_sensor_mutex);

    return byte_count;
}
#endif
/*----------------------------------------------------------------------------*/
/*CTS --> S_IWUSR | S_IRUGO*/
static DEVICE_ATTR(elan_status,					S_IROTH  | S_IWOTH, epl_show_status,  	  		NULL										);
static DEVICE_ATTR(elan_reg,    				S_IROTH  | S_IWOTH, epl_show_reg,   				NULL										);
static DEVICE_ATTR(als_enable,					S_IROTH  | S_IWOTH, NULL,   							epl_store_als_enable					);
static DEVICE_ATTR(als_report_type,				S_IROTH  | S_IWOTH, NULL,								epl_store_als_report_type				);
static DEVICE_ATTR(als_polling_mode,			S_IROTH  | S_IWOTH, epl_show_als_polling,   		epl_store_als_polling_mode				);
static DEVICE_ATTR(gain,						S_IROTH  | S_IWOTH, NULL,   							epl_store_gain						);
static DEVICE_ATTR(als_lux_per_count,			S_IROTH  | S_IWOTH, NULL,   					 		epl_store_als_lux_per_count				);
static DEVICE_ATTR(ps_cal_enable,			    S_IROTH  | S_IWOTH, NULL,   					 		epl_store_ps_cal_enable				);
static DEVICE_ATTR(ps_enable,					S_IROTH  | S_IWOTH, NULL,   							epl_store_ps_enable					);
static DEVICE_ATTR(ps_offset,					S_IROTH  | S_IWOTH, NULL,								epl_store_ps_offset						);
static DEVICE_ATTR(ps_polling_mode,			    S_IROTH  | S_IWOTH, epl_show_ps_polling,   		epl_store_ps_polling_mode				);
static DEVICE_ATTR(ges_raws, 					S_IROTH  | S_IWOTH, epl_show_ges_raws, 	  		NULL										);
static DEVICE_ATTR(ges_enable,					S_IROTH  | S_IWOTH, NULL,   					 		epl_store_ges_enable					);
static DEVICE_ATTR(ges_mode,				    S_IROTH  | S_IWOTH, NULL,								epl_store_ges_mode					);
static DEVICE_ATTR(ges_int_time,				S_IROTH  | S_IWOTH, NULL,   					 		epl_store_ges_int_time					);
static DEVICE_ATTR(ges_polling_mode,			S_IROTH  | S_IWOTH, NULL,   					 		epl_store_ges_polling_mode				);
static DEVICE_ATTR(ges_opt,					    S_IROTH  | S_IWOTH, NULL,   					 		epl_store_ges_opt						);
static DEVICE_ATTR(ges_int_filter_order,		S_IROTH  | S_IWOTH, NULL,   							epl_store_ges_int_filter_order			);
static DEVICE_ATTR(ges_debug,					S_IROTH  | S_IWOTH, NULL,   					 		epl_store_ges_debug					);
static DEVICE_ATTR(ges_work_threshold,			S_IROTH  | S_IWOTH, NULL,   							epl_store_ges_work_threshold			);
static DEVICE_ATTR(ges_zoom_delta,				S_IROTH  | S_IWOTH, NULL,   							epl_store_ges_zoom_delta				);
static DEVICE_ATTR(ges_index,					S_IROTH  | S_IWOTH, NULL,								epl_store_ges_index					);
static DEVICE_ATTR(ir_mode,					    S_IROTH  | S_IWOTH, NULL,   							epl_store_ir_mode						);
static DEVICE_ATTR(ir_boost,					S_IROTH  | S_IWOTH, NULL,   							epl_store_ir_boost						);
static DEVICE_ATTR(ir_drive,					S_IROTH  | S_IWOTH, NULL,   							epl_store_ir_drive						);
static DEVICE_ATTR(ir_on,						S_IROTH  | S_IWOTH, NULL,								epl_store_ir_contrl						);
static DEVICE_ATTR(interrupt_type,				S_IROTH  | S_IWOTH, NULL,								epl_store_interrupt_type				);
static DEVICE_ATTR(osc,						    S_IROTH  | S_IWOTH, NULL,								epl_store_osc							);
static DEVICE_ATTR(integration,					S_IROTH  | S_IWOTH, NULL,								epl_store_integration					);
static DEVICE_ATTR(adc,						    S_IROTH  | S_IWOTH, NULL,								epl_store_adc							);
static DEVICE_ATTR(cycle,						S_IROTH  | S_IWOTH, NULL,								epl_store_cycle						);
static DEVICE_ATTR(ps_w_calfile,				S_IROTH  | S_IWOTH, NULL,			                    epl_store_ps_w_calfile				);
static DEVICE_ATTR(i2c_max_count,			    S_IROTH  | S_IWOTH, NULL,   					 		epl_store_i2c_max_count				);
static DEVICE_ATTR(mode,						S_IROTH  | S_IWOTH, NULL,   							epl_store_mode							);
static DEVICE_ATTR(wait_time,					S_IROTH  | S_IWOTH, NULL,   					 		epl_store_wait_time						);
static DEVICE_ATTR(set_threshold,     			S_IROTH  | S_IWOTH, epl_show_threshold,                epl_store_threshold						);
static DEVICE_ATTR(cal_raw, 					S_IROTH  | S_IWOTH, epl_show_cal_raw, 	  		NULL										);
static DEVICE_ATTR(unlock,				        S_IROTH  | S_IWOTH, NULL,			                    epl_store_unlock						);
static DEVICE_ATTR(als_ch,				        S_IROTH  | S_IWOTH, NULL,			                    epl_store_als_ch_sel					);
static DEVICE_ATTR(run_ps_cali, 				S_IROTH  | S_IWOTH, epl_show_ps_run_cali, 	  		NULL										);
static DEVICE_ATTR(pdata,                       S_IROTH  | S_IWOTH, epl_show_pdata, NULL);
static DEVICE_ATTR(als_data,                    S_IROTH  | S_IWOTH, epl_show_als_data, NULL);
static DEVICE_ATTR(i2c_w,                       S_IROTH  | S_IWOTH, NULL,                               epl_store_reg_write                  );
#if PS_DYN_K
static DEVICE_ATTR(dyn_offset,                  S_IROTH  | S_IWOTH, NULL,                               epl_store_dyn_offset                 );
static DEVICE_ATTR(dyn_max_ir_data,             S_IROTH  | S_IWOTH, NULL,                               epl_store_dyn_max_ir_data            );
#endif
#if ALS_DYN_INTT
static DEVICE_ATTR(als_dyn_c_gain,              S_IROTH  | S_IWOTH, NULL,                               epl_store_c_gain);
#endif
#if HS_ENABLE
static DEVICE_ATTR(elan_renvo,                  S_IROTH  | S_IWOTH, epl_show_renvo,              NULL                                        );
static DEVICE_ATTR(hs_enable,					S_IROTH  | S_IWOTH, NULL,                               epl_store_hs_enable                  );
static DEVICE_ATTR(hs_int_time,					S_IROTH  | S_IWOTH, NULL,                               epl_store_hs_int_time                );
static DEVICE_ATTR(hs_raws,					    S_IROTH  | S_IWOTH, epl_show_hs_raws,            NULL                                        );
#endif
/*----------------------------------------------------------------------------*/
static struct attribute *epl_attr_list[] =
{
    &dev_attr_elan_status.attr,
    &dev_attr_elan_reg.attr,
    &dev_attr_als_enable.attr,
    &dev_attr_als_report_type.attr,
    &dev_attr_als_polling_mode.attr,
    &dev_attr_gain.attr,
    &dev_attr_als_lux_per_count.attr,
    &dev_attr_ps_cal_enable.attr,
    &dev_attr_ps_enable.attr,
    &dev_attr_ps_offset.attr,
    &dev_attr_ps_polling_mode.attr,
    &dev_attr_ges_raws.attr,
    &dev_attr_ges_enable.attr,
    &dev_attr_ges_mode.attr,
    &dev_attr_ges_int_time.attr,
    &dev_attr_ges_polling_mode.attr,
    &dev_attr_ges_opt.attr,
    &dev_attr_ges_int_filter_order.attr,
    &dev_attr_ges_debug.attr,
    &dev_attr_ges_work_threshold.attr,
    &dev_attr_ges_zoom_delta.attr,
    &dev_attr_ges_index.attr,
    &dev_attr_ir_mode.attr,
    &dev_attr_ir_boost.attr,
    &dev_attr_ir_drive.attr,
    &dev_attr_ir_on.attr,
    &dev_attr_interrupt_type.attr,
    &dev_attr_osc.attr,
    &dev_attr_integration.attr,
    &dev_attr_adc.attr,
    &dev_attr_cycle.attr,
    &dev_attr_ps_w_calfile.attr,
    &dev_attr_i2c_max_count.attr,
    &dev_attr_mode.attr,
    &dev_attr_wait_time.attr,
    &dev_attr_set_threshold.attr,
    &dev_attr_cal_raw.attr,
    &dev_attr_unlock.attr,
    &dev_attr_als_ch.attr,
    &dev_attr_run_ps_cali.attr,
    &dev_attr_pdata.attr,
    &dev_attr_als_data.attr,
    &dev_attr_i2c_w.attr,
#if PS_DYN_K
    &dev_attr_dyn_offset.attr,
    &dev_attr_dyn_max_ir_data.attr,
#endif
#if ALS_DYN_INTT
    &dev_attr_als_dyn_c_gain.attr,
#endif
#if HS_ENABLE
    &dev_attr_elan_renvo.attr,
    &dev_attr_hs_enable.attr,
    &dev_attr_hs_int_time.attr,
    &dev_attr_hs_raws.attr,
#endif
    NULL,
};
/*----------------------------------------------------------------------------*/
static struct attribute_group epl_attr_group =
{
    .attrs = epl_attr_list,
};
/*----------------------------------------------------------------------------*/

#if !(SPREAD)/*SPREAD start.....*/
/*----------------------------------------------------------------------------*/
static int epl_als_open(struct inode *inode, struct file *file)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    if (epld->als_opened)
    {
        return -EBUSY;
    }
    epld->als_opened = 1;

    return 0;
}
/*----------------------------------------------------------------------------*/
static int epl_als_read(struct file *file, char __user *buffer, size_t count, loff_t *ppos)
{
    struct epl_priv *epld = epl_obj;
    int buf[1];

    if(epld->read_flag ==1)
    {
        buf[0] = epl_sensor.als.data.channels[1];
        if(copy_to_user(buffer, &buf , sizeof(buf)))
            return 0;
        epld->read_flag = 0;
        return 12;
    }
    else
    {
        return 0;
    }
}
/*----------------------------------------------------------------------------*/
static int epl_als_release(struct inode *inode, struct file *file)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    epld->als_opened = 0;

    return 0;
}
/*----------------------------------------------------------------------------*/
static long epl_als_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int flag;
    unsigned long buf[1];
    struct epl_priv *epld = epl_obj;
    void __user *argp = (void __user *)arg;

    LOG_INFO("als io ctrl cmd %d\n", _IOC_NR(cmd));
    switch(cmd)
    {
        case ELAN_EPL8800_IOCTL_GET_LFLAG:

            LOG_INFO("elan ambient-light IOCTL Sensor get lflag \n");
            flag = epld->enable_lflag;
            if (copy_to_user(argp, &flag, sizeof(flag)))
                return -EFAULT;

            LOG_INFO("elan ambient-light Sensor get lflag %d\n",flag);
        break;

        case ELAN_EPL8800_IOCTL_ENABLE_LFLAG:
#if LEADCORE
        case LIGHT_SET_ENALBE:
#endif
            LOG_INFO("elan ambient-light IOCTL Sensor set lflag \n");
            if (copy_from_user(&flag, argp, sizeof(flag)))
                return -EFAULT;
            if (flag < 0 || flag > 1)
                return -EINVAL;

            epl_enable_als(flag);

            LOG_INFO("elan ambient-light Sensor set lflag %d\n",flag);
        break;

        case ELAN_EPL8800_IOCTL_GETDATA:

            buf[0] = epl_factory_als_data();
            if(copy_to_user(argp, &buf , sizeof(buf)))
                return -EFAULT;
        break;
#if LEADCORE
        case LIGHT_SET_DELAY:

			if (arg > LIGHT_MAX_DELAY)
				arg = LIGHT_MAX_DELAY;
			else if (arg < LIGHT_MIN_DELAY)
				arg = LIGHT_MIN_DELAY;
			LOG_INFO("LIGHT_SET_DELAY--%d\r\n",(int)arg);
			polling_time = arg;
		break;
#endif
        default:
            LOG_ERR("invalid cmd %d\n", _IOC_NR(cmd));
            return -EINVAL;
    }

    return 0;
}
/*----------------------------------------------------------------------------*/
static struct file_operations epl_als_fops =
{
    .owner = THIS_MODULE,
    .open = epl_als_open,
    .read = epl_als_read,
    .release = epl_als_release,
    .unlocked_ioctl = epl_als_ioctl
};
/*----------------------------------------------------------------------------*/
static struct miscdevice epl_als_device =
{
    .minor = MISC_DYNAMIC_MINOR,
#if LEADCORE
    .name = "light",
#else
    .name = "elan_als",
#endif
    .fops = &epl_als_fops
};
/*----------------------------------------------------------------------------*/
#endif /*SPREAD end.........*/
/*----------------------------------------------------------------------------*/
static int epl_ps_open(struct inode *inode, struct file *file)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    if (epld->ps_opened)
        return -EBUSY;

    epld->ps_opened = 1;

    return 0;
}
/*----------------------------------------------------------------------------*/
static int epl_ps_release(struct inode *inode, struct file *file)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    epld->ps_opened = 0;

    return 0;
}
/*----------------------------------------------------------------------------*/
static long epl_ps_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int value;
    int flag;
    struct epl_priv *epld = epl_obj;
    void __user *argp = (void __user *)arg;

    LOG_INFO("ps io ctrl cmd %d\n", _IOC_NR(cmd));
    //ioctl message handle must define by android sensor library (case by case)
    switch(cmd)
    {

        case ELAN_EPL8800_IOCTL_GET_PFLAG:
            LOG_INFO("elan Proximity Sensor IOCTL get pflag \n");
            flag = epld->enable_pflag;
            if (copy_to_user(argp, &flag, sizeof(flag)))
                return -EFAULT;

            LOG_INFO("elan Proximity Sensor get pflag %d\n",flag);
        break;

        case ELAN_EPL8800_IOCTL_ENABLE_PFLAG:
#if LEADCORE
        case PROXIMITY_SET_ENALBE:
#endif
            LOG_INFO("elan Proximity IOCTL Sensor set pflag \n");
            if (copy_from_user(&flag, argp, sizeof(flag)))
                return -EFAULT;
            if (flag < 0 || flag > 1)
                return -EINVAL;

            epl_enable_ps(flag);

            LOG_INFO("elan Proximity Sensor set pflag %d\n",flag);
        break;

        case ELAN_EPL8800_IOCTL_GETDATA:

            value = epl_factory_ps_data();

            LOG_INFO("elan proximity Sensor get data (%d) \n",value);

            if(copy_to_user(argp, &value , sizeof(value)))
                return -EFAULT;
       break;
#if LEADCORE
		case PROXIMITY_SET_DELAY:
			if (arg > PROXIMITY_MAX_DELAY)
				arg = PROXIMITY_MAX_DELAY;
			else if (arg < PROXIMITY_MIN_DELAY)
				arg = PROXIMITY_MIN_DELAY;
			LOG_INFO("PROXIMITY_SET_DELAY--%d\r\n",(int)arg);
			polling_time = arg;
	    break;
#endif
#if SPREAD  /*SPREAD start.....*/
        case ELAN_EPL8800_IOCTL_GET_LFLAG:
            LOG_INFO("elan ambient-light IOCTL Sensor get lflag \n");
            flag = epld->enable_lflag;
            if (copy_to_user(argp, &flag, sizeof(flag)))
                return -EFAULT;

            LOG_INFO("elan ambient-light Sensor get lflag %d\n",flag);
        break;

        case ELAN_EPL8800_IOCTL_ENABLE_LFLAG:
            LOG_INFO("elan ambient-light IOCTL Sensor set lflag \n");
            if (copy_from_user(&flag, argp, sizeof(flag)))
                return -EFAULT;
            if (flag < 0 || flag > 1)
                return -EINVAL;

            epl_enable_als(flag);

            LOG_INFO("elan ambient-light Sensor set lflag %d\n",flag);
        break;
#if 0
        case ELAN_EPL8800_IOCTL_GETDATA:
            buf[0] = epl_factory_als_data();
            if(copy_to_user(argp, &buf , sizeof(buf)))
                return -EFAULT;

        break;
#endif
#endif  /*SPREAD end......*/
        default:
            LOG_ERR("invalid cmd %d\n", _IOC_NR(cmd));
            return -EINVAL;
    }

    return 0;

}
/*----------------------------------------------------------------------------*/
static struct file_operations epl_ps_fops =
{
    .owner = THIS_MODULE,
    .open = epl_ps_open,
    .release = epl_ps_release,
    .unlocked_ioctl = epl_ps_ioctl
};
/*----------------------------------------------------------------------------*/
static struct miscdevice epl_ps_device =
{
    .minor = MISC_DYNAMIC_MINOR,
#if LEADCORE
    .name = "proximity",
#else
    .name = "elan_ps",
#endif
    .fops = &epl_ps_fops
};

#if !(SPREAD) /*SPREAD start.....*/
/*----------------------------------------------------------------------------*/
static ssize_t light_enable_show(struct device *dev,
					 struct device_attribute *attr, char *buf)
{
	struct epl_priv *epld  = epl_obj;
	LOG_INFO("%s: ALS_status=%d\n", __func__, epld->enable_lflag);
	return sprintf(buf, "%d\n", epld->enable_lflag);
}
/*----------------------------------------------------------------------------*/
static ssize_t light_enable_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t size)
{
    uint16_t als_enable = 0;

    LOG_INFO("light_enable_store: enable=%s \n", buf);
	sscanf(buf, "%hu", &als_enable);
    epl_enable_als(als_enable);

	return size;
}
/*----------------------------------------------------------------------------*/
#if MARVELL
static struct device_attribute dev_attr_light_enable =
__ATTR(enable1, 0660,
	   light_enable_show, light_enable_store);
#else
static struct device_attribute dev_attr_light_enable =
__ATTR(enable, 0660,
	   light_enable_show, light_enable_store);
#endif
static struct attribute *light_sysfs_attrs[] = {
	&dev_attr_light_enable.attr,
	NULL
};
/*----------------------------------------------------------------------------*/
static struct attribute_group light_attribute_group = {
	.attrs = light_sysfs_attrs,
};
/*----------------------------------------------------------------------------*/
static int epl_setup_lsensor(struct epl_priv *epld)
{
    int err = 0;
    LOG_INFO("epl_setup_lsensor enter.\n");

    epld->als_input_dev = input_allocate_device();
    if (!epld->als_input_dev)
    {
        LOG_ERR( "could not allocate ls input device\n");
        return -ENOMEM;
    }
    epld->als_input_dev->name = ElanALsensorName;
    //set_bit(EV_ABS, epld->als_input_dev->evbit);
    //input_set_abs_params(epld->als_input_dev, ABS_MISC, 0, 9, 0, 0);
    input_set_capability(epld->als_input_dev, EV_ABS, ABS_MISC);
	input_set_abs_params(epld->als_input_dev, ABS_MISC, 0, 65535, 0, 0);

    err = input_register_device(epld->als_input_dev);
    if (err < 0)
    {
        LOG_ERR("can not register ls input device\n");
        goto err_free_ls_input_device;
    }

    err = misc_register(&epl_als_device);
    if (err < 0)
    {
        LOG_ERR("can not register ls misc device\n");
        goto err_unregister_ls_input_device;
    }

    err = sysfs_create_group(&epld->als_input_dev->dev.kobj, &light_attribute_group);

	if (err) {
		pr_err("%s: could not create sysfs group\n", __func__);
		goto err_free_ls_input_device;
	}
    return err;

err_unregister_ls_input_device:
    input_unregister_device(epld->als_input_dev);
err_free_ls_input_device:
    input_free_device(epld->als_input_dev);
    return err;
}
#endif /*SPREAD end.....*/
/*----------------------------------------------------------------------------*/
static ssize_t proximity_enable_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct epl_priv *epld  = epl_obj;

	LOG_INFO("%s: PS status=%d\n", __func__, epld->enable_pflag);

	return sprintf(buf, "%d\n", epld->enable_pflag);
}
/*----------------------------------------------------------------------------*/
static ssize_t proximity_enable_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size)
{
    uint16_t ps_enable = 0;

    LOG_INFO("proximity_enable_store: enable=%s \n", buf);
	sscanf(buf, "%hu",&ps_enable);
    epl_enable_ps(ps_enable);

	return size;
}
/*----------------------------------------------------------------------------*/
#if MARVELL
static struct device_attribute dev_attr_psensor_enable =
__ATTR(enalbe1, S_IRWXUGO,
	   proximity_enable_show, proximity_enable_store);
#else
static struct device_attribute dev_attr_psensor_enable =
__ATTR(enable, S_IRWXUGO,
	   proximity_enable_show, proximity_enable_store);
#endif
static struct attribute *proximity_sysfs_attrs[] = {
	&dev_attr_psensor_enable.attr,
	NULL
};
/*----------------------------------------------------------------------------*/
static struct attribute_group proximity_attribute_group = {
	.attrs = proximity_sysfs_attrs,
};
/*----------------------------------------------------------------------------*/
static int epl_setup_psensor(struct epl_priv *epld)
{
    int err = 0;

    LOG_INFO("epl_setup_psensor enter.\n");
    epld->ps_input_dev = input_allocate_device();
    if (!epld->ps_input_dev)
    {
        LOG_ERR("could not allocate ps input device\n");
        return -ENOMEM;
    }
    epld->ps_input_dev->name = ElanPsensorName;

    set_bit(EV_ABS, epld->ps_input_dev->evbit);
    input_set_abs_params(epld->ps_input_dev, ABS_DISTANCE, 0, 1, 0, 0);
#if SPREAD
    set_bit(EV_ABS, epld->ps_input_dev->evbit);
    input_set_abs_params(epld->ps_input_dev, ABS_MISC, 0, 9, 0, 0);
#endif

    err = input_register_device(epld->ps_input_dev);
    if (err < 0)
    {
        LOG_ERR("could not register ps input device\n");
        goto err_free_ps_input_device;
    }

    err = misc_register(&epl_ps_device);
    if (err < 0)
    {
        LOG_ERR("could not register ps misc device\n");
        goto err_unregister_ps_input_device;
    }

    err = sysfs_create_group(&epld->ps_input_dev->dev.kobj, &proximity_attribute_group);
	if (err) {
		pr_err("%s: PS could not create sysfs group\n", __func__);
		goto err_free_ps_input_device;
	}

    return err;

err_unregister_ps_input_device:
    input_unregister_device(epld->ps_input_dev);
err_free_ps_input_device:
    input_free_device(epld->ps_input_dev);
    return err;
}
/*----------------------------------------------------------------------------*/
#ifdef CONFIG_SUSPEND
static int epl_suspend(struct i2c_client *client, pm_message_t mesg)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    if(!epld)
    {
        LOG_ERR("null pointer!!\n");
        return -EINVAL;
    }

#if !defined(CONFIG_HAS_EARLYSUSPEND)

    epld->als_suspend = 1;
    epld->ges_suspend = 1;
#if HS_ENABLE
    epld->hs_suspend=1;
#endif
    LOG_INFO("[%s]: enable_pflag=%d, enable_lflag=%d \r\n", __func__, epld->enable_pflag, epld->enable_lflag);

    if(epld->enable_pflag == 0)
    {
        if(epld->enable_lflag == 1 && epl_sensor.als.polling_mode == 0)
        {
            LOG_INFO("[%s]: check ALS interrupt_flag............ \r\n", __func__);
            epl_read_als(epld->client);
            if(epl_sensor.als.interrupt_flag == EPL_INT_TRIGGER)
            {
                LOG_INFO("[%s]: epl_sensor.als.interrupt_flag = %d \r\n", __func__, epl_sensor.als.interrupt_flag);
                //ALS unlock interrupt pin and restart chip
            	epl_I2C_Write(epld->client, 0x12, EPL_CMP_RESET | EPL_UN_LOCK);
            }
        }
        //atomic_set(&obj->ps_suspend, 1);
        LOG_INFO("[%s]: ps disable \r\n", __func__);
        epl_update_mode(epld->client);
    }
#endif /*CONFIG_HAS_EARLYSUSPEND*/
    return 0;
}
/*----------------------------------------------------------------------------*/
#if defined(CONFIG_HAS_EARLYSUSPEND)
static void epl_early_suspend(struct early_suspend *h)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    if(!epld)
    {
        LOG_ERR("null pointer!!\n");
        return;
    }

    epld->als_suspend=1;
    epld->ges_suspend = 1;
#if HS_ENABLE
    epld->hs_suspend=1;
#endif
    LOG_INFO("[%s]: enable_pflag=%d, enable_lflag=%d \r\n", __func__, epld->enable_pflag, epld->enable_lflag);

    if(epld->enable_pflag == 0)
    {
        if(epld->enable_lflag == 1 && epl_sensor.als.polling_mode == 0)
        {
            LOG_INFO("[%s]: check ALS interrupt_flag............ \r\n", __func__);
            epl_read_als(epld->client);
            if(epl_sensor.als.interrupt_flag == EPL_INT_TRIGGER)
            {
                LOG_INFO("[%s]: epl_sensor.als.interrupt_flag = %d \r\n", __func__, epl_sensor.als.interrupt_flag);
                //ALS unlock interrupt pin and restart chip
            	epl_I2C_Write(epld->client, 0x12, EPL_CMP_RESET | EPL_UN_LOCK);
            }
        }
        //atomic_set(&obj->ps_suspend, 1);
        LOG_INFO("[%s]: ps disable \r\n", __func__);
        epl_update_mode(epld->client);
    }
}
#endif
/*----------------------------------------------------------------------------*/
static int epl_resume(struct i2c_client *client)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();

    if(!epld)
    {
        LOG_ERR("null pointer!!\n");
        return -EINVAL;
    }

#if !defined(CONFIG_HAS_EARLYSUSPEND)

    epld->als_suspend=0;
    epld->ps_suspend=0;
    epld->ges_suspend = 0;
#if HS_ENABLE
    epld->hs_suspend=0;
#endif

    LOG_INFO("[%s]: enable_pflag=%d, enable_lflag=%d \r\n", __func__, epld->enable_pflag, epld->enable_lflag);

    if(epld->enable_pflag == 0)
    {
        LOG_INFO("[%s]: ps is disabled \r\n", __func__);
        epl_fast_update(epld->client);
        epl_update_mode(epld->client);
    }
    else if(epld->enable_lflag == 1 && epl_sensor.als.polling_mode==1)
    {
        LOG_INFO("[%s]: restart polling_work \r\n", __func__);
        epl_restart_polling();
    }
#endif
    return 0;
}
/*----------------------------------------------------------------------------*/
#if defined(CONFIG_HAS_EARLYSUSPEND)
static void epl_late_resume(struct early_suspend *h)
{
    struct epl_priv *epld = epl_obj;

    LOG_FUN();
    if(!epld)
    {
        LOG_ERR("null pointer!!\n");
        return;
    }
    epld->als_suspend=0;
    epld->ps_suspend=0;
    epld->ges_suspend = 0;
#if HS_ENABLE
    epld->hs_suspend=0;
#endif
    LOG_INFO("[%s]: enable_pflag=%d, enable_lflag=%d \r\n", __func__, epld->enable_pflag, epld->enable_lflag);

    if(epld->enable_pflag == 0)
    {
        LOG_INFO("[%s]: ps is disabled \r\n", __func__);
        epl_fast_update(epld->client);
        epl_update_mode(epld->client);
    }
    else if(epld->enable_lflag == 1 && epl_sensor.als.polling_mode==1)
    {
        LOG_INFO("[%s]: restart polling_work \r\n", __func__);
        epl_restart_polling();
    }
}
#endif

#endif  /*CONFIG_SUSPEND*/
/*----------------------------------------------------------------------------*/
static void epl_dumpReg(struct i2c_client *client)
{
    LOG_INFO("chip id REG 0x00 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x00));
    LOG_INFO("chip id REG 0x01 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x01));
    LOG_INFO("chip id REG 0x02 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x02));
    LOG_INFO("chip id REG 0x03 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x03));
    LOG_INFO("chip id REG 0x04 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x04));
    LOG_INFO("chip id REG 0x05 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x05));
    LOG_INFO("chip id REG 0x06 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x06));
    LOG_INFO("chip id REG 0x07 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x07));
    LOG_INFO("chip id REG 0x11 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x11));
    LOG_INFO("chip id REG 0x12 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x12));
    LOG_INFO("chip id REG 0x1B value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x1B));
    LOG_INFO("chip id REG 0x20 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x20));
    LOG_INFO("chip id REG 0x21 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x21));
    LOG_INFO("chip id REG 0x24 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x24));
    LOG_INFO("chip id REG 0x25 value = 0x%x\n", i2c_smbus_read_byte_data(client, 0x25));
}
/*----------------------------------------------------------------------------*/
#if SENSOR_CLASS
static int epld_sensor_cdev_enable_als(struct sensors_classdev *sensors_cdev, unsigned int enable)
{
	struct epl_priv *epld = epl_obj;

    if(!epld)
	{
		LOG_ERR("epld is null!!\n");
		return -1;
	}
    LOG_INFO("[%s]: als enable=%d \r\n", __func__, enable);
    epl_enable_als(enable);

	return 0;
}
/*----------------------------------------------------------------------------*/
static int epld_sensor_cdev_enable_ps(struct sensors_classdev *sensors_cdev, unsigned int enable)
{
	struct epl_priv *epld = epl_obj;

    if(!epld)
	{
		LOG_ERR("epld is null!!\n");
		return -1;
	}
    LOG_INFO("[%s]: ps enable=%d \r\n", __func__, enable);
    epl_enable_ps(enable);

	return 0;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_snesor_cdev_set_ps_delay(struct sensors_classdev *sensors_cdev, unsigned int delay_msec)
{
	//struct elan_epl_data *epld = container_of(sensors_cdev, struct elan_epl_data, als_cdev);

	if (delay_msec < PS_MIN_POLLING_RATE)	/*at least 200 ms */
		delay_msec = PS_MIN_POLLING_RATE;
	polling_time = delay_msec;	/* convert us => ms */

	return 0;
}
/*----------------------------------------------------------------------------*/
static ssize_t epl_cdev_set_als_delay(struct sensors_classdev *sensors_cdev, unsigned int delay_msec)
{
	//struct elan_epl_data *epld = container_of(sensors_cdev, struct elan_epl_data, als_cdev);

	if (delay_msec < ALS_MIN_POLLING_RATE)	/*at least 200 ms */
		delay_msec = ALS_MIN_POLLING_RATE;
	polling_time = delay_msec;	/* convert us => ms */

	return 0;
}
#endif
/*----------------------------------------------------------------------------*/
static int epl_als_intr_update_table(struct epl_priv *epld)
{
	int i;
	for (i = 0; i < 10; i++) {
		if ( epl_als_lux_intr_level[i] < 0xFFFF )
		{
#if ALS_DYN_INTT
		    if(epl_sensor.als.report_type == CMC_BIT_DYN_INT)
		    {
                epl_als_adc_intr_level[i] = epl_als_lux_intr_level[i] * 1000 / epl_c_gain;
		    }
		    else
#endif
		    {
                epl_als_adc_intr_level[i] = epl_als_lux_intr_level[i] * 1000 / epl_sensor.als.factory.lux_per_count;
		    }


            if(i != 0 && epl_als_adc_intr_level[i] <= epl_als_adc_intr_level[i-1])
    		{
                epl_als_adc_intr_level[i] = 0xffff;
    		}
		}
		else {
			epl_als_adc_intr_level[i] = epl_als_lux_intr_level[i];
		}
		LOG_INFO(" [%s]:epl_als_adc_intr_level: [%d], %ld \r\n", __func__, i, epl_als_adc_intr_level[i]);
	}

	return 0;
}
/*----------------------------------------------------------------------------*/
static int epl_probe(struct i2c_client *client,const struct i2c_device_id *id)
{
    int err = 0, i = 0;
    struct epl_priv *epld;

    LOG_INFO("elan sensor probe enter.\n");

    epld = kzalloc(sizeof(struct epl_priv), GFP_KERNEL);
    if (!epld)
        return -ENOMEM;

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
    {
        dev_err(&client->dev,"No supported i2c func what we need?!!\n");
        err = -ENOTSUPP;
        goto i2c_fail;
    }

    if((i2c_smbus_read_byte_data(client, 0x21)) != 0x61){
        LOG_INFO("elan ALS/PS sensor is failed. \n");
        err = -1;
        goto i2c_fail;
     }

    epl_dumpReg(client);

    epld->als_level_num = sizeof(epld->als_level)/sizeof(epld->als_level[0]);
    epld->als_value_num = sizeof(epld->als_value)/sizeof(epld->als_value[0]);
    BUG_ON(sizeof(epld->als_level) != sizeof(als_level));
    memcpy(epld->als_level, als_level, sizeof(epld->als_level));
    BUG_ON(sizeof(epld->als_value) != sizeof(als_value));
    memcpy(epld->als_value, als_value, sizeof(epld->als_value));

    epld->client = client;
    epld->irq = client->irq;
#if HS_ENABLE
    epld->hs_suspend = 0;
	mutex_init(&hs_sensor_mutex);
#endif

    epld->ges_input_dev = input_allocate_device();
    epld->ges_input_dev->evbit[0] |= BIT_MASK(EV_REP);
    epld->ges_input_dev->keycodemax = 500;
    epld->ges_input_dev->name ="elan_gesture";

    for(i=0; i<EVENT_UNKNOWN; i++)
        epld->ges_input_dev->keybit[BIT_WORD(KEYCODE_ARRAY[i])] |= BIT_MASK(KEYCODE_ARRAY[i]);

    set_bit(EV_KEY, epld->ges_input_dev->evbit);
    set_bit(EV_REL, epld->ges_input_dev->evbit);
    set_bit(EV_ABS, epld->ges_input_dev->evbit);

    i2c_set_clientdata(client, epld);

    epl_obj = epld;

    INIT_DELAYED_WORK(&epld->eint_work, epl_eint_work);
    INIT_DELAYED_WORK(&epld->polling_work, epl_polling_work);
#if PS_DYN_K
    INIT_DELAYED_WORK(&epld->dynk_thd_polling_work, epl_dynk_thd_polling_work);
#endif
    mutex_init(&sensor_mutex);
    wake_lock_init(&ps_lock, WAKE_LOCK_SUSPEND, "ps wakelock");

    if(input_register_device(epld->ges_input_dev))
        LOG_ERR("register input error\n");

    //initial global variable and write to senosr
    initial_global_variable(client, epld);
#if !(SPREAD)
    err = epl_setup_lsensor(epld);
    if (err < 0)
    {
        LOG_ERR("epl_setup_lsensor error!!\n");
        goto err_lightsensor_setup;
    }
#endif

    err = epl_setup_psensor(epld);
    if (err < 0)
    {
        LOG_ERR("epl_setup_psensor error!!\n");
        goto err_psensor_setup;
    }


    if (epl_sensor.als.polling_mode==0 || epl_sensor.ps.polling_mode==0 || epl_sensor.ges.polling_mode ==0)
    {
        err = epl_setup_interrupt(epld);
        if (err < 0)
        {
            LOG_ERR("setup error!\n");
            goto err_sensor_setup;
        }
    }
    //disable_irq(epld->irq); //ices add
#if SENSOR_CLASS
    epld->als_cdev = als_cdev;
	epld->als_cdev.sensors_enable = epld_sensor_cdev_enable_als;
	epld->als_cdev.sensors_poll_delay = epl_cdev_set_als_delay;
	//epld->als_cdev.sensors_flush = epl_snesor_cdev_als_flush; //dont support
	err = sensors_classdev_register(&client->dev, &epld->als_cdev);
	if (err) {
		LOG_ERR("sensors class register failed.\n");
		goto err_register_als_cdev;
	}

	epld->ps_cdev = ps_cdev;
	epld->ps_cdev.sensors_enable = epld_sensor_cdev_enable_ps;
	epld->ps_cdev.sensors_poll_delay = epl_snesor_cdev_set_ps_delay;
	//epld->ps_cdev.sensors_flush = epl_snesor_cdev_ps_flush;   //dont support
	//epld->ps_cdev.sensors_calibrate = epl_snesor_cdev_ps_calibrate;   //dont support
	//epld->ps_cdev.sensors_write_cal_params = epl_snesor_cdev_ps_write_cal;    //dont support
	//epld->ps_cdev.params = epld->calibrate_buf;   //dont support
	err = sensors_classdev_register(&client->dev, &epld->ps_cdev);
	if (err) {
		LOG_ERR("sensors class register failed.\n");
		goto err_register_ps_cdev;
	}
#endif

#ifdef CONFIG_SUSPEND
#if defined(CONFIG_HAS_EARLYSUSPEND)
    epld->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + 1;
    epld->early_suspend.suspend = epl_early_suspend;
    epld->early_suspend.resume = epl_late_resume;
    register_early_suspend(&epld->early_suspend);
#endif
#endif

    sensor_dev = platform_device_register_simple("elan_alsps", -1, NULL, 0);
    if (IS_ERR(sensor_dev))
    {
        printk ("sensor_dev_init: error\n");
        goto err_fail;
    }

    err = sysfs_create_group(&sensor_dev->dev.kobj, &epl_attr_group);
    if (err !=0)
    {
        dev_err(&client->dev,"%s:create sysfs group error", __func__);
        goto err_fail;
    }
    LOG_INFO("sensor probe success.\n");

    return err;

#if SENSOR_CLASS
err_register_ps_cdev:
    sensors_classdev_unregister(&epld->ps_cdev);
err_register_als_cdev:
	sensors_classdev_unregister(&epld->als_cdev);
#endif
err_fail:
    input_unregister_device(epld->als_input_dev);
    input_unregister_device(epld->ps_input_dev);
#if !(SPREAD)
    input_free_device(epld->als_input_dev);
#endif
    input_free_device(epld->ps_input_dev);
#if !(SPREAD)
err_lightsensor_setup:
#endif
err_psensor_setup:
err_sensor_setup:
    misc_deregister(&epl_ps_device);
#if !(SPREAD)
    misc_deregister(&epl_als_device);
#endif
//err_create_singlethread_workqueue:
i2c_fail:
//err_platform_data_null:
    kfree(epld);
    return err;
}
/*----------------------------------------------------------------------------*/
static int epl_remove(struct i2c_client *client)
{
    struct epl_priv *epld = i2c_get_clientdata(client);

    dev_dbg(&client->dev, "%s: enter.\n", __func__);

   // unregister_early_suspend(&epld->early_suspend);
    sysfs_remove_group(&sensor_dev->dev.kobj, &epl_attr_group);
    platform_device_unregister(sensor_dev);
    input_unregister_device(epld->als_input_dev);
    input_unregister_device(epld->ps_input_dev);
#if !(SPREAD)
    input_free_device(epld->als_input_dev);
#endif
    input_free_device(epld->ps_input_dev);
    misc_deregister(&epl_ps_device);
#if !(SPREAD)
    misc_deregister(&epl_als_device);
#endif
    free_irq(epld->irq,epld);
    kfree(epld);
    return 0;
}
/*----------------------------------------------------------------------------*/
static const struct i2c_device_id epl_id[] =
{
    { EPL_DEV_NAME, 0 },
    { }
};
/*----------------------------------------------------------------------------*/
#if QCOM || MARVELL
static struct of_device_id epl_match_table[] = {
            { .compatible = "epl,mg569xx",},
            { },
        };
#endif
/*----------------------------------------------------------------------------*/
static struct i2c_driver epl_driver =
{	
	.class  = I2C_CLASS_HWMON,
	    .driver	= {
        .name = EPL_DEV_NAME,
        .owner = THIS_MODULE,
#if QCOM || MARVELL
        .of_match_table =epl_match_table,
#endif
    },
    .probe	= epl_probe,
    .remove	= epl_remove,
    .id_table	= epl_id,
	

	.address_list	= normal_i2c,
#ifdef CONFIG_SUSPEND
    .suspend = epl_suspend,
    .resume = epl_resume,
#endif
};
/*----------------------------------------------------------------------------*/
static int __init epl_init(void)
{
	int ret;
	LOG_INFO("elan sensor __init epl_init*********.\n");
	
	epl_driver.detect = mg569_detect;
	ret = i2c_add_driver(&epl_driver);
	LOG_INFO("*************************elan sensor***************************************\n");
        return ret; 
    //return i2c_add_driver(&epl_driver);
}
/*----------------------------------------------------------------------------*/
static void __exit  epl_exit(void)
{
    i2c_del_driver(&epl_driver);
}
/*----------------------------------------------------------------------------*/
module_init(epl_init);
module_exit(epl_exit);
/*----------------------------------------------------------------------------*/
MODULE_AUTHOR("Renato Pan <renato.pan@eminent-tek.com>");
MODULE_DESCRIPTION("ELAN mg569xx driver");
MODULE_LICENSE("GPL");

