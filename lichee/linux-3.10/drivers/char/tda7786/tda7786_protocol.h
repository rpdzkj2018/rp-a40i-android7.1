#ifndef __TDA7786_PROTOCOL_H__
#define __TDA7786_PROTOCOL_H__

#ifdef TDA7786_PROTOCOL_IMPLEMENTATION
	#define PROTOCOL_EXTERN
#else
	#define PROTOCOL_EXTERN	extern
#endif

//#if (FM_MODULE_TYPE == FM_MODULE_TYPE_TDA7786) || (FM_MODULE_TYPE == FM_MODULE_TYPE_UNION)

#define TDA7786_I2C_MAIN_ADDR		(0xC2)
#define TDA7786_DIGITAL_I2C_ADDR	(0xC2 >> 1)
#define TDA7786_I2C_ASSIT_ADDR	(0xC8>>1)

typedef enum
{
	TDA7786_RW_NO_AUTOINC_32BIT,
	TDA7786_RW_NO_AUTOINC_24BIT,
	TDA7786_RW_AUTOINC_32BIT,
	TDA7786_RW_AUTOINC_24BIT,

	TDA7786_RW_MAX
}tda7703_rw_mode;

//PROTOCOL_EXTERN void TDA7786_HIT_DirectWrite(unsigned char DataNum, u8 *AddrBuf, u8 *WriteBuf, unsigned char mode, unsigned char cmd);
 void TDA7786_HIT_DirectWrite(unsigned char DataNum, u8 *AddrBuf, u8 *WriteBuf, unsigned char mode, unsigned char cmd);
 void TDA7786_HIT_DirectRead(unsigned int DataNum, u8 *AddrBuf, u8 *ReadBuf, unsigned char mode);
void TDA7786_HIT_BootcodeDownload(void);
//PROTOCOL_EXTERN void TDA7786_HIT_DirectRead(unsigned int DataNum, u8 *AddrBuf, u8 *ReadBuf, unsigned char mode);

//#endif
#endif

