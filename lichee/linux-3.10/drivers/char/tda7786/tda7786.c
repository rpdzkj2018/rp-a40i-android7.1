/**************************************************************************
*  ak7601.c
* 
*  ak7601 sample code version 1.0
* 
*  Create Date : 2015/07/31
* 
*  Modify Date : 
*
*  Create by   : gufengyun
* 
**************************************************************************/

#include <linux/i2c.h>
#include <linux/input.h>
#ifdef CONFIG_HAS_EARLYSUSPEND
    #include <linux/pm.h>
    #include <linux/earlysuspend.h>
#endif
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/async.h>
#include <linux/hrtimer.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/file.h>
#include <linux/proc_fs.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/gpio.h>
//#include <mach/irqs.h>
//#include <mach/system.h>
//#include <mach/hardware.h>

#include <linux/miscdevice.h>

#include <linux/types.h>

#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/err.h>
#include <linux/device.h>

#include <linux/of_gpio.h>
#include <linux/clk.h>
#include <linux/interrupt.h>
#include <linux/rfkill.h>
#include <linux/regulator/consumer.h>
#include <linux/sys_config.h>
#include <linux/if_ether.h>
#include <linux/etherdevice.h>
#include <linux/crypto.h>

#include <linux/scatterlist.h>
#include <linux/pinctrl/pinconf-sunxi.h>

#include "tda7786.h"
#include "tda7786_data.h"

#include "tda7786_boot_data.h"

#define DEVICE_NAME	"tda7786"

#define HIT_ADDRESS_BUFF_LEN	(3)
#define HIT_DATA_BUFF_LEN		(40)

static u8 HITAddressBuff[HIT_ADDRESS_BUFF_LEN];
static u8 HITDataBuff[HIT_DATA_BUFF_LEN];
static u8 hit_startup_fsm;
static  u8	TuneMode = FM_RECEIVER;
static  u8 F_AlignDone;
static  u8 F_IS;
static  u8 F_TDBusy;
static  u8  F_SeqChg;


//static hit_para_t hit_para;
static u32 BackupFreq, CurFreq;

typedef struct tda7786_config
{
	u32 tda7786_used;
	u32 twi_id;
	u32 address;
	//struct gpio_config reset_gpio;
	u32 reset_gpio;
	//struct gpio_config mute_gpio;
	u32 rsd_gpio;
}tda7786_config;

static   __fm_setting_info_t       tda7786_info[2] = {
											{8750, 10800, 10430, 0, 3, 0, Band_FM1},
											{531, 1629, 531, 0, 0, 3, 0, Band_MW1}};

																						
											
//static ak7601_parameters  parameter;
static tda7786_config tda7786_config_info;

static struct i2c_client *this_client;

static const struct i2c_device_id tda7786_id[] = {
	{ DEVICE_NAME, 0 },
	{}
};
//extern void TDA7786_HIT_BootcodeDownload(void);
//extern void TDA7786_HIT_DirectWrite(unsigned char DataNum, u8 *AddrBuf, u8 *WriteBuf, unsigned char mode, unsigned char cmd);
//extern void TDA7786_HIT_DirectRead(unsigned int DataNum, u8 *AddrBuf, u8 *ReadBuf, unsigned char mode);
//extern void TDA7786_HIT_DirectRead(unsigned int DataNum, u8 *AddrBuf, u8 *ReadBuf, unsigned char mode);

#define __DEBUG_ON
#ifdef __DEBUG_ON
	#define printinfo(fmt, arg...)	printk("%s-<%d>: "fmt, __func__, __LINE__, ##arg)
#else
	#define printinfo(...)
#endif

 int tda7786_open(struct inode *inode,struct file *filp){
 	printinfo("Device Opened Success!\n:*************************\n");
	return 0;
}
 int tda7786_release(struct inode *inode,struct file *filp){
 	printinfo("Device released Success!\n:*************************\n");
	return 0;
}

static int AW_I2C_ReadXByte(uint8_t *buf, uint16_t len)
{	
	struct i2c_msg msgs[2]; 
	int ret=-1; 

	msgs[0].flags = !I2C_M_RD;	
	msgs[0].addr = this_client->addr;	
	msgs[0].len = 3;		//data address	
	msgs[0].buf = buf;	
	msgs[1].flags = I2C_M_RD;
	msgs[1].addr = this_client->addr; //client->addr;	
	msgs[1].len = len-3;	
	msgs[1].buf = buf+3;	
	ret=i2c_transfer(this_client->adapter, msgs, 2); 
	return ret;
}

static int AW_I2C_WriteXByte(uint8_t *data, uint16_t len)
{ 
	struct i2c_msg msg; 
	int ret=-1; 
	msg.flags = !I2C_M_RD;
	msg.addr = this_client->addr;   //client->addr;	
	msg.len = len;	
	msg.buf = data; 
	ret=i2c_transfer(this_client->adapter, &msg,1);	
	return ret;
}



void TDA7786_HIT_DirectWrite(unsigned char DataNum, u8 *AddrBuf, u8 *WriteBuf, unsigned char mode, unsigned char cmd)//bool cmd)
{
	unsigned int i,j;
	//unsigned int checksum[4]={0,0,0,0};
	unsigned int checksum[3]={0,0,0};
	//DI;//asm sim;
	unsigned int buf_cnt=0;
	u8 buf[512] = {0};
	switch(mode)
	{
	case TDA7786_RW_NO_AUTOINC_32BIT:
#if 0
	HIT_StartTWI();
		HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress
	  	HIT_TxByteDirect((AddrBuf[0]&0x01)|0x80);//mode+addr1
	  	HIT_TxByteDirect(AddrBuf[1]);
		HIT_TxByteDirect(AddrBuf[2]);
	  	HIT_TxByteDirect(WriteBuf[0]);
	  	HIT_TxByteDirect(WriteBuf[1]);
	  	HIT_TxByteDirect(WriteBuf[2]);
	  	HIT_TxByteDirect(WriteBuf[3]);
		HIT_StopTWI();
#endif		
		break;
	case TDA7786_RW_AUTOINC_24BIT:
	
				
		buf[0] = (AddrBuf[0]&0x01)|0xf0;
		buf[1] = AddrBuf[1];
		buf[2] = AddrBuf[2];
		buf_cnt+=2;
		
		//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress
	  	//HIT_TxByteDirect((AddrBuf[0]&0x01)|0xf0);//mode+addr1
	  	//HIT_TxByteDirect(AddrBuf[1]);
		//HIT_TxByteDirect(AddrBuf[2]);
		for(i=0;i<DataNum;i++)
		{
			//HIT_TxByteDirect(WriteBuf[i*3]);
			//HIT_TxByteDirect(WriteBuf[i*3+1]);
			//HIT_TxByteDirect(WriteBuf[i*3+2]);
			buf[++buf_cnt] = WriteBuf[i*3];
			buf[++buf_cnt] = WriteBuf[i*3+1];
			buf[++buf_cnt] = WriteBuf[i*3+2];


			
			if(cmd)
			{
				for(j=2; j>=1;j--)
				{
				 	checksum[j]=checksum[j]+WriteBuf[i*3+j];
					if (checksum[j] >= 256)
					{
			                    checksum[j - 1] = checksum[j - 1] + 1;
			                    checksum[j] = checksum[j] - 256;
					}
				}
				 checksum[0]=checksum[0]+WriteBuf[i*3];
				 if (checksum[0]>=256)
				 	checksum[0]=checksum[0]-256;

			}
			
		}
		if(cmd)
		{


					buf[++buf_cnt] = checksum[0]&0xff;
					buf[++buf_cnt] = checksum[1]&0xff;
					buf[++buf_cnt] = checksum[2]&0xff;
					

		}
		
		//HIT_StopTWI();
	
		//AW_I2C_WriteXByte(buf,TDA7786_I2C_MAIN_ADDR,buf_cnt+1);
		AW_I2C_WriteXByte(buf,buf_cnt+1);
		break;
	default:
		break;
	}

	//EI;//asm rim;

}
/*********************************************
	Function:		HIT_DirectRead
	Description:	Directly read data from HIT
	Write/Modify:	Yete HUANG
	Time:		2008-March
*********************************************/
void TDA7786_HIT_DirectRead(unsigned int DataNum, u8 *AddrBuf, u8 *ReadBuf, unsigned char mode)//bool cmd)
{
//note :mode should by TDA7786_RW_NO_AUTOINC_32BIT**************

	unsigned int i;
	//DI;//asm sim;
	unsigned int tmp=DataNum;
	unsigned int buf_cnt=0;
	//u8 buf_recv[40] = {0};
	u8 buf_send[30] ={0};
	//HIT_StartTWI();
	//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress/write
	switch(mode)
	{
	case TDA7786_RW_NO_AUTOINC_32BIT: 

		break;
	case TDA7786_RW_AUTOINC_24BIT:
 
	//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress/write
  		//HIT_TxByteDirect((AddrBuf[0]&0x01)|0x70);//mode+addr1
	  	//HIT_TxByteDirect(AddrBuf[1]);
		//HIT_TxByteDirect(AddrBuf[2]);
		//HIT_StopTWI();
		
		buf_send[0] = (AddrBuf[0]&0x01)|0x70;  //del by wxh
		//buf_send[0] = (AddrBuf[0]&0x01)|0x00;   //modify by wxh
		buf_send[1] = AddrBuf[1];
		buf_send[2] = AddrBuf[2];
		buf_cnt+=2;

		mdelay(5);
		buf_cnt = 0;
		
		
		//HIT_i2c_delay(50);
		//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR|0x01);//chipaddress/read
		//HIT_i2c_delay(50);

		/*for(i=0;i<tmp;i++)
		{
			ReadBuf[i*3]=HIT_RxByte();
			HIT_AckTWI();
			ReadBuf[i*3+1]=HIT_RxByte();
			HIT_AckTWI();
			ReadBuf[i*3+2]=HIT_RxByte();	
			if(i==DataNum-1)
			{
				HIT_nAckTWI();
			}
			else
			{
				HIT_AckTWI();
			}
		}*/
		
	
		AW_I2C_ReadXByte(buf_send,3+(tmp-1)*4+4); 
		for(i=0;i<3+(tmp-1)*4+4;i++)
			{
				ReadBuf[i] = buf_send[i+3];
			}
		//HIT_StopTWI();
	
		break;
	default:
		break;
	}
	//EI;//asm rim;

}

/*********************************************
	Function:		HIT_BootcodeDownload
	Description:	Download the script file data
	Write/Modify:	Yete HUANG
	Time:		March-2008
*********************************************/
void TDA7786_HIT_BootcodeDownload(void)
{
	unsigned char num;
	unsigned int i,j;
	
	u8 buf[512]={0};
	unsigned int buf_cnt=0;
	//DI;							//asm sim;
		i=0;	
	while(tda7786_I2CBootData[i]!=0xff)
	{
		num=tda7786_I2CBootData[i];
		//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress
	  	//HIT_TxByteDirect((tda7786_I2CBootData[i+1]&0x01)|0xe0);//addr1
	  	//HIT_TxByteDirect(tda7786_I2CBootData[i+2]);
		//HIT_TxByteDirect(tda7786_I2CBootData[i+3]);
		buf[0] = (tda7786_I2CBootData[i+1]&0x01)|0xe0;
		buf[1] =  tda7786_I2CBootData[i+2];
		buf[2] =  tda7786_I2CBootData[i+3];
		buf_cnt = 2;
		for(j=i+4;j<i+4*num+4;j++)
		{
			//HIT_TxByteDirect(tda7786_I2CBootData[j]);
			buf[++buf_cnt] = tda7786_I2CBootData[j];
		}
		//HIT_StopTWI();
		i=i+4*num+4;

		AW_I2C_WriteXByte(buf,buf_cnt+1);
		
	}
	//EI;
}



/********************************************************************************/
 int tda7786_ioctl(struct file *filp,unsigned int cmd,unsigned long arg){
 	printinfo("tda7786_ioctl Success!\n:*************************\n");
	
	switch(cmd)
	{
		case 0:	
				if(arg==0){
					printinfo("functions_off************ioctl(fd,0,0) :*************************\n");
						
						gpio_set_value(tda7786_config_info.reset_gpio, 0);
						//gpio_set_value(tda7786_config_info.mute_gpio, 1);
						mdelay(10);
						gpio_set_value(tda7786_config_info.reset_gpio, 1);
						mdelay(10);
						//functions_off();
						
						//gpio_set_value(tda7786_config_info.mute_gpio, 0);
					}
				else if(arg==1){
						printinfo("FUNCTION1_ON**************ioctl(fd,0,1) :*************************\n");
						
						gpio_set_value(tda7786_config_info.reset_gpio, 0);
						//gpio_set_value(tda7786_config_info.mute_gpio, 1);
						mdelay(10);
						gpio_set_value(tda7786_config_info.reset_gpio, 1);
						mdelay(10);
						//FUNCTION1_ON();					
						//gpio_set_value(tda7786_config_info.mute_gpio, 0);
					}
			break;
			
		
		
		default:
			break;
	}
	
	return 0;
}


static bool Tda7786StartupFinish(void)
{
	return (hit_startup_fsm == TUNER_STARTUP_IDLE);
}


static void HIT_CmdWriteRead(unsigned char cmdcode,  unsigned char para_num, unsigned char rece_num)
{
	HITAddressBuff[0]=0x01;//Send command address 0x019000
	HITAddressBuff[1]=0x90;
	HITAddressBuff[2]=0x00;

	HITDataBuff[0]=(cmdcode&0xf0)>>4;
	HITDataBuff[1]=(cmdcode&0x0f)<<4;
	HITDataBuff[2]= (para_num+1) & 0x1f;

	TDA7786_HIT_DirectWrite(para_num+1, HITAddressBuff, HITDataBuff, TDA7786_RW_AUTOINC_24BIT, 1);

	HITAddressBuff[2]=0xEB;//command read address 0x01900a
	//if(rece_num>=1)
	{
		TDA7786_HIT_DirectRead(rece_num, HITAddressBuff, HITDataBuff, TDA7786_RW_AUTOINC_24BIT); //del by wxh
	}
}
static void Tda7786On(void)
{


	//TDA7786_INT_OUTPUT_LOW();
	//esKRNL_TimeDly(2);
	mdelay(10);
	//TDA7786_RST_OUTPUT_LOW();
	gpio_set_value(tda7786_config_info.reset_gpio, 0);
	//esKRNL_TimeDly(5);
	mdelay(300);
	//TDA7786_RST_OUTPUT_HIGH();
	gpio_set_value(tda7786_config_info.reset_gpio, 1);
	//esKRNL_TimeDly(1);
	mdelay(10);
	//TDA7786_INT_OUTPUT_HIGH();
	//esKRNL_TimeDly(1);
	//mdelay(10);
	
	hit_startup_fsm = TUNER_STARTUP_REQ;
}

/*********************************************
	Function:		HIT_CmdSetFEReg
	Description:	Set Front-end Registers. If the num=0, then just send cmd to FE registers to ROM-specified default values
	Write/Modify:	Yete HUANG
	Time:		2007-12
*********************************************/
static void HIT_CmdSetFEReg(unsigned char band, unsigned char start, unsigned char num)
{
	unsigned char cmdcode, i;
	cmdcode=CmdCode_SetFEReg;

	if(num==0)
	{
		//set FE registers to ROM-specified default values
		HIT_CmdWriteRead(cmdcode, 0,1);
	}
	else
	{
		//set FE registers to user-specified values, starting from specified subaddress
		HITDataBuff[3]=0x00;//para 1---Subaddress
		HITDataBuff[4]=0x00;
		HITDataBuff[5]=start;

		if(start==15)
		{
			if(band==BAND_FM)
			{
				HITDataBuff[6]=FMFEReg[8*3];
				HITDataBuff[7]=FMFEReg[8*3+1];
				HITDataBuff[8]=FMFEReg[8*3+2];
			}
			else
			{

				HITDataBuff[6]=AMFEReg[8*3];
				HITDataBuff[7]=AMFEReg[8*3+1];
				HITDataBuff[8]=AMFEReg[8*3+2];

			}
		}
		else
		{
			if(band==BAND_FM)
			{
				for(i=0; i<num; i++)
				{
					HITDataBuff[6+i*3]=FMFEReg[start*3+i*3];
					HITDataBuff[7+i*3]=FMFEReg[start*3+i*3+1];
					HITDataBuff[8+i*3]=FMFEReg[start*3+i*3+2];
				}
			}

			else
			{
				for(i=0; i<num; i++)
				{
					HITDataBuff[6+i*3]=AMFEReg[start*3+i*3];
					HITDataBuff[7+i*3]=AMFEReg[start*3+i*3+1];
					HITDataBuff[8+i*3]=AMFEReg[start*3+i*3+2];
				}
			}
		}
		HIT_CmdWriteRead(cmdcode, num+1,1);
	}

}

/*********************************************
	Function:		HIT_CmdSetIRType
	Description:
	Write/Modify:	Yete HUANG
	Time:		2008-3
*********************************************/
static void HIT_CmdSetIRType(unsigned char type)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_IR;
	HITDataBuff[3]=0x0;//Para1
	HITDataBuff[4]=0x0;
	//if(band==FM)
	//	HITDataBuff[5]=OnlineIR_FM;
	//else
	HITDataBuff[5]=type;
	HIT_CmdWriteRead(cmdcode,1,1);
}

/*********************************************
	Function:		HIT_CmdReadTDSR
	Description:
	Write/Modify:	Yete HUANG
	Time:		2008-02
*********************************************/
static void HIT_CmdReadTDSR(void)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_ReadTDS;

		

HIT_CmdWriteRead(cmdcode,0,1+1+1); //add read out checksum  del by wxh
	
	//modify by wxh
/*	if(HITDataBuff[4]&0x04)  
		F_AlignDone=1;
	else
		F_AlignDone=0;

	if(HITDataBuff[4]&0x08)
		F_IS=1;
	else
		F_IS=0;
*/
	if(HITDataBuff[5]&0x40)
		F_TDBusy=1;
	else
		F_TDBusy=0;
	
	if(HITDataBuff[3]&0x01)
		F_SeqChg=1;
	else
		F_SeqChg=0;
}

/*********************************************
	Function:		HIT_CmdStartAlign
	Description:
	Write/Modify:	Yete HUANG
	Time:		2008-2
*********************************************/
static void HIT_CmdStartAlign(unsigned char AlignMode)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_Startup;
	HITDataBuff[3]=0x0;//Para1
	HITDataBuff[4]=0x0;
	HITDataBuff[5]=AlignMode;
	HIT_CmdWriteRead(cmdcode,1,1);

}

/*********************************************
	Function:		HIT_DSPMode
	Description:	Set HIT DSP to FM,AM,IDLE,WX
	Write/Modify:	Yete HUANG
	Time:		March-2008
*********************************************/

static void HIT_DSPMode(unsigned char DspWorkMode)
{
	HITDataBuff[3]=0x01;//Para 1
	HITDataBuff[4]=0xa0;
	HITDataBuff[5]=0x06;

	HITDataBuff[6]=0x0;//Para2,
	HITDataBuff[7]=0x0;
	HITDataBuff[8]=DspWorkMode;
	HIT_CmdWriteRead(CmdCode_WriteXmen,2,1);
}
static void HIT_CmdStarUp(void)
{
	unsigned char cmdcode;

	cmdcode =CmdCode_StartStartUp;
#if 0	
	HITDataBuff[3]=0x02;//Para 1
	HITDataBuff[4]=0x20;
	HITDataBuff[5]=0x02;
	
	HITDataBuff[6]=0x0;//Para2,
	HITDataBuff[7]=0x0;
	HITDataBuff[8]=0x2;
#endif	

	HITDataBuff[3]=0x0;//Para1
	HITDataBuff[4]=0x0;
	HITDataBuff[5]=AlignVCO;
	HIT_CmdWriteRead(cmdcode,1,1);

}

static void HIT_Read_ChipID(void)
{
	HITDataBuff[3]=0x01;//Para 1
	HITDataBuff[4]=0x40;
	HITDataBuff[5]=0x06;
	
	HITDataBuff[6]=0x0;//Para2,
	HITDataBuff[7]=0x0;
	HITDataBuff[8]=0x0;
	HIT_CmdWriteRead(CmdCode_WriteXmen,1,1);

}
static void HIT_Elite()
{


	HITAddressBuff[0]=0x01;
			HITAddressBuff[1]=0x90;
			HITAddressBuff[2]=0xEB;  
			
TDA7786_HIT_DirectRead(1, HITAddressBuff, HITDataBuff, TDA7786_RW_AUTOINC_24BIT);
	
	
}


static void HIT_CmdWriteBeCoeff(const unsigned char* p_be_coeff)
{
     unsigned char num, num_word;
     unsigned int i;
     unsigned char addr1, addr2, addr3;
     unsigned long addr;
     unsigned char cmdcode;
     unsigned char offset;
     unsigned long addr_range;
     while( *p_be_coeff != 0xff )
     {
          num = num_word = *p_be_coeff;
          addr1 = *(p_be_coeff+1);
          addr2 = *(p_be_coeff+2);
          addr3 = *(p_be_coeff+3);
          offset = 0;
          addr_range = *(p_be_coeff + 1);
          addr_range <<=8;
          addr_range |= *(p_be_coeff + 2);
          addr_range <<=8;
          addr_range |= *(p_be_coeff + 3);
          if( addr_range <= 0x019fff  || (addr_range >= 0x01c000 && addr_range <= 0x01ffff ))
          {
               cmdcode = CmdCode_WriteDMAmem;
          }
          else  if( addr_range >= 0x01a000 && addr_range <= 0x01afff)
          {
			   cmdcode = CmdCode_Writemen;
          }
          else if( addr_range >= 0x01b000 && addr_range <= 0x01bfff )
          {
			   cmdcode = CmdCode_Writemen;
          }

          if(cmdcode == CmdCode_WriteDMAmem)
          {
               for(i = 0; i < num; i++)
               {
                    HITDataBuff[3]=addr1;//Para 1  memory address
                    HITDataBuff[4]=addr2;
                    HITDataBuff[5]=addr3;
                    HITDataBuff[6] = *(p_be_coeff+4+i*4);//Para 2
                    HITDataBuff[7] = *(p_be_coeff+5+i*4);
                    HITDataBuff[8] = *(p_be_coeff+6+i*4);
                    HITDataBuff[9] = HITDataBuff[7];
                    HITDataBuff[10] = HITDataBuff[8] ;
                    HITDataBuff[11] = *(p_be_coeff+7+i*4);
                    HIT_CmdWriteRead(cmdcode,3,1);
                    addr = addr1;
                    addr <<=8;
                    addr |= addr2;
                    addr <<=8;
                    addr |= addr3;
                    addr++;
                    addr1 = (addr & 0x00ff0000)>>16;
                    addr2 = (addr & 0x0000ff00)>>8;
                    addr3 = addr & 0x000000ff;
               }
               p_be_coeff += num_word *4 +4;
          }
		  else if(cmdcode == CmdCode_Writemen)
          {
               while(num > 7)
               {
                    HITDataBuff[3] = addr1;       //Para 1  memory address
                    HITDataBuff[4] = addr2;
                    HITDataBuff[5] = addr3;
                    for(i=0; i<7; i++)
                    {
                         HITDataBuff[6+i*3] = *(p_be_coeff+4+i*4+offset);//Para 2
                         HITDataBuff[7+i*3] = *(p_be_coeff+5+i*4+offset);
                         HITDataBuff[8+i*3] = *(p_be_coeff+6+i*4+offset);
                    }
                    HIT_CmdWriteRead(cmdcode,8,1);
                    addr = addr1;
                    addr <<=8;
                    addr |= addr2;
                    addr <<=8;
                    addr |= addr3;
                    addr += 7;
                    addr1 = (addr & 0x00ff0000)>>16;
                    addr2 = (addr & 0x0000ff00)>>8;
                    addr3 = addr & 0x000000ff;
                    offset += 4*7;
                    num -= 7;
               }
               HITDataBuff[3]=addr1;//Para 1  memory address
               HITDataBuff[4]=addr2;
               HITDataBuff[5]=addr3;
               for(i=0; i<num; i++)
               {
                    HITDataBuff[6+i*3]=*(p_be_coeff+4+i*4+offset);//Para 2
                    HITDataBuff[7+i*3]=*(p_be_coeff+5+i*4+offset);
                    HITDataBuff[8+i*3]=*(p_be_coeff+6+i*4+offset);
               }
               HIT_CmdWriteRead(cmdcode,num+1,1);
               p_be_coeff += num_word *4 +4;
          }
     }
}

/*********************************************
	Function:		HIT_CmdSetBand
	Description:	Set to band(FM/MW---USA/EUR/JP)
	Write/Modify:	Yete HUANG
	Time:		2007-12
*********************************************/
static void HIT_CmdSetBand(unsigned char band, __u32 MinFreq, __u32 MaxFreq,__u32 freq, u8 Area)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_SetBand;

	if(band==BAND_FM)
	{
		HITDataBuff[3]=0x00;//para 1
		HITDataBuff[4]=0x00;
		HITDataBuff[5]=band;

		HITDataBuff[6]=(unsigned char)((MinFreq&0xFF0000)>>16);//Para 2
		HITDataBuff[7]=(unsigned char)((MinFreq&0x00FF00)>>8);
		HITDataBuff[8]=(unsigned char)((MinFreq&0x0000FF));

		HITDataBuff[9]=(unsigned char)((MaxFreq&0xFF0000)>>16);//Para 3
		HITDataBuff[10]=(unsigned char)((MaxFreq&0x00FF00)>>8);
		HITDataBuff[11]=(unsigned char)(MaxFreq&0x0000FF);
		
		HITDataBuff[12]=(unsigned char)((freq&0xFF0000)>>16);//Para4
		HITDataBuff[13]=(unsigned char)((freq&0x00FF00)>>8);
		HITDataBuff[14]=(unsigned char)(freq&0x0000FF);
		
		HIT_CmdWriteRead(cmdcode, 4, 1);

	}
#if 0
	else if (band==BAND_AM)
	{

		if (band==BAND_AM &&(Area==DVR_FM_AREA_JAPAN||Area==DRV_FM_AREA_EUROPE))
		{
			HITDataBuff[3]=0x00;//para 1
			HITDataBuff[4]=0x00;
			HITDataBuff[5]=0x03;

			HITDataBuff[6]=(unsigned char)(((unsigned long)MinFreq/10&0xFF0000)>>16);//Para 2
			HITDataBuff[7]=(unsigned char)(((unsigned long)MinFreq/10&0x00FF00)>>8);
			HITDataBuff[8]=(unsigned char)((unsigned long)MinFreq/10&0x0000FF);

			HITDataBuff[9]=(unsigned char)(((unsigned long)MaxFreq/10&0xFF0000)>>16);//Para 3
			HITDataBuff[10]=(unsigned char)(((unsigned long)MaxFreq/10&0x00FF00)>>8);
			HITDataBuff[11]=(unsigned char)((unsigned long)MaxFreq/10&0x0000FF);

			HIT_CmdWriteRead(cmdcode, 3,1);

		}

		else   // other area
		{
			HITDataBuff[3]=0x00;//para 1
			HITDataBuff[4]=0x00;
			HITDataBuff[5]=0x02;

			HITDataBuff[6]=(unsigned char)(((unsigned long)MinFreq/10&0xFF0000)>>16);//Para 2
			HITDataBuff[7]=(unsigned char)(((unsigned long)MinFreq/10&0x00FF00)>>8);
			HITDataBuff[8]=(unsigned char)((unsigned long)MinFreq/10&0x0000FF);

			HITDataBuff[9]=(unsigned char)(((unsigned long)MaxFreq/10&0xFF0000)>>16);//Para 3
			HITDataBuff[10]=(unsigned char)(((unsigned long)MaxFreq/10&0x00FF00)>>8);
			HITDataBuff[11]=(unsigned char)((unsigned long)MaxFreq/10&0x0000FF);

			HIT_CmdWriteRead(cmdcode, 3, 1);
		}
	}
#endif
}

/*********************************************
	Function:		HIT_CmdDynIS
	Description:
	Write/Modify:	Yete HUANG
	Time:		2008-9
*********************************************/
static void HIT_CmdDynIS(unsigned char mode)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_SetDynIS;
	/*Para1----0x000000 means disable the dynamic image selection;
			     0x000001 means DynIS is enabled without the cyclic rechecking of the optimum injection side.
			     0x000002, the DynIS is enabled with the cyclic rechecking of the optimum injection side.*/
	/*no parameter----mode=0xff*/
	if (mode==0xff)
	{
		HIT_CmdWriteRead(cmdcode,0,1);
	}
	else
	{
		HITDataBuff[3]=0x0;
		HITDataBuff[4]=0x0;
		HITDataBuff[5]=mode;
		HIT_CmdWriteRead(cmdcode,1,1);

	}
}

/*********************************************
	Function:		HIT_CmdSeekTH
	Description:    It's used to program the measurement time and the thresholds of the station detector in seek mode. tmeas = M_CYC/45600 seconds
				fieldstrength /quality:  				high for good reception quality
				adjacent channel/detune/multipath:	low for good reception quality
	Write/Modify:	Yete HUANG
	Time:		2008-02
*********************************************/
static void HIT_CmdSeekTH(unsigned long MeasCycle, unsigned long fieldstrengthTH, unsigned long AdjTH, unsigned long DetuneTH, unsigned long MultipathTH, unsigned long QualityTH)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_SetSeekTH;

	HITDataBuff[3]=(unsigned char)((MeasCycle&0xFF0000)>>16);//Para1   Tmeas=MeasCycle/45600= MeasCycle*0.02193ms
	HITDataBuff[4]=(unsigned char)((MeasCycle&0x00FF00)>>8);
	HITDataBuff[5]=(unsigned char)(MeasCycle&0x0000FF);

	HITDataBuff[6]=(unsigned char)((fieldstrengthTH&0xFF0000)>>16);//Para2
	HITDataBuff[7]=(unsigned char)((fieldstrengthTH&0x00FF00)>>8);
	HITDataBuff[8]=(unsigned char)(fieldstrengthTH&0x0000FF);

	HITDataBuff[9]=(unsigned char)((AdjTH&0xFF0000)>>16);//Para3
	HITDataBuff[10]=(unsigned char)((AdjTH&0x00FF00)>>8);
	HITDataBuff[11]=(unsigned char)(AdjTH&0x0000FF);

	HITDataBuff[12]=(unsigned char)((DetuneTH&0xFF0000)>>16);//Para4
	HITDataBuff[13]=(unsigned char)((DetuneTH&0x00FF00)>>8);
	HITDataBuff[14]=(unsigned char)(DetuneTH&0x0000FF);

	HITDataBuff[15]=(unsigned char)((MultipathTH&0xFF0000)>>16);//Para5
	HITDataBuff[16]=(unsigned char)((MultipathTH&0x00FF00)>>8);
	HITDataBuff[17]=(unsigned char)(MultipathTH&0x0000FF);

	HITDataBuff[18]=(unsigned char)((QualityTH&0xFF0000)>>16);//Para6
	HITDataBuff[19]=(unsigned char)((QualityTH&0x00FF00)>>8);
	HITDataBuff[20]=(unsigned char)(QualityTH&0x0000FF);

	HIT_CmdWriteRead(cmdcode,6,1);


}

/*********************************************
	Function:		Elite_CmdMuSICA
	Description:	Enable/disable Musica function command (Musica feature is to improve a lot the field performance in multi-path interfere condition)
*********************************************/
void Elite_CmdMuSICA(unsigned char val)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_MuSICA;

	HITDataBuff[3]=0x0;
	HITDataBuff[4]=0x0;
	HITDataBuff[5]=val;
	HIT_CmdWriteRead(cmdcode,1,1);
}



/*********************************************
	Function:		HIT_CmdSetDiss
	Description:
	Write/Modify:	Yete HUANG
	Time:		2008-3
*********************************************/
static void HIT_CmdSetDiss(unsigned char mode, unsigned char bandwidth)
{
	unsigned char cmdcode;
	cmdcode=CmdCode_SetDiss;
	HITDataBuff[3]=0x0;//Para1
	HITDataBuff[4]=0x0;
	HITDataBuff[5]=mode;

	if(mode==Diss_FM_Manual ||mode==Diss_FM_Special)
	{
		HITDataBuff[6]=0x0;//Para2
		HITDataBuff[7]=0x0;
		HITDataBuff[8]=bandwidth;
		HIT_CmdWriteRead(cmdcode,2,1);
	}
	else
	{
		HIT_CmdWriteRead(cmdcode,1,1);
	}
}

static void HIT_CmdSetFreq(__u32 setfreq, unsigned char InjectSidemode)
{
	unsigned char cmdcode;
	unsigned long freq;

	cmdcode=CmdCode_SetFreq;
	if(FreqInWBFM(setfreq))
		freq=(unsigned long)setfreq+100000;
	else if(FreqInFM(setfreq))		
		freq=(unsigned long)setfreq*10; 
	else if(FreqInSWAM(setfreq))
		freq=(unsigned long)((setfreq-1384)*5);
	else
		freq=(unsigned long)setfreq;

	HITDataBuff[3]=(unsigned char)((freq&0xFF0000)>>16);//Para1
	HITDataBuff[4]=(unsigned char)((freq&0x00FF00)>>8);
	HITDataBuff[5]=(unsigned char)(freq&0x0000FF);
/*  del by wxh
	HITDataBuff[6]=0x00;//Para2
	HITDataBuff[7]=0x00;
	HITDataBuff[8]=InjectSidemode;

	HIT_CmdWriteRead(cmdcode,2,1);
*/
	HIT_CmdWriteRead(cmdcode,1,1);
}

static void Tda7786SetFreq( __u32 setfreq)
{
	__u8 i = 0;
	__u8 cnt = 0;
	__u32 cur_freq;
	cur_freq = setfreq/10;

	do
	{
		cnt++;
		mdelay(10);
		if(cnt>200) break;
		HIT_CmdReadTDSR();
	}while(F_SeqChg);
	cnt =0;

	if(FreqInFM(cur_freq) && !FreqInWBFM(cur_freq))
	{
		HIT_CmdSetFreq(cur_freq,InjectSide_Auto);
		//HIT_CmdSetFreq(setfreq,InjectSide_Auto);
		//HIT_CmdSetDiss(Diss_FM_Manual, Diss_FM_Manual_BW4); //del 
		//esKRNL_TimeDly(10);
		//msleep(10);
		//HIT_CmdSetDiss(Diss_FM_Auto, 0);
		tda7786_info[FM_RECEIVER].curr_freq = cur_freq;
	}
	else if(FreqInWBFM(cur_freq))
	{
		HIT_CmdSetFreq(cur_freq,InjectSide_Auto);
		HIT_CmdSetDiss(Diss_WX_Neutral, 0);
	}
	else if(FreqInAM(cur_freq))
	{
		HIT_CmdSetFreq(cur_freq,InjectSide_Auto);
		//HIT_CmdSetDiss(Diss_AM_Neutral, 0);
		
		tda7786_info[AM_RECEIVER].curr_freq = cur_freq;
	}
	
	//esKRNL_TimeDly(10);
	//msleep(10);
	//CurFreq	= setfreq;
	
}

static void HIT_Change_Band( unsigned char  bandreq )
{
	//Change to FM band from other band
	unsigned int i;
	__u32 minfreq, maxfeq, curfreq;
	if(bandreq == CHANGE_FM)
	{
		//HIT_DSPMode(DSP_FM);  del by wxh
	        //HIT_CmdWriteBeCoeff( FMSWPatchData);  del by wxh
	        //set AF parameters--The step can be skipped if using defualt value
		//HIT_CmdSetBand(BAND_FM, tda7786_info[FM_RECEIVER].min_freq, tda7786_info[FM_RECEIVER].max_freq,97100,tda7786_info[FM_RECEIVER].area_select);
	minfreq = tda7786_info[FM_RECEIVER].min_freq * 10;
	maxfeq = tda7786_info[FM_RECEIVER].max_freq * 10;
	curfreq = tda7786_info[FM_RECEIVER].curr_freq * 10;
	HIT_CmdSetBand(BAND_FM, minfreq, maxfeq,curfreq,1);
		
#ifdef HIT2AB
	        HIT_CmdSetFEReg(BAND_FM,0,5);
	        HIT_CmdSetFEReg(BAND_FM,5,5);
	        HIT_CmdSetFEReg(BAND_FM,15,1);
#else
		//HIT_CmdSetFEReg(BAND_FM,0,4);/*use ctm define FE setting*/
		//HIT_CmdSetFEReg(BAND_FM,4,4);
		//HIT_CmdSetFEReg(BAND_FM,15,1);
#endif
	        //HIT_CmdSetDDC();
		//esKRNL_TimeDly(5);
	        //HIT_CmdSetIRType(OnlineIR_FM);

		F_TDBusy=1;
		do
		{
			HIT_CmdReadTDSR();
			if(++i > 255)
				break;
			mdelay(1);
		}while(F_SeqChg);
		
		
	        /*Enable option funtion e.g. DMQ*/
	       // HIT_CmdMuSICA(0);
		//HIT_CmdDynIS(0x1); //del
	        /*SetSeekParameter if use auto seek method in HIT2*/
 		//HIT_CmdSeekTH(0x500, 0xC80000, 0x300000, 0x030000, 0x100000, 0x000000);  
		//esKRNL_TimeDly(5);
		//mdelay(5);

		//T_CmdSetFreq(RadioCtrl.CurrentFrequency); 

		//esKRNL_TimeDly(5);
	

		//HIT_CmdSetDiss(Diss_FM_Manual, Diss_FM_Manual_BW4);
		//HIT_CmdSetDiss(Diss_FM_Auto, 0);
		//HIT_CmdWriteBeCoeff( FMCustomWSPData);  //del by wxh
		//mdelay(5);

				/*Enable option funtion e.g. Musica*/

			
		Elite_CmdMuSICA(1); //add by wxh


		/*Enable option funtion e.g. Musica*/
		//Elite_CmdMuSICA(musica_flg); //add 
	}
	//Change to AM band from other band
	else if(bandreq == CHANGE_AM )
	{
	/*
		if(tda7786_info[TuneMode].area_select ==DVR_FM_AREA_JAPAN||tda7786_info[TuneMode].area_select  ==DRV_FM_AREA_EUROPE)
		{
			HIT_DSPMode(DSP_AM_EuJp);
		}
		else
		{
			HIT_DSPMode(DSP_AM_Us);
		}
	*/
	HIT_CmdSetBand(BAND_AM, tda7786_info[AM_RECEIVER].min_freq, tda7786_info[AM_RECEIVER].max_freq, tda7786_info[AM_RECEIVER].curr_freq,1);
	
		do
		{
			HIT_CmdReadTDSR();
			if(++i > 65500)
				break;
			mdelay(1);
		}while(F_SeqChg);
	
       		//HIT_CmdWriteBeCoeff( AMSWPatchData);
	#ifdef HIT2AB
	        HIT_CmdSetFEReg(BAND_AM,0,5);/*use ctm define FE setting*/
	        HIT_CmdSetFEReg(BAND_AM,5,5);
	        HIT_CmdSetFEReg(BAND_AM,15,1);
	#else
		//HIT_CmdSetFEReg(BAND_AM,0,4);/*use ctm define FE setting*/
		//HIT_CmdSetFEReg(BAND_AM,4,4);
		//HIT_CmdSetFEReg(BAND_AM,15,1);
	#endif
	
	        /*SetSeekParameter if use auto seek method in HIT2*/
		//HIT_CmdSeekTH(0x000700, 0xB60000, 0x7fffff, 0x100000, 0x7fffff, 0x000000);
		//Tda7786SetFreq(CurFreq);
		//esKRNL_TimeDly(5);
		//msleep(5);
	
		//HIT_CmdSetDiss(Diss_AM_Neutral, 0);
		HIT_CmdWriteBeCoeff(AMCustomWSPData);
			
	}
	
	//Tda7786SetFreq(CurFreq);
}


static void HIT_Startup_Seq(void)
{
	static u16 times = 0;
	switch (hit_startup_fsm)
	{
	case TUNER_STARTUP_REQ:
		//HIT_Read_ChipID();
		//esKRNL_TimeDly(10);
		msleep(10);
		TDA7786_HIT_BootcodeDownload();  
		//esKRNL_TimeDly(15);
		msleep(15);
		
		//esKRNL_TimeDly(15);
		//HIT_CmdStarUp();
		
		HIT_Elite();
		if(HITDataBuff[0]==0xAF&&HITDataBuff[1]==0xFE&&HITDataBuff[2]==0x42)
			{
				printinfo("TDA7786_HIT_BootcodeDownload success!!!!!!!!! @@@@@@@@\n");
				msleep(15);
			//HIT_DSPMode(DSP_FM);
			  HIT_CmdStartAlign(AlignVCO); //detel by wxh
			hit_startup_fsm=TUNER_STARTUP_WAIT_VCO_FE_ALIGN;
			break;
			}
			
		
		hit_startup_fsm = TUNER_STARTUP_REQ;

		//esKRNL_TimeDly(50);
		msleep(50);
		break;
	case TUNER_STARTUP_WAIT_DSPINIT:
		msleep(50);
		HIT_CmdReadTDSR();
			times++;

		if((!F_TDBusy)||(times>500))
		{
		printinfo("TUNER_STARTUP_WAIT_DSPINIT HIT_CmdReadTDSR times = %d @@@@@@@@\n",times);
		   //set FE FM parameters
		   //HIT_CmdSetFEReg(FM,0,0);//num=0, send current internal values to FE registers (refresh)
		   HIT_CmdSetFEReg(BAND_FM,0,4);/*use ctm define FE setting*/
		   HIT_CmdSetFEReg(BAND_FM,4,4);
		   HIT_CmdSetFEReg(BAND_FM,15,1);
		   hit_startup_fsm = TUNER_STARTUP_WAIT_VCO_FE_ALIGN;
		   times =0;
		}
		break;
	case TUNER_STARTUP_WAIT_VCO_FE_ALIGN:
		HIT_CmdReadTDSR();
		if(!F_TDBusy || times >= 500)
		{
		
			//HIT_DSPMode(DSP_IDLE);
			if(TuneMode == FM_RECEIVER)
			{
				HIT_Change_Band(CHANGE_FM);
			}
			else
			{
				HIT_Change_Band(CHANGE_AM);
			}
			//F_TunerDispReq=1;


			
			Tda7786SetFreq(104300);
			hit_startup_fsm= TUNER_STARTUP_IDLE;
			
		}
		else
		{
			times++;
		}
		break;
	case TUNER_STARTUP_IDLE:
		{
		times =0;
		break;
		}	
	default:
		break;
	}
}

static struct file_operations tda7786_ops = {	
	.owner 	= THIS_MODULE,	
	.open 	= tda7786_open,
	//.write 	= ak7601_write,
	.release= tda7786_release,
	.unlocked_ioctl = tda7786_ioctl,
};


static struct miscdevice tda7786_dev = {
	.minor	= MISC_DYNAMIC_MINOR,	
	.fops	= &tda7786_ops,	
	.name	= "tda7786",
};


static int tda7786_probe(struct i2c_client * client,const struct i2c_device_id * id)
{
	int err = 0;
	int aux;
	printinfo("AW Tda7786_probe begin ! :*************************\n");
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		err = -ENODEV;
		printinfo("i2c_check_functionality failed! :*************************\n");
		return err;
	}
	this_client = client;

	
	//mute	

	printinfo("AW Tda7786 On begin ! :*************************\n");
		Tda7786On();
	
	aux = Band_FM1;
	switch(aux)
	{		
		case Band_FM1:
		case Band_FM2:
		case Band_FM3:
			TuneMode = FM_RECEIVER;
			break;
		case Band_MW1:
		case Band_MW2:
			TuneMode = AM_RECEIVER;
			break;
		default:
			break;
	}
	
	tda7786_info[TuneMode].band = aux;	
		
	do
	{
		HIT_Startup_Seq();
	}while(!Tda7786StartupFinish());
	
		
		
	printinfo("*****HIT_Startup_Seq successed !*************************\n");

	if(misc_register(&tda7786_dev)<0)
	{
		pr_err("tda7786 register device failed!\n");
		return -1;
	}
	
	
	printinfo("misc_register successed!*************\n");
	
	return 0;
}

static int  tda7786_remove(struct i2c_client *client)
{
	i2c_set_clientdata(client, NULL);
	return 0;
}

  
static const unsigned short normal_i2c[] = {0xc2>>1, I2C_CLIENT_END}; 


static int  tda7786_detect(struct i2c_client *client, struct i2c_board_info *info)
{
	struct i2c_adapter *adapter = client->adapter;
        
        if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA))
                return -ENODEV;
	printinfo("adapter->nr=%d,client->addr=%x\n",adapter->nr,client->addr);
	if((tda7786_config_info.twi_id == adapter->nr) && (client->addr == tda7786_config_info.address))
	{
		printinfo("%s: Detected chip %s at adapter %d, address 0x%02x\n",
			 __func__, DEVICE_NAME, i2c_adapter_id(adapter), client->addr);

	
		struct i2c_msg msgs[2]; 
		char buf[10];
		int ret=-1; 

		memset(buf,0,sizeof(buf));
		msgs[0].flags = !I2C_M_RD;	
		msgs[0].addr = client->addr;	
		msgs[0].len = 3;		//data address	
		msgs[0].buf = buf;	
		msgs[1].flags = I2C_M_RD;
		msgs[1].addr = client->addr; //client->addr;	
		msgs[1].len = 1;	
		msgs[1].buf = buf;	
		ret=i2c_transfer(client->adapter, msgs, 2); 
		if(ret <= 0)
		{
			printk(KERN_ERR "detect tda7786 fail\n");
			return -ENODEV;
		}
		strlcpy(info->type, DEVICE_NAME, I2C_NAME_SIZE);
		return 0;
	}else{
		return -ENODEV;
	}
}


MODULE_DEVICE_TABLE(i2c, tda7786_id);

static struct i2c_driver tda7786_driver = {
	.class = I2C_CLASS_HWMON,
	.probe		= tda7786_probe,
	.remove		= tda7786_remove,
	.id_table	= tda7786_id,
	.detect = tda7786_detect,
	.driver	= {
		.name	= DEVICE_NAME,
		.owner	= THIS_MODULE,
	},
	.address_list	= normal_i2c,
};

static int script_data_init(void)
{
	//script_item_value_type_e type = 0;
	//script_item_u item_temp;
	struct device_node *np = NULL;  //wxh
	struct gpio_config config;
	int ret;
	int tda7786_used;
	int twi_id;
	int address;
	np = of_find_node_by_name(NULL,"tda7786");
	if (!np) {
		 pr_err("ERROR! get tda7786_para failed, func:%s, line:%d\n",__FUNCTION__, __LINE__);
		 return -1;
	}
	tda7786_config_info.tda7786_used = 1;
	tda7786_config_info.twi_id = 2;
	tda7786_config_info.address=0xc2>>1;
	//------------------------------	

//-----------------

	tda7786_config_info.reset_gpio = of_get_named_gpio_flags(np, "tda7786_reset", 0, (enum of_gpio_flags *)&config);
	if (!gpio_is_valid(tda7786_config_info.reset_gpio))
	{
		pr_err("%s: reset_gpio is invalid. \n",__func__ );	
		return -1;
	}else
	{
			pr_err("%s: reset_gpio success. \n",__func__ );
				if(0 != gpio_request(tda7786_config_info.reset_gpio, NULL)) 
				{		
					//pr_err("reset_gpio_request is failed\n");		
					printinfo("reset_gpio_request is failed\n");
					return -1;	
				} 	
				if (0 != gpio_direction_output(tda7786_config_info.reset_gpio, 0)) 
				{		
					//pr_err("reset_gpio set err!");	
					printinfo("reset_gpio set err!");	
					return -1;	
				}
				mdelay(300);
				gpio_set_value(tda7786_config_info.reset_gpio, 0);
				//esKRNL_TimeDly(5);
				mdelay(300);
				//TDA7786_RST_OUTPUT_HIGH();
				gpio_set_value(tda7786_config_info.reset_gpio, 1);

				
	}
	
	tda7786_config_info.rsd_gpio = of_get_named_gpio_flags(np, "tda7786_rds", 0, (enum of_gpio_flags *)&config);
	if (!gpio_is_valid(tda7786_config_info.rsd_gpio))
	{
		pr_err("%s: rsd_gpio is invalid. \n",__func__ );	
		//return -1;
	}else
	{
			pr_err("%s: rsd_gpio success. \n",__func__ );
				if(0 != gpio_request(tda7786_config_info.rsd_gpio, NULL)) 
				{		
					//pr_err("reset_gpio_request is failed\n");		
					printinfo("rsd_gpio_request is failed\n");
					//return -1;	
				} 	
				
				if (0 != gpio_direction_input(tda7786_config_info.rsd_gpio)) 
				{		
					//pr_err("reset_gpio set err!");	
					printinfo("rsd_gpio set err!");	
					//return -1;	
				}
				
				
	}
	
					
//--------------------------------
	
	

	return 1;
}

static int __init tda7786_init(void)
{ 

	int ret = -1;   
	printinfo("tda7786_init 20160908:*************************\n");
	if(script_data_init() > 0)
	{
	printinfo("tda7786_init\n");
		ret = i2c_add_driver(&tda7786_driver);
	}
	return 0;
	//return ret;
}

static void __exit tda7786_exit(void)
{
	printinfo("tda7786_exit\n");
	i2c_del_driver(&tda7786_driver);
	if(tda7786_config_info.reset_gpio != 0)
	{
		gpio_free(tda7786_config_info.reset_gpio);
		
		printinfo("tda7786_reset_gpio_free\n");
	}

}

module_init(tda7786_init);
module_exit(tda7786_exit);

MODULE_AUTHOR("<wxh@allwinnertech.com>");
MODULE_DESCRIPTION("external fm mdriver");
MODULE_LICENSE("GPL");

