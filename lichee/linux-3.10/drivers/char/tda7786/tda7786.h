#ifndef __TDA7786_H__
#define __TDA7786_H__

#ifdef TDA7786_IMPLEMENTATION
	#define TDA7786_EXTERN
#else
	#define TDA7786_EXTERN	extern
#endif

//#include  "drv_fm_i.h"

#define ModeXX1             0

//#if (FM_MODULE_TYPE == FM_MODULE_TYPE_TDA7786) || (FM_MODULE_TYPE == FM_MODULE_TYPE_UNION)

#define BAND_FM					1
#define BAND_AM 					2
#define WB					3
#define SW					4

#define CHANGE_FM 1
#define CHANGE_AM 2
#define CHANGE_WX 3
#define CHANGE_SW 4

#define FM_BAND_NUM 3
#define AM_BAND_NUM 2

#define DSP_IDLE					0x00
#define DSP_FM					0x01
#define DSP_AM_EuJp				0x02
#define DSP_AM_Us				0x03
#define DSP_WX					0x04


#define LW_FREQ						300
#define AM_FREQ						6450
#define SW_FREQ                    1725

//#define FM_SW_START_FREQ			2300
//#define FM_SW_STOP_FREQ			18135

#define FM_FREQ						6450

#define FM_NORMAL_START_FREQ		8000
#define FM_OIRT_START_FREQ		6450
#define FM_OIRT_STOP_FREQ			7500
#define FM_JAPAN_START_FREQ		7500
#define FM_JAPAN_STOP_FREQ		9100

#define FM_WB_START_FREQ			62300
#define FM_WB_STOP_FREQ			62600


#define FreqInFM(x)				((x)>((AM_FREQ)))
#define FreqInAM(x)				((x)<((FM_FREQ)))
#define FreqInNormalFM(x)			(x>FM_NORMAL_START_FREQ && x<FM_WB_START_FREQ)
#define FreqInOirtFM(x)			((x>FM_OIRT_START_FREQ && x<FM_OIRT_STOP_FREQ))
#define FreqInJapanFM(x)			(x>FM_JAPAN_START_FREQ && x<FM_JAPAN_STOP_FREQ)
#define FreqInWBFM(x)				(x>FM_WB_START_FREQ && x<FM_WB_STOP_FREQ)
#define FreqInMWAM(x)				((x)>LW_FREQ && (x)<SW_FREQ)
#define FreqInLWAM(x)				((x)<LW_FREQ)
#define FreqInSWAM(x)				((x)<FM_FREQ && (x)>SW_FREQ)

//Command
#define CmdCode_ReadXmen		0x01
#define CmdCode_ReadYmen		0x02
#define CmdCode_WriteXmen		0x04
#define CmdCode_WriteYmen		0x05
#define CmdCode_ReadDMAmem    0x00
#define CmdCode_WriteDMAmem   0x03
#define CmdCode_Writemen                 0x1F
#define CmdCode_Readmen                  0x1E

//#define CmdCode_SetFreq			0x24
#define CmdCode_SetFreq			0x08

#define InjectSide_Auto                  0x00
#define InjectSide_FixedLow              0x01
#define InjectSide_FixedHigh             0x02

#define CmdCode_SetFEReg		0x09
#define CmdCode_SetBand		0x23
#define CmdCode_SetDynIS		0x0b
#define CmdCode_SetSeekTH		0x14
#define CmdCode_StartManuSeek	0x15
#define CmdCode_StartStartUp	0x22


#define CmdCode_AFCheck		0x07
#define CmdCode_AFSwitch		0x10
#define CmdCode_AFStart			0x0d
#define CmdCode_AFMeasure		0x0e
#define CmdCode_AFEnd			0x0f

#define CmdCode_SetDDC                   0x1D

//#define CmdCode_StartAlignment	0x0c
#define CmdCode_Startup	0x22

#define AlignVCO_EUUSA					0x00//87.5MHz-108MHz
#define AlignVCO_Japan					0x01//76MHz-90MHz
#define AlignVCO							0x02//only align VCO

#define CmdCode_StartAutoSeek	0x16
#define AutoEnd							0x01
#define WaitCmdEnd						0x00

#define CmdCode_SeekEnd		0x17
#define SeekContinue						0x01
#define SeekStopMuted					0x02
#define SeekStopUnmuted					0x03

#define CmdCode_ReadSeekStatus	0x18
#define SeekStatus_RstQaul				0x01
#define SeekStatus_NoRstQaul				0x00
#define SeekStatus_ManualSeek			0x01
#define SeekStatus_AutoSeek				0x02
#define SeekStatus_ASE					0x04
#define SeekStatus_Seekok                          0x01
#define SeekStatus_FullCircle                       0x02

#define CmdCode_ReadTDS		0x11
#define CmdCode_ReadFEReg		0x12
#define CmdCode_ReadRDSQual	0x13

#define CmdCode_IR				0x19
#define OnlineIR_FM						0x00
#define HybridIR_AM						0x01
#define HybridIR_FM						0x02
#define IRalignment						0x03
#define HybridIR_WX                      0x04

#define CmdCode_SetDiss			0x1a
#define Diss_FM_Auto						0x00
#define Diss_FM_Manual					0x01
#define Diss_FM_Special					0x02
#define Diss_AM_Neutral					0x0a
#define Diss_WX_Neutral					0x0b
//BW for manual and special FM mode
#define Diss_FM_Manual_BW0						0x00//(narrowest)
#define Diss_FM_Manual_BW1						0x01
#define Diss_FM_Manual_BW2						0x02
#define Diss_FM_Manual_BW3						0x03
#define Diss_FM_Manual_BW4						0x04
#define Diss_FM_Manual_BW5						0x05
#define Diss_FM_Manual_BW6						0x06//(widest)

#define CmdCode_IBOC_Blender  0x06
#define CmdCode_MuSICA        0x20

#define BootData					0x00
#define FMPatchData				0x01
#define WXPatchData			0x02
#define AMPatchData			0x03
#define FMWSPData				0x04
#define AMWSPData				0x05
#define WXWSPData				0x06
#define BootData					0x00
#define FMPatchData				0x01
#define WXPatchData			0x02
#define AMPatchData			0x03
#define FMWSPData				0x04
#define AMWSPData				0x05
#define WXWSPData				0x06


#define TUNER_STARTUP_IDLE                      0x0
#define TUNER_STARTUP_REQ                       0x1
#define TUNER_STARTUP_WAIT_DSPINIT              0x2
#define TUNER_STARTUP_WAIT_VCO_FE_ALIGN         0x3

#define  FM_RECEIVER 0
#define  AM_RECEIVER 1

#define Band_FM1 1
#define Band_FM2 2
#define Band_FM3 3
#define Band_MW1 4
#define Band_MW2 5

#define DVR_FM_AREA_JAPAN   0
#define DRV_FM_AREA_EUROPE  1

#include "tda7786_protocol.h"

typedef struct 	fm_setting_info_t {
	u32  min_freq;
	u32  max_freq;
	u32  curr_freq;
	u32  area_select;
	u32  not_know;
	u32  signal_level;
	u32  stereo_select;
	u32  band;
	//87500, 108000, 0, 0, 3, 0, Band_FM1
} __fm_setting_info_t;
/*
 __s32 tda7786_fm_bandswitch(__s32 aux);
 __s32  tda7786_fm_init(__s32 aux);
 __s32 tda7786_fm_stereo_choose(__s32 audio_method);
 __s32 tda7786_fm_area_choose(__s32 area, void *pbuffer);
 __s32  tda7786_fm_exit(void);
 __s32 tda7786_fm_play(__s32 freq);
 __s32  tda7786_fm_send_on(void);
 __s32  tda7786_fm_send(__s32 freq);
 __s32  tda7786_fm_send_off(void);
 __s32  tda7786_fm_pa_gain(__u8 pagain);
 __s32  tda7786_fm_get_status(void);
 __s32 tda7786_fm_signal_level(__s32 signal_level);
 __s32  tda7786_fm_mute(__s32 voice_onoff);
 __s32 tda7786_fm_search_start(__s32 freq, __u32 search_dir);
 __s32 tda7786_fm_search_end(void);
 __s32 tda7786_fm_search_continue(void);
 __s32  tda7786_fm_auto_search(__s32 freq, __u32 search_dir);
 __s32 tda7786_fm_get_level(void);
 __s32 tda7786_fm_manual_search(__s32 freq, __u32 search_dir);
 __s32 tda7786_fm_get_stereo_indicator(void);
 */
//TDA7786_EXTERN __s32 tda7786_fm_get_stereo_indicator(void);

//extern fm_module_func_t tda7786_module;

//#endif
#endif

