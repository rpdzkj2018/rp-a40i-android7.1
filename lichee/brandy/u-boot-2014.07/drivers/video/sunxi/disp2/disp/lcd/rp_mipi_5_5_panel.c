#include "N106_panel.h"

extern s32 bsp_disp_get_panel_info(u32 screen_id, disp_panel_para *info);
static void LCD_power_on(u32 sel);
static void LCD_power_off(u32 sel);
static void LCD_bl_open(u32 sel);
static void LCD_bl_close(u32 sel);

static void LCD_panel_init(u32 sel);
static void LCD_panel_exit(u32 sel);

static u8 const mipi_dcs_pixel_format[4] = {0x77, 0x66, 0x66, 0x55};
#define panel_reset(val) sunxi_lcd_gpio_set_value(sel, 0, val)

static void LCD_cfg_panel_info(panel_extend_para *info)
{
	u32 i = 0, j = 0;
	u32 items;
	u8 lcd_gamma_tbl[][2] = {
		/*{input value, corrected value}*/
		{0, 0},
		{15, 15},
		{30, 30},
		{45, 45},
		{60, 60},
		{75, 75},
		{90, 90},
		{105, 105},
		{120, 120},
		{135, 135},
		{150, 150},
		{165, 165},
		{180, 180},
		{195, 195},
		{210, 210},
		{225, 225},
		{240, 240},
		{255, 255},
	};

	u32 lcd_cmap_tbl[2][3][4] = {
	{
		{LCD_CMAP_G0, LCD_CMAP_B1, LCD_CMAP_G2, LCD_CMAP_B3},
		{LCD_CMAP_B0, LCD_CMAP_R1, LCD_CMAP_B2, LCD_CMAP_R3},
		{LCD_CMAP_R0, LCD_CMAP_G1, LCD_CMAP_R2, LCD_CMAP_G3},
		},
		{
		{LCD_CMAP_B3, LCD_CMAP_G2, LCD_CMAP_B1, LCD_CMAP_G0},
		{LCD_CMAP_R3, LCD_CMAP_B2, LCD_CMAP_R1, LCD_CMAP_B0},
		{LCD_CMAP_G3, LCD_CMAP_R2, LCD_CMAP_G1, LCD_CMAP_R0},
		},
	};

	items = sizeof(lcd_gamma_tbl)/2;
	for (i = 0; i < items-1; i++) {
		u32 num = lcd_gamma_tbl[i+1][0] - lcd_gamma_tbl[i][0];

		for (j = 0; j < num; j++) {
			u32 value = 0;

			value = lcd_gamma_tbl[i][1] + ((lcd_gamma_tbl[i+1][1] - lcd_gamma_tbl[i][1]) * j)/num;
			info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] = (value<<16) + (value<<8) + value;
		}
	}
	info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items-1][1]<<16) + (lcd_gamma_tbl[items-1][1]<<8) + lcd_gamma_tbl[items-1][1];

	memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));

}

static s32 LCD_open_flow(u32 sel)
{
	LCD_OPEN_FUNC(sel, LCD_power_on, 30);   //open lcd power, and delay 50ms
	printf("flow power on\n");
	LCD_OPEN_FUNC(sel, LCD_panel_init, 50);   //open lcd power, than delay 200ms
	LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 100);     //open lcd controller, and delay 100ms
	LCD_OPEN_FUNC(sel, LCD_bl_open, 0);     //open lcd backlight, and delay 0ms
	
	return 0;
}

static s32 LCD_close_flow(u32 sel)
{
	LCD_CLOSE_FUNC(sel, LCD_bl_close, 200);
	LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 10);
	LCD_CLOSE_FUNC(sel, LCD_panel_exit, 200);
	LCD_CLOSE_FUNC(sel, LCD_power_off, 500);
	return 0;
}

static void LCD_power_on(u32 sel)
{
	sunxi_lcd_gpio_set_value(sel,2,0);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_power_enable(sel, 0);//config lcd_power pin to open lcd power0
	sunxi_lcd_backlight_enable(sel);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,0,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,1,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,2,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,3,1);
	printf("power on\n");
	sunxi_lcd_pin_cfg(sel, 1);
}

static void LCD_power_off(u32 sel)
{
	sunxi_lcd_pin_cfg(sel, 0);
	sunxi_lcd_delay_ms(20);
	panel_reset(0);
	sunxi_lcd_delay_ms(5);
	sunxi_lcd_power_disable(sel, 1);
	sunxi_lcd_delay_ms(5);
	sunxi_lcd_power_disable(sel, 0);
}

static void LCD_bl_open(u32 sel)
{
	sunxi_lcd_pwm_enable(sel);
	sunxi_lcd_backlight_enable(sel);
}

static void LCD_bl_close(u32 sel)
{
	sunxi_lcd_backlight_disable(sel);
	sunxi_lcd_pwm_disable(sel);
}

#define REGFLAG_DELAY	0XFE
#define REGFLAG_END_OF_TABLE	0xFD  /*END OF REGISTERS MARKER*/

struct LCM_setting_table {
    u8 cmd;
    u32 count;
    u8 para_list[64];
};

/*add panel initialization below*/


static struct LCM_setting_table LCM_LTL106HL02_setting[] = {

#if 1		//5.5inch 1080
	 {0xFE, 01, {0x00}},
	 {0xC2, 01, {0x08}},
	 {0x35, 01, {0x00}},
	 {0x53, 01, {0x20}},
	 {0x51, 01, {0xFF}},
#endif

#if 0		//5.5inch 720
			{0xFE,  01, {0x01}},
			{0x24,  01, {0xC0}},
			{0x25,  01, {0x53}},
			{0x26,  01, {0x00}},
			{0x2B,  01, {0xE5}},
			{0x27,  01, {0x0A}},
			{0x29,  01, {0x0A}},
			{0x16,  01, {0x52}},
			{0x2F,  01, {0x53}},
			{0x34,  01, {0x5A}},
			{0x1B,  01, {0x00}},
			{0x12,  01, {0x0A}},
			{0x1A,  01, {0x06}},
			{0x46,  01, {0x4F}},
			{0x52,  01, {0xA0}},
			{0x53,  01, {0x00}},
			{0x54,  01, {0xA0}},
			{0x55,  01, {0x00}},
			{0xFE,  01, {0x03}},
			{0x00,  01, {0x05}},
			{0x01,  01, {0x16}},
			{0x02,  01, {0x0B}},
			{0x03,  01, {0x0F}},
			{0x04,  01, {0x7D}},
			{0x05,  01, {0x00}},
			{0x06,  01, {0x50}},
			{0x07,  01, {0x05}},
			{0x08,  01, {0x16}},
			{0x09,  01, {0x0D}},
			{0x0A,  01, {0x11}},
			{0x0B,  01, {0x7D}},
			{0x0C,  01, {0x00}},
			{0x0D,  01, {0x50}},
			{0x0E,  01, {0x07}},
			{0x0F,  01, {0x08}},
			{0x10,  01, {0x01}},
			{0x11,  01, {0x02}},
			{0x12,  01, {0x00}},
			{0x13,  01, {0x7D}},
			{0x14,  01, {0x00}},
			{0x15,  01, {0x85}},
			{0x16,  01, {0x08}},
			{0x17,  01, {0x03}},
			{0x18,  01, {0x04}},
			{0x19,  01, {0x05}},
			{0x1A,  01, {0x06}},
			{0x1B,  01, {0x00}},
			{0x1C,  01, {0x7D}},
			{0x1D,  01, {0x00}},
			{0x1E,  01, {0x85}},
			{0x1F,  01, {0x08}},
			{0x20,  01, {0x00}},
			{0x21,  01, {0x00}},
			{0x22,  01, {0x00}},
			{0x23,  01, {0x00}},
			{0x24,  01, {0x00}},
			{0x25,  01, {0x00}},
			{0x26,  01, {0x00}},
			{0x27,  01, {0x00}},
			{0x28,  01, {0x00}},
			{0x29,  01, {0x00}},
			{0x2A,  01, {0x07}},
			{0x2B,  01, {0x08}},
			{0x2D,  01, {0x01}},
			{0x2F,  01, {0x02}},
			{0x30,  01, {0x00}},
			{0x31,  01, {0x40}},
			{0x32,  01, {0x05}},
			{0x33,  01, {0x08}},
			{0x34,  01, {0x54}},
			{0x35,  01, {0x7D}},
			{0x36,  01, {0x00}},
			{0x37,  01, {0x03}},
			{0x38,  01, {0x04}},
			{0x39,  01, {0x05}},
			{0x3A,  01, {0x06}},
			{0x3B,  01, {0x00}},
			{0x3D,  01, {0x40}},
			{0x3F,  01, {0x05}},
			{0x40,  01, {0x08}},
			{0x41,  01, {0x54}},
			{0x42,  01, {0x7D}},
			{0x43,  01, {0x00}},
			{0x44,  01, {0x00}},
			{0x45,  01, {0x00}},
			{0x46,  01, {0x00}},
			{0x47,  01, {0x00}},
			{0x48,  01, {0x00}},
			{0x49,  01, {0x00}},
			{0x4A,  01, {0x00}},
			{0x4B,  01, {0x00}},
			{0x4C,  01, {0x00}},
			{0x4D,  01, {0x00}},
			{0x4E,  01, {0x00}},
			{0x4F,  01, {0x00}},
			{0x50,  01, {0x00}},
			{0x51,  01, {0x00}},
			{0x52,  01, {0x00}},
			{0x53,  01, {0x00}},
			{0x54,  01, {0x00}},
			{0x55,  01, {0x00}},
			{0x56,  01, {0x00}},
			{0x58,  01, {0x00}},
			{0x59,  01, {0x00}},
			{0x5A,  01, {0x00}},
			{0x5B,  01, {0x00}},
			{0x5C,  01, {0x00}},
			{0x5D,  01, {0x00}},
			{0x5E,  01, {0x00}},
			{0x5F,  01, {0x00}},
			{0x60,  01, {0x00}},
			{0x61,  01, {0x00}},
			{0x62,  01, {0x00}},
			{0x63,  01, {0x00}},
			{0x64,  01, {0x00}},
			{0x65,  01, {0x00}},
			{0x66,  01, {0x00}},
			{0x67,  01, {0x00}},
			{0x68,  01, {0x00}},
			{0x69,  01, {0x00}},
			{0x6A,  01, {0x00}},
			{0x6B,  01, {0x00}},
			{0x6C,  01, {0x00}},
			{0x6D,  01, {0x00}},
			{0x6E,  01, {0x00}},
			{0x6F,  01, {0x00}},
			{0x70,  01, {0x00}},
			{0x71,  01, {0x00}},
			{0x72,  01, {0x20}},
			{0x73,  01, {0x00}},
			{0x74,  01, {0x08}},
			{0x75,  01, {0x08}},
			{0x76,  01, {0x08}},
			{0x77,  01, {0x08}},
			{0x78,  01, {0x08}},
			{0x79,  01, {0x08}},
			{0x7A,  01, {0x00}},
			{0x7B,  01, {0x00}},
			{0x7C,  01, {0x00}},
			{0x7D,  01, {0x00}},
			{0x7E,  01, {0xBF}},
			{0x7F,  01, {0x02}},
			{0x80,  01, {0x06}},
			{0x81,  01, {0x14}},
			{0x82,  01, {0x10}},
			{0x83,  01, {0x16}},
			{0x84,  01, {0x12}},
			{0x85,  01, {0x08}},
			{0x86,  01, {0x3F}},
			{0x87,  01, {0x3F}},
			{0x88,  01, {0x3F}},
			{0x89,  01, {0x3F}},
			{0x8A,  01, {0x3F}},
			{0x8B,  01, {0x0C}},
			{0x8C,  01, {0x0A}},
			{0x8D,  01, {0x0E}},
			{0x8E,  01, {0x3F}},
			{0x8F,  01, {0x3F}},
			{0x90,  01, {0x00}},
			{0x91,  01, {0x04}},
			{0x92,  01, {0x3F}},
			{0x93,  01, {0x3F}},
			{0x94,  01, {0x3F}},
			{0x95,  01, {0x3F}},
			{0x96,  01, {0x05}},
			{0x97,  01, {0x01}},
			{0x98,  01, {0x3F}},
			{0x99,  01, {0x3F}},
			{0x9A,  01, {0x0F}},
			{0x9B,  01, {0x0B}},
			{0x9C,  01, {0x0D}},
			{0x9D,  01, {0x3F}},
			{0x9E,  01, {0x3F}},
			{0x9F,  01, {0x3F}},
			{0xA0,  01, {0x3F}},
			{0xA2,  01, {0x3F}},
			{0xA3,  01, {0x09}},
			{0xA4,  01, {0x13}},
			{0xA5,  01, {0x17}},
			{0xA6,  01, {0x11}},
			{0xA7,  01, {0x15}},
			{0xA9,  01, {0x07}},
			{0xAA,  01, {0x03}},
			{0xAB,  01, {0x3F}},
			{0xAC,  01, {0x3F}},
			{0xAD,  01, {0x05}},
			{0xAE,  01, {0x01}},
			{0xAF,  01, {0x17}},
			{0xB0,  01, {0x13}},
			{0xB1,  01, {0x15}},
			{0xB2,  01, {0x11}},
			{0xB3,  01, {0x0F}},
			{0xB4,  01, {0x3F}},
			{0xB5,  01, {0x3F}},
			{0xB6,  01, {0x3F}},
			{0xB7,  01, {0x3F}},
			{0xB8,  01, {0x3F}},
			{0xB9,  01, {0x0B}},
			{0xBA,  01, {0x0D}},
			{0xBB,  01, {0x09}},
			{0xBC,  01, {0x3F}},
			{0xBD,  01, {0x3F}},
			{0xBE,  01, {0x07}},
			{0xBF,  01, {0x03}},
			{0xC0,  01, {0x3F}},
			{0xC1,  01, {0x3F}},
			{0xC2,  01, {0x3F}},
			{0xC3,  01, {0x3F}},
			{0xC4,  01, {0x02}},
			{0xC5,  01, {0x06}},
			{0xC6,  01, {0x3F}},
			{0xC7,  01, {0x3F}},
			{0xC8,  01, {0x08}},
			{0xC9,  01, {0x0C}},
			{0xCA,  01, {0x0A}},
			{0xCB,  01, {0x3F}},
			{0xCC,  01, {0x3F}},
			{0xCD,  01, {0x3F}},
			{0xCE,  01, {0x3F}},
			{0xCF,  01, {0x3F}},
			{0xD0,  01, {0x0E}},
			{0xD1,  01, {0x10}},
			{0xD2,  01, {0x14}},
			{0xD3,  01, {0x12}},
			{0xD4,  01, {0x16}},
			{0xD5,  01, {0x00}},
			{0xD6,  01, {0x04}},
			{0xD7,  01, {0x3F}},
			{0xDC,  01, {0x02}},
			{0xDE,  01, {0x12}},
			{0xFE,  01, {0x0E}},
			{0x01,  01, {0x75}},
			{0x54,  01, {0x01}},
			{0xFE,  01, {0x04}},
			{0x60,  01, {0x00}},
			{0x61,  01, {0x0C}},
			{0x62,  01, {0x12}},
			{0x63,  01, {0x0E}},
			{0x64,  01, {0x06}},
			{0x65,  01, {0x12}},
			{0x66,  01, {0x0E}},
			{0x67,  01, {0x0B}},
			{0x68,  01, {0x15}},
			{0x69,  01, {0x0B}},
			{0x6A,  01, {0x10}},
			{0x6B,  01, {0x07}},
			{0x6C,  01, {0x0F}},
			{0x6D,  01, {0x12}},
			{0x6E,  01, {0x0C}},
			{0x6F,  01, {0x00}},
			{0x70,  01, {0x00}},
			{0x71,  01, {0x0C}},
			{0x72,  01, {0x12}},
			{0x73,  01, {0x0E}},
			{0x74,  01, {0x06}},
			{0x75,  01, {0x12}},
			{0x76,  01, {0x0E}},
			{0x77,  01, {0x0B}},
			{0x78,  01, {0x15}},
			{0x79,  01, {0x0B}},
			{0x7A,  01, {0x10}},
			{0x7B,  01, {0x07}},
			{0x7C,  01, {0x0F}},
			{0x7D,  01, {0x12}},
			{0x7E,  01, {0x0C}},
			{0x7F,  01, {0x00}},
			{0xFE,  01, {0x00}},
			{0x58,  01, {0xAD}},	
#endif
 		{0x11,     0,    {0x00} },
 		{REGFLAG_DELAY, REGFLAG_DELAY, {120} },
		{0x29,			0,     {0x00} },
		{REGFLAG_DELAY, REGFLAG_DELAY, {50}},

		
	{REGFLAG_END_OF_TABLE, REGFLAG_END_OF_TABLE, {} }
};

static void LCD_panel_init(u32 sel)
{
	u32 i;
	
	printf("\n%s\n\n",__func__);

	sunxi_lcd_dsi_clk_enable(sel);
	sunxi_lcd_delay_ms(100);

	for (i = 0; ; i++) {
			if (LCM_LTL106HL02_setting[i].count == REGFLAG_END_OF_TABLE)
				break;
			else if (LCM_LTL106HL02_setting[i].count == REGFLAG_DELAY)
				sunxi_lcd_delay_ms(LCM_LTL106HL02_setting[i].para_list[0]);
			else{
				printf("%d %x %x %x\n",i, LCM_LTL106HL02_setting[i].cmd, LCM_LTL106HL02_setting[i].para_list[0], LCM_LTL106HL02_setting[i].count);
				dsi_dcs_wr(sel, LCM_LTL106HL02_setting[i].cmd, LCM_LTL106HL02_setting[i].para_list, LCM_LTL106HL02_setting[i].count);
				}
	}
	printf("\n\t<<uboot>>====>%s, init finished!\n", __func__);
	return;
}

static void LCD_panel_exit(u32 sel)
{
	sunxi_lcd_dsi_dcs_write_0para(sel, DSI_DCS_SET_DISPLAY_OFF);
	sunxi_lcd_delay_ms(20);
	sunxi_lcd_dsi_dcs_write_0para(sel, DSI_DCS_ENTER_SLEEP_MODE);
	sunxi_lcd_delay_ms(80);

	return ;
}

/*sel: 0:lcd0; 1:lcd1*/
static s32 LCD_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
	return 0;
}

/*sel: 0:lcd0; 1:lcd1
static s32 LCD_set_bright(u32 sel, u32 bright)
{
	sunxi_lcd_dsi_dcs_write_1para(sel, 0x51, bright);
	return 0;
}*/

__lcd_panel_t rp_mipi_5_5_panel = {
	/* panel driver name, must mach the name of lcd_drv_name in sys_config.fex */
	.name = "rp_mipi_5_5_panel",
	.func = {
		.cfg_panel_info = LCD_cfg_panel_info,
		.cfg_open_flow = LCD_open_flow,
		.cfg_close_flow = LCD_close_flow,
		.lcd_user_defined_func = LCD_user_defined_func,
		/*.set_bright = LCD_set_bright,*/
	},
};
