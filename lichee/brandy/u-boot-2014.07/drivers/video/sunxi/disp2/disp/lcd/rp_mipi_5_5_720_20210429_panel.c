#include "N106_panel.h"

extern s32 bsp_disp_get_panel_info(u32 screen_id, disp_panel_para *info);
static void LCD_power_on(u32 sel);
static void LCD_power_off(u32 sel);
static void LCD_bl_open(u32 sel);
static void LCD_bl_close(u32 sel);

static void LCD_panel_init(u32 sel);
static void LCD_panel_exit(u32 sel);

static u8 const mipi_dcs_pixel_format[4] = {0x77, 0x66, 0x66, 0x55};
#define panel_reset(val) sunxi_lcd_gpio_set_value(sel, 0, val)

static void LCD_cfg_panel_info(panel_extend_para *info)
{
	u32 i = 0, j = 0;
	u32 items;
	u8 lcd_gamma_tbl[][2] = {
		/*{input value, corrected value}*/
		{0, 0},
		{15, 15},
		{30, 30},
		{45, 45},
		{60, 60},
		{75, 75},
		{90, 90},
		{105, 105},
		{120, 120},
		{135, 135},
		{150, 150},
		{165, 165},
		{180, 180},
		{195, 195},
		{210, 210},
		{225, 225},
		{240, 240},
		{255, 255},
	};

	u32 lcd_cmap_tbl[2][3][4] = {
	{
		{LCD_CMAP_G0, LCD_CMAP_B1, LCD_CMAP_G2, LCD_CMAP_B3},
		{LCD_CMAP_B0, LCD_CMAP_R1, LCD_CMAP_B2, LCD_CMAP_R3},
		{LCD_CMAP_R0, LCD_CMAP_G1, LCD_CMAP_R2, LCD_CMAP_G3},
		},
		{
		{LCD_CMAP_B3, LCD_CMAP_G2, LCD_CMAP_B1, LCD_CMAP_G0},
		{LCD_CMAP_R3, LCD_CMAP_B2, LCD_CMAP_R1, LCD_CMAP_B0},
		{LCD_CMAP_G3, LCD_CMAP_R2, LCD_CMAP_G1, LCD_CMAP_R0},
		},
	};

	items = sizeof(lcd_gamma_tbl)/2;
	for (i = 0; i < items-1; i++) {
		u32 num = lcd_gamma_tbl[i+1][0] - lcd_gamma_tbl[i][0];

		for (j = 0; j < num; j++) {
			u32 value = 0;

			value = lcd_gamma_tbl[i][1] + ((lcd_gamma_tbl[i+1][1] - lcd_gamma_tbl[i][1]) * j)/num;
			info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] = (value<<16) + (value<<8) + value;
		}
	}
	info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items-1][1]<<16) + (lcd_gamma_tbl[items-1][1]<<8) + lcd_gamma_tbl[items-1][1];

	memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));

}

static s32 LCD_open_flow(u32 sel)
{
	LCD_OPEN_FUNC(sel, LCD_power_on, 30);   //open lcd power, and delay 50ms
	printf("flow power on\n");
	LCD_OPEN_FUNC(sel, LCD_panel_init, 50);   //open lcd power, than delay 200ms
	LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 100);     //open lcd controller, and delay 100ms
	LCD_OPEN_FUNC(sel, LCD_bl_open, 0);     //open lcd backlight, and delay 0ms
	
	return 0;
}

static s32 LCD_close_flow(u32 sel)
{
	LCD_CLOSE_FUNC(sel, LCD_bl_close, 200);
	LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 10);
	LCD_CLOSE_FUNC(sel, LCD_panel_exit, 200);
	LCD_CLOSE_FUNC(sel, LCD_power_off, 500);
	return 0;
}

static void LCD_power_on(u32 sel)
{
	sunxi_lcd_gpio_set_value(sel,2,0);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_power_enable(sel, 0);//config lcd_power pin to open lcd power0
	sunxi_lcd_backlight_enable(sel);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,0,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,1,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,2,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,3,1);
	printf("power on\n");
	sunxi_lcd_pin_cfg(sel, 1);
}

static void LCD_power_off(u32 sel)
{
	sunxi_lcd_pin_cfg(sel, 0);
	sunxi_lcd_delay_ms(20);
	panel_reset(0);
	sunxi_lcd_delay_ms(5);
	sunxi_lcd_power_disable(sel, 1);
	sunxi_lcd_delay_ms(5);
	sunxi_lcd_power_disable(sel, 0);
}

static void LCD_bl_open(u32 sel)
{
	sunxi_lcd_pwm_enable(sel);
	sunxi_lcd_backlight_enable(sel);
}

static void LCD_bl_close(u32 sel)
{
	sunxi_lcd_backlight_disable(sel);
	sunxi_lcd_pwm_disable(sel);
}

#define REGFLAG_DELAY	0XFE
#define REGFLAG_END_OF_TABLE	0xFD  /*END OF REGISTERS MARKER*/

struct LCM_setting_table {
    u8 cmd;
    u32 count;
    u8 para_list[64];
};

/*add panel initialization below*/


static struct LCM_setting_table LCM_LTL106HL02_setting[] = {
		{0xB9, 0x03,  {0xF1, 0x12, 0x83}},
		{0xBA, 0x1b,  {0x33, 0x81, 0x05, 0xF9, 0x0E, 0x0E, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x44, 0x25, 0x00, 0x91, 0x0A, 0x00, 0x00, 0x02, 0x4F, 0xD1, 0x00, 0x00, 0x37}},
		{0xB8, 0x04,  {0x26, 0x22, 0x20, 0x03}},
		{0xBF, 0x03,  {0x02, 0x11, 0x00}},
		{0xB3, 0x0a,  {0x0C, 0x10, 0x0A, 0x50, 0x03, 0xFF, 0x00, 0x00, 0x00, 0x00}},
		{0xC0, 0x09,  {0x73, 0x73, 0x50, 0x50, 0x00, 0x00, 0x08, 0x70, 0x00}},
		{0xBC, 0x01,  {0x46}},
		{0xCC, 0x01,  {0x0B}},
		{0xB4, 0x01,  {0x80}},
		{0xB2, 0x03,  {0xC8, 0x12, 0x30}},
		{0xE3, 0x0e,  {0x07, 0x07, 0x0B, 0x0B, 0x03, 0x0B, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0xC0, 0x10}},
		{0xC1, 0x0c,  {0x53, 0x00, 0x1E, 0x1E, 0x77, 0xC1, 0xFF, 0xFF, 0xAF, 0xAF, 0x7F, 0x7F}},
		{0xB5, 0x02,  {0x07, 0x07}},
		{0xB6, 0x02,  {0x70, 0x70}},
		{0xC6, 0x06,  {0x00, 0x00, 0xFF, 0xFF, 0x01, 0xFF}},
		{0xE9, 0x3f,  {0xC2, 0x10, 0x05, 0x04, 0xFE, 0x02, 0x81, 0x12, 0x31, 0x45, 0x3F, 0x83, 0x12, 0x91, 0x3B, 0x2A, 0x08, 0x05, 0x00, 0x00, 0x00, 0x00, 0x08, 0x05, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x02, 0x46, 0x02, 0x48, 0x68, 0x88, 0x88, 0x88, 0x80, 0x88, 0xFF, 0x13, 0x57, 0x13, 0x58, 0x78, 0x88, 0x88, 0x88, 0x81, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0xB1, 0x3B, 0x00, 0x00, 0x00, 0x00, 0x00}},
		{0xEA, 0x3d,  {0x00, 0x1A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x31, 0x75, 0x31, 0x18, 0x78, 0x88, 0x88, 0x88, 0x85, 0x88, 0xFF, 0x20, 0x64, 0x20, 0x08, 0x68, 0x88, 0x88, 0x88, 0x84, 0x88, 0x20, 0x10, 0x00, 0x00, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x30, 0x02, 0xA1, 0x00, 0x00, 0x00, 0x00}},
		{0xE0, 0x22,  {0x00, 0x05, 0x07, 0x1A, 0x39, 0x3F, 0x33, 0x2C, 0x06, 0x0B, 0x0D, 0x11, 0x13, 0x12, 0x14, 0x10, 0x1A, 0x00, 0x05, 0x07, 0x1A, 0x39, 0x3F, 0x33, 0x2C, 0x06, 0x0B, 0x0D, 0x11, 0x13, 0x12, 0x14, 0x10, 0x1A}},
 		
		{0x11,     0,    {0x00} },
 		{REGFLAG_DELAY, REGFLAG_DELAY, {120} },
		{0x29,			0,     {0x00} },
		{REGFLAG_DELAY, REGFLAG_DELAY, {100}},

		
	{REGFLAG_END_OF_TABLE, REGFLAG_END_OF_TABLE, {} }
};

static void LCD_panel_init(u32 sel)
{
	u32 i;
	
	printf("\n%s\n\n",__func__);

	sunxi_lcd_dsi_clk_enable(sel);
	sunxi_lcd_delay_ms(100);

	for (i = 0; ; i++) {
			if (LCM_LTL106HL02_setting[i].count == REGFLAG_END_OF_TABLE)
				break;
			else if (LCM_LTL106HL02_setting[i].count == REGFLAG_DELAY)
				sunxi_lcd_delay_ms(LCM_LTL106HL02_setting[i].para_list[0]);
			else{
				//printf("%d %x %x %x\n",i, LCM_LTL106HL02_setting[i].cmd, LCM_LTL106HL02_setting[i].para_list[0], LCM_LTL106HL02_setting[i].count);
				dsi_dcs_wr(sel, LCM_LTL106HL02_setting[i].cmd, LCM_LTL106HL02_setting[i].para_list, LCM_LTL106HL02_setting[i].count);
				}
	}
	printf("\t<<uboot>>====>%s, 5.5inch 720 20210429 init finished!\n", __func__);
	return;
}

static void LCD_panel_exit(u32 sel)
{
	sunxi_lcd_dsi_dcs_write_0para(sel, DSI_DCS_SET_DISPLAY_OFF);
	sunxi_lcd_delay_ms(20);
	sunxi_lcd_dsi_dcs_write_0para(sel, DSI_DCS_ENTER_SLEEP_MODE);
	sunxi_lcd_delay_ms(80);

	return ;
}

/*sel: 0:lcd0; 1:lcd1*/
static s32 LCD_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
	return 0;
}

/*sel: 0:lcd0; 1:lcd1
static s32 LCD_set_bright(u32 sel, u32 bright)
{
	sunxi_lcd_dsi_dcs_write_1para(sel, 0x51, bright);
	return 0;
}*/

__lcd_panel_t rp_mipi_5_5_720_20210429_panel = {
	/* panel driver name, must mach the name of lcd_drv_name in sys_config.fex */
	.name = "rp_mipi_5_5_720_20210429_panel",
	.func = {
		.cfg_panel_info = LCD_cfg_panel_info,
		.cfg_open_flow = LCD_open_flow,
		.cfg_close_flow = LCD_close_flow,
		.lcd_user_defined_func = LCD_user_defined_func,
		/*.set_bright = LCD_set_bright,*/
	},
};
